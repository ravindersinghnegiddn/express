var Vimeo = require('vimeo').Vimeo;
var constant = require('../config/config.json')



 const CLIENT_ID =constant.vimeo.CLIENT_ID
 const CLIENT_SECRET =constant.vimeo.CLIENT_SECRET
 const ACCESS_TOKEN =constant.vimeo.ACCESS_TOKEN
 var client = new Vimeo(CLIENT_ID, CLIENT_SECRET, ACCESS_TOKEN);

const uploadVideo = async(path) =>{
   const file_name = path
  //upload video
  console.log(path)
  var newPath = '/Users/algosofttechnologies/Desktop/myMovie.m4v'
  return new Promise((resolve,reject) =>{
    client.upload(
      file_name,
      function (uri) {
        console.log('Your video URI is: ' + uri);
        client.request(uri + '?fields=link', function (error, body, statusCode, headers) {
           if (error) {
             console.log('There was an error making the request.')
             console.log('Server reported: ' + error)
             return
           }
           else{
             console.log("body data ",body)
             console.log('Your video link is: ' + body.link)
           }
         })
      },
      function (bytes_uploaded, bytes_total) {
        var percentage = (bytes_uploaded / bytes_total * 100).toFixed(2)
        console.log(bytes_uploaded, bytes_total, percentage + '%')
      },
      function (error) {
        console.log('Failed because: ' + error)
      }
    )
  })
 


//get video url link 
// var uri = '/videos/511007113'
// client.request(uri + '?fields=link', function (error, body, statusCode, headers) {
//   if (error) {
//     console.log('There was an error making the request.')
//     console.log('Server reported: ' + error)
//     return
//   }
//   else{
//     console.log('Your video link is: ' + body.connections.pictures.uri)
//   }
  
// })



//get transcode status
// var uri = '/videos/511004798'
// client.request( uri + '?fields=transcode.status', function (error, body, status_code, headers) {
//   if (body.transcode.status === 'complete') {
   
//     console.log('Your video finished transcoding.')
//   } else if (body.transcode.status === 'in_progress') {
//     console.log('Your video is still transcoding.')
//   } else {
//     console.log('Your video encountered an error during transcoding.')
//   }
// })


//delete video
// var uri = '/videos/511007113'
// client.request({
//   method: 'DELETE',
//   path: uri,
// }, function (error, body, statusCode, headers) {
//   if (error) {
//     console.log('There was an error making the request.')
//     console.log('Server reported: ' + error)
//     return
//   }

//   console.log('VIDEO DELETED SUCCESSFULLY')

 
// })


}

module.exports ={uploadVideo}