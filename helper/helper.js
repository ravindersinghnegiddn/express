const helper = {};

var d = new Date();
var moment = require('moment');


//const nodemailer = require('nodemailer');
var ejs = require("ejs");
var promise = require('promise');
var path = require('path');
const { resolve } = require('path');
//var publicFolder = path.dirname('public');
//var environment = require('../config/environment');
//var env = require('../config/env.' + environment.mode);
//var constants = require('../config/constants');
// var transporter = nodemailer.createTransport({
// 	// host:'smtp.iluud.com',
// 	// port:587,
// 	// secure:false, // use SSL,
// 	// ignoreTLS:true,
// 	// auth: {
// 	//     user: 'info@iluud.com',
// 	// 	pass: 'Neofibre@001',
// 	// },  tls: {
// 	// 	rejectUnauthorized: false
// 	// }
// 	host: env.mail.host,
// 	port: env.mail.port,
// 	secure: false, // use SSL
// 	ignoreTLS: true,
// 	auth: {
// 		user: env.mail.username,
// 		pass: env.mail.password,
// 	}
// });


helper.isValidEmail = (email) => {
	var pattern = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
	return new RegExp(pattern).test(email);
}

helper.formatDate = (date) => {
	return moment(date).format('DD/MM/YYYY');
}

helper.getCurrentDate = (date) => {
	return moment(date).format('YYYY-MM-DD');
}

helper.compare_dates = (date1, date2) => {
	return moment(date1).isAfter(date2);
}

helper.get_sql_date = () => {
	//return moment().format('YYYY-MM-DD HH:mm:ss');
	return moment().utcOffset("+05:30").format('YYYY-MM-DD HH:mm:ss');
};

helper.next_month_date = (start_date) => {
	var eMoment = moment(start_date);
	return eMoment.add(1, 'months').format('YYYY-MM-01');
}

helper.current_month_start_date = (date) => {
	var eMoment = moment(date);
	return eMoment.format('YYYY-MM-01');
}

helper.current_month_last_date = (date) => {
	var eMoment = moment(date);
	return eMoment.format('YYYY-MM-31');
}

helper.get_current_year = () => {
	return moment().format('YYYY');
};

helper.format_mongo_data = (data) => {
	return JSON.parse(JSON.stringify(data));
};

helper.generate_url = (req) => {
	return req.protocol + '://' + req.hostname +':'+ req.connection.localPort;
}

helper.generate_url_host = (req) => {
	return req.protocol + '://' + req.hostname;
}

helper.generate_otp = () => {
	return Math.floor(Math.random() * (9999 - 1000)) + 1000;
	// return Math.floor(1000 + Math.random() * 9999);
}

helper.ucfirst = (str) => {
	return str.charAt(0).toUpperCase() + str.slice(1)
}

helper.console = (output) => {
	console.log("Requested Output statement below:");
	console.log(output);
}

helper.sendMailWithTemplate = (templatePath, emailSubject, email, dataObject) => {
	return new promise(function (resolve, reject) {
		ejs.renderFile(templatePath, dataObject, function (err, dataTemplate) {
			if (err) {
				console.log("In sendMailWithTemplate error >>");
				console.log(err);
			} else {
				var mainOptions = {
					from: "iLuud <" + env.mail.username + ">",
					to: email,
					subject: emailSubject,
					html: dataTemplate
				};

				transporter.sendMail(mainOptions, function (err, info) {
					if (err) {
						console.log("transporter.sendMail >>> ", err);
					} else {
						console.log('------------------->>>>', info)
						resolve(1);
					}
				});
			}
		});
	});
}

helper.password = () => {
	return Math.floor(Math.random() * (99999999 - 10000000)) + 10000000;
}

helper.defaultImage = (image) => {
	if (image == null || image == '') {
		newImage = publicFolder + '/images/' + constants.DEFAULT_IMAGE;
	} else {
		newImage = image;
	}
	return newImage;
}

helper.diff = (start, end) => {
	start = start.split(":");
	end = end.split(":");
	var startDate = new Date(0, 0, 0, start[0], start[1], 0);
	var endDate = new Date(0, 0, 0, end[0], end[1], 0);
	var diff = endDate.getTime() - startDate.getTime();
	var hours = Math.floor(diff / 1000 / 60 / 60);
	diff -= hours * 1000 * 60 * 60;
	var minutes = Math.floor(diff / 1000 / 60);

	// If using time pickers with 24 hours format, add the below line get exact hours
	if (hours < 0)
		hours = hours + 24;

	return (hours * 60) + minutes;

	// return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}

helper.makeString = (length) => {
	var result = '';
	var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var charactersLength = characters.length;
	for (var i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
}

helper.courseSelection = (data1) =>{
	var data = JSON.parse(JSON.stringify(data1))
	return new Promise((resolve,reject) =>{
       var totalData = []
	   var selecteddata = []
	   var finalData = []
	   data.forEach(element => {
		  var obj1 = {}
		  obj1.catId= element.category._id
		  obj1.courseId = element.course._id
		  obj1.courseName = element.course.courseName
		  obj1.categoryName = element.category.name
		  obj1.courseThumbnail = element.course.courseThumbnail
		  obj1.courseAuthor = element.course.courseAuthor
		  obj1.authorImage = element.course.authorImage
		  totalData.push(obj1)
	  });
	  console.log(totalData)
	  for(var i = 0 ;i<totalData.length ;i++){
		console.log(">>>>",totalData[i].catId)
		  var obj ={}
		  if(selecteddata.length>0){
			      
                   if(!selecteddata.includes(totalData[i].catId) && !selecteddata.includes(totalData[i].courseId)){
				     	
					   selecteddata.push(totalData[i].catId)
			           selecteddata.push(totalData[i].courseId)
					   obj.catId = totalData[i].catId
			           obj.courseId = totalData[i].courseId
					   obj.courseName = totalData[i].courseName
					   obj.categoryName = totalData[i].categoryName
					   obj.courseThumbnail = totalData[i].courseThumbnail
					   obj.courseAuthor = totalData[i].courseAuthor
					   obj.authorImage = totalData[i].authorImage
					   finalData.push(obj)
					  
				   }
				   else{
					
				   }
				 
		  }
		  else{
			console.log("last else",i)
			  var obj2 = {}
			  obj2.catId = totalData[i].catId
			  obj2.courseId = totalData[i].courseId
			  obj2.courseName = totalData[i].courseName
			  obj2.categoryName = totalData[i].categoryName
			  obj2.courseThumbnail = totalData[i].courseThumbnail
			  obj2.courseAuthor = totalData[i].courseAuthor
			  obj2.authorImage = totalData[i].authorImage
			  selecteddata.push(totalData[i].catId)
			  selecteddata.push(totalData[i].courseId)
			  finalData.push(obj2)
		  }
	  }
	 
	   resolve(finalData)



	   //************************************** */


	  
	   
	})
  
}

helper.generate_order_no = () => {
	return 'ORD#' + moment().unix();
}

helper.x = (timestamp1, timestamp2)  => {
    var difference = timestamp1 - timestamp2;
    var daysDifference = Math.floor(difference/1000/60/60/24);

    return daysDifference;
}

helper.videoSelection= (videoList1,watchList) =>{
	var result = {}
	var selectedId = []
	return new Promise((resolve,reject) =>{
        var videoList = videoList1.videoList
	    

         videoList.forEach(videoListElement =>{

			watchList.forEach(watchListElement =>{
				  if(videoListElement._id == watchListElement.videoId){
					  console.log("if")
					  selectedId.push(watchListElement.videoId)
					  console.log(selectedId)
				  }
				  else{
					  if(selectedId.includes(videoListElement._id)){
                        console.log(videoListElement._id)
					  }
					  else{
					  selectedId.push(videoListElement._id)
					  }
				  }
				 
				
			})
		 })
	     resolve(true)



	   //************************************** */


	  
	   
	})
  
}



module.exports = helper;