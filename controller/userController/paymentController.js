const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const sendResponse = require('../../util/CustomResponse')
const ObjectId = require('mongoose')
const _ = require('lodash')
const moment = require('moment')
var ip = require('ip');
const helper = require('../../helper/helper')
const paymentService = require('../../services/userServices/paymentServices')
const userService = require('../../services/userServices/userServices')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
//



const stripe = require("stripe")("sk_test_y1m0v1HOezcYvX2SA5aYj1er");
class payment 
{

constructor(){}

    calculateOrderAmount = async(items) => {
        // Replace this constant with a calculation of the order's amount
        // Calculate the order total on the server to prevent
        // people from directly manipulating the amount on the client
        return 1400;
    };
    generateToken = async(data) =>{
        return jwt.sign({id:data._id},'*sssktAf9VcK6%Pv')
    }
    createOrder = async(req,res) =>{
        try{
        let {user_id,course_id,total_price} = req.body;
        var order_no   = helper.generate_order_no();
        let orderResult = await paymentService.userCreateOrderServices({order_no,user_id,course_id,total_price})
        sendResponse.sendSuccess(orderResult, res, 1, "order created");
        }
        catch(err){
            sendResponse.sendCustomError({ "error":err }, res, 0, "error occur while creating order");
        }
    }
    paymentInit = async(req,res) => {
        client_secret = '';
        try{
        let {first_name,last_name,email,phone_number,address,country,state,pincode,purchase_type,course_id,course_price,tax,total_price,user_id,order_id,currency,amount_in_choosed_currency:amount_in_choosed_currency,browse_type,membership_price} = req.body;
        if(browse_type == 'app'){
            const paymentIntent = await stripe.paymentIntents.create({
                amount: amount_in_choosed_currency,
                currency: currency,
                description: 'Payment for orderId #'+order_id,
                shipping: {
                    name: first_name,
                    address: {
                      line1: address,
                      postal_code: pincode,
                      city: state,
                      state: state,
                      country: country,
                    },
                },
                payment_method_types: ['card'],
            });
            var status = "initiated";
            var client_secret = paymentIntent.client_secret;
        }
        
        const paymentInitResult = await paymentService.initOrderPaymentService({first_name,last_name,email,phone_number,address,country,state,pincode,purchase_type,course_id,course_price,tax,total_price,user_id,order_id,currency,amount_in_choosed_currency:amount_in_choosed_currency})
        sendResponse.sendSuccess({paymentInitResult,"client_secret":client_secret}, res, 1, "payment intent successfully");
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in payment init catch block");
            
        }
    }
    paymentFinal = async(req,res) => {
        try{
            let {user_id,order_id,status,stripeToken,customerId,captureAmount,stripeChargeId} = req.body;
            let paymentData = await commonService.getData(requiredFiles.orderModel,'single',{"user_id":user_id,"order_id":order_id})
            if(paymentData !== null && paymentData !== ''){
                var updatedata = await paymentService.updatePaymentByid({user_id,order_id,status,stripeToken,customerId,captureAmount,stripeChargeId})
                if(updatedata.nModified ==1){
                    var orderId = paymentData.order_id
                    if(orderId != null && orderId !== ''){
                        if(paymentData.purchase_type == 'membership'){
                            var params = {"is_membership_active":"Y"}
                            var userUpdate = await commonService.updateData(requiredFiles.userModel,'_id',user_id,params)
                            var subscriptionStatus = 'active';
                            sendResponse.sendSuccess({"order_id":paymentData.order_id,"user_id":paymentData.user_id,"reference_id":stripeChargeId,"status":'paid',"subscriptionStatus":subscriptionStatus}, res, 1, "payment successfull");
                        }
                        else if(paymentData.purchase_type == 'single'){
                            var courseCountUpdate = await paymentService.updateCourseByid(paymentData.course_id)  // update course enrolled count
                            var params = {"is_membership_active":"N"}
                            var userUpdate = await commonService.updateData(requiredFiles.userModel,'_id',user_id,params)
                            var subscriptionStatus = 'inactive';
                            sendResponse.sendSuccess({"order_id":paymentData.order_id,"user_id":paymentData.user_id,"reference_id":stripeChargeId,"status":'paid',"subscriptionStatus":subscriptionStatus}, res, 1, "payment successfull");
                        }
                        else{
                            sendResponse.sendCustomError({}, res, 0, "Problem in purchase type");
                        }
                        
                    }
                    else{
                        sendResponse.sendCustomError({}, res, 0, "Problem in orderId");
                    }
                    
                }
                else{
                    sendResponse.sendCustomError({}, res, 0, "unable to process payment");
                }
                
            }
            else{
                sendResponse.sendCustomError({order_id}, res, 0, "payment_id is invalid");
            }
            
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in final payment catch block"+req.body.user_id+'______'+req.body.order_id);
            
        }
    }
    
    subscriptionPaymentInit = async(req,res) => {
        try{
        let {user_id,total_price,currency} = req.body;
        const paymentIntent = await stripe.paymentIntents.create({
        amount: total_price,
        currency: currency
        });
        console.log(paymentIntent)
        var status = "initiated"
        // res.send({
        //   clientSecret: paymentIntent.client_secret
        // });
        var order_no   = helper.generate_order_no();
        const paymentInitResult = await paymentService.initSubscriptionPaymentService({user_id,total_price,currency,status,order_no})
        console.log("paymentInitResult--------------------------------->",paymentInitResult)

        res.send({
            clientSecret: paymentIntent.client_secret,
            payment_id: paymentInitResult._id,
            orderNo:paymentInitResult.orderNo,
        });
        //sendResponse.sendSuccess({clientSecret: paymentIntent.client_secret}, res, 1, "payment intent successfully");
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in subscription payment init catch block");
            
        }
    }
    subscriptionPaymentFinal = async(req,res) => {
        try{
        let {user_id,payment_id,status,stripePaymentId} = req.body;
        let paymentData = await paymentService.subscriptionPaymentDetail(payment_id)   //get payment detail by paymentId
        console.log("----------PAYMENT DETAIL---------------",paymentData)

        if(paymentData != null){
            var purchasedDate = (new Date()).getTime();
            var expiryDate = new Date();
            expiryDate.setDate(expiryDate.getDate() + 30);
            var expiryDate1 = expiryDate.getTime()
            
            var updatedata = await paymentService.updateSubscriptionPaymentByid({user_id,payment_id,status,stripePaymentId,purchasedDate,expiryDate1}) //UPDATE PAYMENT 
            if(updatedata.nModified ==1){
                sendResponse.sendSuccess({}, res, 1, "subscription payment successfull");
            }
            else{
                sendResponse.sendSuccess({}, res, 0, "unable to process payment");
            }
        }
        else{
            sendResponse.sendSuccess({}, res, 1, "payment_id is invalid");
        } 
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in final payment catch block");
            
        }
    }
    subscriptionStatus = async(req,res) => {
        try{
            var userId = req.data.user_id

            var todayDate = (new Date()).getTime();
            
            var subscriptionData = await userService.subscriptionDetail(userId,todayDate)
            console.log(userId,todayDate,subscriptionData)         
            if(subscriptionData != null && subscriptionData.status == 'paid'){
            
                var subscriptionStatus= 'active'
            }
            else{
                var subscriptionStatus= 'inactive'
            }
            sendResponse.sendSuccess({subscriptionStatus:subscriptionStatus}, res, 1, "subscription status");
                
        }
        catch(err){
            console.log(err)
            sendResponse.sendCustomError({ "error":err }, res, 0, "error in login catch block");
        }
    }
    subscriptionDetail = async(req,res) => {
        try{
            
            var subscriptionResult = await userService.allSubscriptionDetail()

            sendResponse.sendSuccess({subscriptionDetail:subscriptionResult}, res, 1, "subscription detail");

        }
        catch(err){
            console.log(err)
            sendResponse.sendCustomError({ "error":err }, res, 0, "error in subscription detail catch block");
        }
    }
}

module.exports = new payment()