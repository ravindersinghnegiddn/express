const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const sendResponse = require('../../util/CustomResponse')
const ObjectId = require('mongoose')
const _ = require('lodash')
const moment = require('moment')
var ip = require('ip');
const helper = require('../../helper/helper')
const paymentService = require('../../services/userServices/paymentServices')
const userService = require('../../services/userServices/userServices')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
const stripe = require("stripe")("sk_test_y1m0v1HOezcYvX2SA5aYj1er");
class eventPayment 
{

constructor(){}
    paymentInit = async(req,res) => {
        
        try{
            client_secret = '';
            if(req.body.browse_type == 'app'){
                const paymentIntent = await stripe.paymentIntents.create({
                    amount: req.body.amount_in_choosed_currency,
                    currency:req.body.currency,
                    description: 'Payment for orderId #'+req.body.order_id,
                    shipping: {
                        name: req.body.first_name,
                        address: {
                          line1: req.body.address,
                          postal_code: req.body.pincode,
                          city: req.body.state,
                          state: req.body.state,
                          country: req.body.country,
                        },
                    },
                    payment_method_types: ['card'],
                });
                var status = "initiated";
                var client_secret = paymentIntent.client_secret;
            }

            var paymentInsert = requiredFiles.paymentModel;
            let params = new paymentInsert({
                first_name:req.body.first_name,
                last_name:req.body.last_name,
                email:req.body.email,
                phone_number:req.body.phone_number,
                address:req.body.address,
                country:req.body.country,
                state:req.body.state,
                pincode:req.body.pincode,
                purchase_type:req.body.purchase_type,
                tax:req.body.tax,
                total_price:req.body.total_price,
                course_id:req.body.course_id,
                course_price:req.body.course_price,
                order_id:req.body.order_id,
                user_id:req.body.user_id,
                currency:req.body.currency,
                amount_in_choosed_currency:req.body.amount_in_choosed_currency,
                creation_date:Date.now()
            })
             
            const data = await commonService.insertData(params);
           
            if(data){
                sendResponse.sendSuccess({data,"client_secret":client_secret}, res, 1, "Payment Initiated");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding");
            }
            
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
            
        }
    }


    paymentFinal = async(req,res) => {

    try{
        
        const orderData = await commonService.getData(requiredFiles.paymentModel,'single',{'order_id':req.body.order_id})   //check eventId
        
        if(orderData!==null)
        {
            let params = {
                status:req.body.status,
                stripeToken:req.body.stripeToken,
                customerId:req.body.customerId,
                captureAmount:req.body.captureAmount,
                stripeChargeId:req.body.stripeChargeId,  
                update_date:Date.now()
            }
                const paymentData = await commonService.updateData(requiredFiles.paymentModel,'order_id',req.body.order_id,params)  //update events
        
                if(paymentData){
                    sendResponse.sendSuccess({paymentData}, res, 1, "Payment Done");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while Payment");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "orderId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update catch block");
        
    }
}

}

module.exports = new eventPayment()
 