const userService = require('../../services/userServices/userServices')
const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const sendResponse = require('../../util/CustomResponse')
const userServices = require('../../services/userServices/userServices')
const { ObjectId } = require('mongodb');
const _ = require('lodash')
const moment = require('moment')
var ip = require('ip');
const helper = require('../../helper/helper')
const { updateOrderByid } = require('../../services/userServices/paymentServices')
const requiredFiles = require('../../database/required');
const commonService = require('../../services/userServices/commonService')
const emailService = require('../../services/userServices/email_template_service')
const imageService = require('../../services/adminServices/imageServices')
const { request } = require('express')
var nodemailer = require('nodemailer');
class user {

constructor(){}

userSignup = async(req,res) => {
     
    try{
        const{firstName,email,password,categories,questions}= req.body
        const hashPassword = sha256(password)
        let userData = {firstName,email,hashPassword}
        if((typeof(categories)) == 'string'){
            var categoryArray = JSON.parse(categories) 
            console.log("categoryArray---------------->",categoryArray)    
        }
        if((typeof(questions)) == 'string'){
            var questionArray = JSON.parse(questions)   
            console.log("questionArray---------------->",questionArray)    
        }
        var data = await userService.userSignupServices(userData)  //insert data in user Collections
        if(data._id !=null)
        {   
            var user = data._id
            var userCategory = {user,categoryArray}
            var categoryData = await userService.userAddCategoryServices(userCategory) //insert data into user_category collection
            if(categoryData._id !=null)
            {
               var userQuestion = {user,questionArray}
               var questionData = await userService.userAddQuestionServices(userQuestion) //insert data into question category collection
               if(questionData._id != null){
                    sendResponse.sendSuccess({}, res, 1, "signup successfully"); 
                }
               else
                {
                    sendResponse.sendCustomError({ "error":err }, res, 0, "error occur while inserting data into user question collection"); 
                }
            }
            else{

                sendResponse.sendCustomError({ "error":err }, res, 0, "error occur while inserting data into user category collection");
            }
        }
        else
        {
            sendResponse.sendCustomError({ "error":err }, res, 0, "error occur while  inserting data into user collection");
        }

    }
    catch(err){
    console.log(err)
      //res.send({"status":"false","message":"error while inserting data ","error":err.message})
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in user signup catch block");
    }
}

login = async(req,res) => {
    try{
        
        var {email,password,deviceToken,deviceIp,lat,long,deviceType}= req.body
        const hashPassword = sha256(password)
        let userData = {email,hashPassword}
        
        var data = await userService.userLoginServices(userData)
        //console.log("data----",data)  
       
       
        if(data){
           var updateData = {}
           var token = await this.generateToken(data)
          
           var subscriptionData = await userService.subscriptionDetail(data._id)
           
           var todayDate = (new Date()).getTime();
           
           if(data.is_membership_active == 'Y'){
           
             var subscriptionStatus= 'active'
           }
           else{
            var subscriptionStatus= 'inactive'
           }
           updateData.token = token
           updateData.deviceToken = deviceToken
           updateData.deviceIp = deviceIp
           updateData.lat = lat
           updateData.long = long  
           updateData.deviceType=deviceType
           updateData.updatedAt = Date.now() 
           console.log(updateData)
           //update user set token
           
           var result = await userService.updateUser(data._id,updateData)
           
           if(result){    
           sendResponse.sendSuccess({data,token,subscriptionStatus}, res, 1, "login success");
           }
           else{
            sendResponse.sendCustomError('unable to update token', res, 0, "internal server error..");
           }
        }
        else{
            
            sendResponse.sendCustomError({}, res, 0, "invalid credential");
        }
    }
    catch(err){
        console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in login catch block");
    }
}

getCategoryList = async(req,res) => {

    try{
         console.log(req.data)
        const data = await userService.listCategory()
        
        if(data){
       
        sendResponse.sendSuccess({"categoryList":data}, res, 1, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 1, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in listCategory catch block");
        
    }

}

getQuestionList = async(req,res) => {
    try{
         
        const data = await userService.listQuestion()
      
        if(data)
        {
            
            sendResponse.sendSuccess({"questionList":data}, res, 1, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 1, "question list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in listquestion catch block");
        
    }
}

getQuestionList1 = async(req,res) => {
    try{
     
        
        var a = req.body.categoryId.replace(/'/g, '"');
        var categoryArray = JSON.parse(a);
        if(categoryArray.length<3){
            sendResponse.sendSuccess('', res, 0, "at least 3 question are required");
        }
      else{
        const data = await userService.listQuestion1(categoryArray)
        console.log(data)
        if(data)
        {
            
            sendResponse.sendSuccess({"questionList":data}, res, 1, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question list fetched  successfully");
        }   
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in listquestion catch block");
        
    }
}

getQuestionList2 = async(req,res) => {
    try{
     
        
        var a = req.body.categoryId.replace(/'/g, '"');
        var categoryArray = JSON.parse(a);
        if(categoryArray.length<3){
            sendResponse.sendSuccess('', res, 0, "at least 3 question are required");
        }
      else{
        var data = await userService.listQuestion1(categoryArray)
      
        if(data)
        {
            var result = []
            data.forEach(element => {
                var obj ={}
                if(element.category !=null){
                    console.log("element -------------",element)
                  obj.categoryId = element.category._id
                  obj.categoryName = element.category.name
                  obj.question = element.question
                  obj.questionId = element._id
                  obj.subQuestion = element.category.title
                  result.push(obj)
                }
            });
            //console.log(">>>>>",data)
            // var grouped = _.mapValues(_.groupBy(result, 'categoryName'),
            //               clist => clist.map(results => _.omit(results, 'categoryName')));

            // console.log(grouped);
            sendResponse.sendSuccess({"questionList":result}, res, 1, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question list fetched  successfully");
        }   
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in listquestion catch block");
        
    }
}

//new signup
userSignup1 = async(req,res) => {
     
    try{
        var {firstName,email,password,categories,questions,deviceIp,deviceToken,deviceType,lat,long}= req.body
        const hashPassword = sha256(password)

        if((typeof(categories)) == 'string'){
            var categoryArray = JSON.parse(categories) 
            console.log("categoryArray---------------->",categoryArray)    
        }
        if((typeof(questions)) == 'string'){
            var questionArray = JSON.parse(questions)   
            console.log("questionArray---------------->",questionArray)    
        }
        let userData = {firstName,email,hashPassword,categoryArray,questionArray}

        var data = await userService.userSignupServices(userData)  //insert data in user Collections
        var data1 = JSON.parse(JSON.stringify(data))
        delete data1.password
        console.log(data1)

         if(data._id !=null)
        {   
           var updateData ={}
           var token = await this.generateToken(data)
           updateData.token = token
           updateData.deviceToken = deviceToken
           updateData.deviceIp = deviceIp
           updateData.deviceType=deviceType
           updateData.lat = lat
           updateData.long = long  
           updateData.updatedAt = Date.now()
           console.log(updateData)   
           //update user set token
           var result = await userService.updateUser(data._id,updateData)


           var sendmail = await emailService.sendMailToUserForSignUp(firstName,email)

           sendResponse.sendSuccess({data:data1,token:token}, res, 1, "signup successfully"); 
                    
        }
        else
        {
            sendResponse.sendCustomError({ "error":err }, res, 0, "error occur while  inserting data into user collection");
        }

    }
    catch(err){
    console.log(err)
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in user signup catch block");
    }
}

homePage= async(req,res) => {
    try{
        
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var categoryData = await userService.userGetCategoryArray(req.data.user_id)
        if(categoryData[0].selectedCategory.length > 0){
                
                var searchControl={} 
                searchControl={
                    $or:[
                         {name:{ $regex: '.*' + search_value+ '.*' }},
                         {title:{ $regex: '.*' + search_value + '.*' }}
                        ]
                 }
                const pageControl = {skip1,requested_count1} 
                const selectedCategoryProgram  = await userService.selectedCategoryCourse3(searchControl,pageControl,categoryData[0].selectedCategory)
                
                if(selectedCategoryProgram){
                   const selectedDataNew = await helper.courseSelection(selectedCategoryProgram)
                   const allProgram  = await userService.allProgram()
                   sendResponse.sendSuccess({selectedCategoryProgram,selectedDataNew,allProgram}, res, 1, "course list fetched  successfully");
                }
                else{
                    sendResponse.sendSuccess({selectedCategoryCourse :"no data found"}, res, 0, "course list fetched  successfully");
                } 
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "user has not selected any category");
        }
         
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}
courseList= async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = parseInt(requested_count)  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var search = search_value ? search_value : ""
        var searchControl={} 
        searchControl={
            $and:[
                {course_type:"Programmes",'course_details_updated':'Y'},
              
               ],
                    $or:[
                         {courseName:{ $regex: '.*' + search+ '.*' }},
                       
                        ]
                 }
        const pageControl = {skip1,requested_count1} 
       
        const allProgram  = await userService.listCourse(searchControl,pageControl)
        console.log(allProgram)
        sendResponse.sendSuccess({allProgram}, res, 0, "course list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in course List catch block");
        
    }
}

paidMasterClass= async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = parseInt(requested_count)  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var search = search_value ? search_value : ""
        var searchControl={} 
        searchControl={
            $and:[
                {course_type:"Paid live master class",'coursePrice':{$gt:0}},
              
               ],
                    $or:[
                         {courseName:{ $regex: '.*' + search+ '.*' }},
                       
                        ]
                 }
        const pageControl = {skip1,requested_count1} 
       
        const paidMasterClass  = await userService.listCourse(searchControl,pageControl)
        console.log(paidMasterClass)
        sendResponse.sendSuccess({paidMasterClass}, res, 0, "course list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in course List catch block");
        
    }
}

//user purchased course List
purchasedCourseList= async(req,res) => {
    try{
        let { userId,page_no, requested_count, } = req.body
       // console.log(req.data)
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
                var searchControl={} 
                searchControl={
                    $and:[
                    {user_id:req.data.user_id},
                    {status:"paid"},  
                    {purchase_type:"single"}
                    ]
                 }
                const pageControl = {skip1,requested_count1} 
                //console.log(searchControl,pageControl)
                const allProgram  = await userService.listPurchasedCourse(searchControl,pageControl)
                //console.log('>>>>>>>>>>>>>>>>>>'+allProgram);
                if(allProgram !=null){
                    var dataList = []
                    //console.log(allProgram.purchasedProgram)
                    allProgram.purchasedProgram.forEach(element =>{
                        var obj = {}
                        obj.status = element.status
                        obj.courseId = element.course_id._id
                        obj.courseName = element.course_id.courseName
                        obj.courseThumbnail = element.course_id.courseThumbnail
                        obj.courseAuthor = element.course_id.courseAuthor
                        //console.log(element.course_id.courseName)
                        dataList.push(obj)
                    })
                }
                sendResponse.sendSuccess({purchasedProgram:dataList}, res, 1, "purchased course list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in purchased course list catch block");
        
    }
}

viewCourse = async(req,res) => {
    try{
       
        if(!req.body.id){
            sendResponse.sendSuccess('', res, 0, "please provide courseId");
        }
        else{ 
        var purchasedCourse = await userService.viewPurchasedCourse(req.body.id,req.data.user_id)
        console.log("------------------purchasedcoursedata-------------",purchasedCourse)
        var data = await userService.viewCourse(req.body.id)
        
        if(data){
            if(purchasedCourse !==null && (purchasedCourse.course_id ==data._id)){
                data.isPurchased = 1
            }
            else{
                data.isPurchased =0
            }
            sendResponse.sendSuccess({courseData:data}, res, 0, "course detail fetched  successfully");
            }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "course detail fetched  successfully");
        }  
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}

generateToken = async(data) =>{
    return jwt.sign({id:data._id},'*sssktAf9VcK6%Pv')
}

getProfile = async(req,res) => {
    try{
        var userData = await userService.userProfileServices(req.query.userId)
        //console.log("data----",userData)  
        if(userData){
            sendResponse.sendCustomError(userData, res, 1, "user data fetched successfully");
        }
        else{ 
            sendResponse.sendCustomError({}, res, 1, "unable to fetch data");
        }
    }
    catch(err){
        console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in login catch block");
    }
}

getAboutUs = async(req,res) => {
    try{
        var aboutUsData = await userService.getAboutUs()
       
        
        if(aboutUsData){
            var aboutUs  = aboutUsData.data
            sendResponse.sendSuccess({aboutUs}, res, 1, "about Us data fetched successfully");
        }
        else{ 
            sendResponse.sendCustomError({}, res, 1, "unable to fetch data");
        }
    }
    catch(err){
        console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in user about us catch block");
    }
}

updateProfile = async(req,res) => {
     
    try{
        const {firstName,lastName,title,city,industry,aboutMe,website,facebook,twitter,linkedin,phone_number,address,country,state,pincode,shortDescription} = req.body
        console.log(req.files);
        
        //let userData = {firstName,email,hashPassword,categoryArray,questionArray}
        if(req.files){
            if(req.files.profileImg.tempFilePath){
                var profile_img = await imageService.uploadCloudinary(req.files.profileImg.tempFilePath);
            }
        }
        var updateData ={}
        updateData.firstName = firstName
        updateData.lastName= lastName,
        updateData.profileImg = profile_img
        updateData.title = title
        updateData.city = city
        updateData.country = country
        updateData.industry= industry
        updateData.aboutMe= aboutMe
        updateData.website= website
        updateData.facebook= facebook
        updateData.twitter= twitter
        updateData.linkdIn= linkedin
        updateData.phone_number=phone_number,
        updateData.address=address,
        updateData.state=state,
        updateData.pincode=pincode,
        updateData.shortDescription=shortDescription,
        updateData.updatedAt =Date.now()
           
        var result = await userService.updateUser(req.data.user_id,updateData)
        if(result){    
            sendResponse.sendSuccess({}, res, 1, "profile updated successfully");
            }
            else{
             sendResponse.sendCustomError('unable to update profile', res, 0, "internal server error..");
            }
    }
    catch(err){
    console.log(err)
      //res.send({"status":"false","message":"error while inserting data ","error":err.message})
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in update profile catch block");
    }
}
viewPurchasedCourseVideo = async(req,res) => {
    try{
       
        if(!req.body.id){
            sendResponse.sendSuccess('', res, 0, "please provide courseId");
        }
        else{ 
        var purchasedCourse = await userService.viewPurchasedCourse(req.body.id)   //get purchased course  detail by id
        console.log(purchasedCourse)
       
        if(purchasedCourse !==null){
       
        sendResponse.sendSuccess({courseData:data}, res, 0, "course detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "this course is not purchased");
        }  
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}

newsList= async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var searchControl={} 
        searchControl={
                    $or:[
                         {newsHeadline:{ $regex: '.*' + search_value+ '.*' }},
                       
                        ]
                 }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)
        const allNews  = await userService.listNews(searchControl,pageControl)
        sendResponse.sendSuccess({allNews}, res, 0, "news list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list news catch block");
        
    }
}

viewNews = async(req,res) => {
    try{
        if(!req.body.id){
            sendResponse.sendSuccess('', res, 0, "please provide news id");
        }
        else{ 
        var newsResult = await userService.viewNews(req.body.id)   //get purchased course  detail by id
        
       
        if(newsResult !==null){
        var relatedNews = await userService.viewNewsRelated(req.body.id)   //get purchased course  detail by id
        sendResponse.sendSuccess({newsResult:newsResult,relatedNews}, res, 0, "news detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "news detail fetched  successfully");
        }  
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}
listInstructor = async(req,res) => {
    try
    { 
         let { page_no, requested_count,  search_value } = req.body
         

         var requested_count1 = requested_count ? parseInt(requested_count) : 10  
         var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
         var instructor_id = req.body.instructorId;
         var searchControl={} 
            searchControl={
                $or:[
                    {firstName:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }},
                    {lastName:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1}
        var result = await userService.listInstructor(pageControl,searchControl,instructor_id)
        
        if(result){
           
           sendResponse.sendSuccess(result, res, 1, "instructor list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no user found"}, res, 1, "instructor list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list instructor (user controller)) catch block");
    }
}

InstructorCourseList= async(req,res) => {
    try{
        let { page_no, requested_count,  search_value ,instructorId} = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var searchControl={} 
        searchControl={
            $and:[
                {isAdmin:false},
              
               ],
                    $or:[
                         {courseName:{ $regex: '.*' + search_value+ '.*' }},
                       
                        ]
                 }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)
        const instructorPrograms  = await userService.listInstructorCourse(searchControl,pageControl,instructorId)
        sendResponse.sendSuccess(instructorPrograms, res, 1, " Instructor course list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Instructor course List catch block");
        
    }
}

listEvent = async(req,res) => {
    try
    { 
        let { page_no, requested_count,  search_value,instructorId } = req.body
         

         var requested_count1 = requested_count ? parseInt(requested_count) : 10  
         var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
         var instructor_id = req.body.instructorId;
         var searchControl={} 
            searchControl={
                $or:[
                    {firstName:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }},
                    {lastName:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1}
        var result = await userService.listEvent(pageControl,searchControl,instructor_id)
        
        if(result){
           
           sendResponse.sendSuccess(result, res, 1, "event list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no event found"}, res, 1, "event list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list event (user controller)) catch block");
    }
}

viewEvent = async(req,res) => {
    try
    { 
         let {id} = req.body
         
        var result = await userService.viewEvent({id})
        
        if(result){
           console.log(">>>>>>>>>>>>>>>>>>>>>>",typeof(result))
           sendResponse.sendSuccess({eventDetails:result}, res, 1, "event detail fetched  successfully");
        }
        else{
            var error = "abcd"
            sendResponse.sendSuccess({eventDetails:error}, res, 1, "event details fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in  event details (user controller)) catch block");
    }
}

listEventInstructor = async(req,res) => {
    try
    { 
         let { page_no,requested_count,search_value,instructorId } = req.body
         

         var requested_count1 = requested_count ? parseInt(requested_count) : 10  
         var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
         var searchControl={} 
            searchControl={
                $or:[
                    {firstName:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }},
                    {lastName:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1}
        var result = await userService.listEventInstructor(pageControl,searchControl,instructorId)
        
        if(result){

           sendResponse.sendSuccess(result, res, 1, "event list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no event found"}, res, 1, "event list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list event instructor (user controller)) catch block");
    }
}

viewEventInstructor = async(req,res) => {
    try
    { 
         let {id} = req.body
         
        var result = await userService.viewEventInstructor({id})
        
        if(result){
           console.log(">>>>>>>>>>>>>>>>>>>>>>",typeof(result))
           sendResponse.sendSuccess({eventDetails:result}, res, 1, "event detail fetched  successfully");
        }
        else{
            var error = "abcd"
            sendResponse.sendSuccess({eventDetails:error}, res, 1, "event details fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in  event details instructor (user controller)) catch block");
    }
}

/* * *********************************************************************
* * Function name : changePassword
* * Developed By : Tejaswi
* * Purpose  : This function use for home page
* * Date : 20 MAY 2021
* * **********************************************************************/

putChangePassword = async(req,res) => {
     
    console.log(req.body);
    try{
        const {password,user_id} = req.body
         
        var updateData ={}
        updateData.password = sha256(password)
        updateData.updatedAt =Date.now()
           
        var result = await userService.updateUser(user_id,updateData)
        if(result){    
            sendResponse.sendSuccess({}, res, 1, "password changed.");
            }
            else{
             sendResponse.sendCustomError('unable to change password', res, 0, "internal server error..");
            }
    }
    catch(err){
    console.log(err)
      //res.send({"status":"false","message":"error while inserting data ","error":err.message})
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in password catch block");
    }
}

 /* * *********************************************************************
	 * * Function name : getPurchaseHistory
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

 getPurchaseHistory = async(req,res) => {
            
    var purchaseHistory    = await commonService.getData(requiredFiles.orderModel,'multiple','user_id',req.body.user_id);
   
    sendResponse.sendSuccess({purchaseHistory}, res, 1, "Data fetched successfully.");
}

/* * *********************************************************************
* * Function name : paidMasterClassDetails
* * Developed By : Tejaswi
* * Purpose  : This function use for home page
* * Date : 13 MAY 2021
* * **********************************************************************/

paidMasterClassDetails = async(req,res) => {
    try{

        if(_.isEmpty(req.body.course_id)){
            sendResponse.sendCustomError({}, res, 0, "Please Enter Course Id");
        }
        else{
            var course_id             =     req.body.course_id;
            //console.log(course_id);
            const courseData          =     await commonService.getData(requiredFiles.coursesModel,'single',{"_id":ObjectId(course_id)});
            if(courseData._id !== null){
                const courseDetails       =     await commonService.getData(requiredFiles.coursesDetails,'single',{"course_id":course_id});
                if(courseDetails._id !== ''){
                    var whereCon = {
                        course_id:req.body.course_id,
                        user_id:req.body.user_id,
                        purchase_type:"event",
                    };
                    const getPaymentData      =     await commonService.getDataByWhereCondition(requiredFiles.orderModel,'single','','',whereCon);
                    
                    sendResponse.sendSuccess({courseData,courseDetails,getPaymentData}, res, 1, "Data fetched successfully.");
                }
                else{
                    sendResponse.sendCustomError({ "error":err }, res, 0, "No data found");
                }
            }
            else{
                sendResponse.sendCustomError({ "error":err }, res, 0, "Invalid Course Id");
            }
            
        }
        
    }
    catch(err){
          console.log(err)
          sendResponse.sendCustomError({ "error":err }, res, 0, "error in catch block");
        }
}

freeMasterClass= async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = parseInt(requested_count)  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var search = search_value ? search_value : ""
        var searchControl={} 
        searchControl={
            $and:[
                {course_type:"Paid live master class",'coursePrice':0},
              
               ],
                    $or:[
                         {courseName:{ $regex: '.*' + search+ '.*' }},
                       
                        ]
                 }
        var searchControl1={} 
        searchControl1={
            $and:[
                {course_type:"Free master class",'coursePrice':0},
            
            ],
                    $or:[
                        {courseName:{ $regex: '.*' + search+ '.*' }},
                    
                        ]
                }
        const pageControl = {skip1,requested_count1} 
       
        const freeMasterClass  = await userService.listCourse(searchControl1,pageControl)
        const freeLiveMasterClass  = await userService.listCourse(searchControl,pageControl)
        console.log(freeMasterClass)
        sendResponse.sendSuccess({freeMasterClass,freeLiveMasterClass}, res, 0, "course list fetched  successfully");
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in course List catch block");
        
    }
}

/* * *********************************************************************
* * Function name : freeMasterClassDetails
* * Developed By : Tejaswi
* * Purpose  : This function use for home page
* * Date : 13 MAY 2021
* * **********************************************************************/

freeMasterClassDetails = async(req,res) => {
    try{

        if(_.isEmpty(req.body.course_id)){
            sendResponse.sendCustomError({}, res, 0, "Please Enter Course Id");
        }
        else{
            var course_id             =     req.body.course_id;
            //console.log(course_id);
            const courseData          =     await commonService.getData(requiredFiles.coursesModel,'single',{"_id":ObjectId(course_id)});
            if(courseData._id !== null){
                const courseDetails       =     await commonService.getData(requiredFiles.coursesDetails,'single',{"course_id":course_id});
                if(courseDetails._id !== ''){
                    var whereCon = {
                        course_id:req.body.course_id,
                        user_id:req.body.user_id,
                        purchase_type:"event",
                    };
                    
                    sendResponse.sendSuccess({courseData,courseDetails}, res, 1, "Data fetched successfully.");
                }
                else{
                    sendResponse.sendCustomError({ "error":err }, res, 0, "No data found");
                }
            }
            else{
                sendResponse.sendCustomError({ "error":err }, res, 0, "Invalid Course Id");
            }
            
        }
        
    }
    catch(err){
          console.log(err)
          sendResponse.sendCustomError({ "error":err }, res, 0, "error in catch block");
        }
}

}

module.exports = new user()