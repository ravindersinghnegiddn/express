const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var nodemailer = require('nodemailer');
const { ObjectId } = require('mongodb');
class webCmsController{
      constructor(){}

 
      /* * *********************************************************************
	 * * Function name : setAgoraDetails
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      setAgoraDetails = async(req,res) => {
            
            var getCourseData = await commonService.getData(requiredFiles.coursesModel,'single',{'alias':req.body.course_id,'eventStatus':{$ne:'completed'}});
            
            if(getCourseData !== null && getCourseData._id !== null){
                  var agora = requiredFiles.agoraModel
                  let params = new agora({
                  channel_name : req.body.channel_name,
                  token : req.body.token,
                  instructor_id : req.body.instructor_id,
                  course_id : getCourseData._id,
                  creation_date : req.body.creation_date,
                  })
                  var agoraData = await commonService.insertData(params);
                  if(agoraData !== 'null'){
                        var Uparams = {
                              eventStatus : 'started',
                        }
                        await commonService.updateData(requiredFiles.coursesModel,'_id',ObjectId(getCourseData._id),Uparams);
                        var courseData = await commonService.getData(requiredFiles.coursesModel,'single',{'_id':getCourseData._id})
                  }
                  sendResponse.sendSuccess({agoraData,courseData}, res, 1, "Inserted Successfully.");
            }
            else{
                  sendResponse.sendCustomError({}, res, 0, "This event is already completed");
            }
       }

        /* * *********************************************************************
	 * * Function name : updateEventStatus
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      updateEventStatus = async(req,res) => {
            
            var getCourseData = await commonService.getData(requiredFiles.coursesModel,'single',{'alias':req.body.course_id});
            //console.log('????????????'+req.body.course_id)
            if(getCourseData !== 'null'){
                  var Uparams = {
                        eventStatus : 'completed',
                  }
                  await commonService.updateData(requiredFiles.coursesModel,'_id',ObjectId(getCourseData._id),Uparams);
            }
            sendResponse.sendSuccess({getCourseData}, res, 1, "Inserted Successfully.");
       }

      /* * *********************************************************************
	 * * Function name : getAgoraDetails
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getAgoraDetails = async(req,res) => {

            var getCourseData = await commonService.getData(requiredFiles.coursesModel,'single',{'alias':req.body.course_id,'eventStatus':'started'});
           
           if(getCourseData !== null && getCourseData._id !== null){
                  if(getCourseData.course_type == 'Free master class' || getCourseData.coursePrice == 0){
                        var getAgoraData = await commonService.getData(requiredFiles.agoraModel,'multiple',{'course_id':ObjectId(getCourseData._id),'instructor_id':getCourseData.instructorId},'limit');
                        sendResponse.sendSuccess({getAgoraData}, res, 1, "Fetched  Successfully.");
                  }
                  else if(getCourseData.course_type == 'Paid live master class' && getCourseData.coursePrice == 0){
                        var getAgoraData = await commonService.getData(requiredFiles.agoraModel,'multiple',{'course_id':ObjectId(getCourseData._id),'instructor_id':getCourseData.instructorId},'limit');
                        sendResponse.sendSuccess({getAgoraData}, res, 1, "Fetched  Successfully.");
                  }
                  
                  else{
                        var getpaymentData = await commonService.getData(requiredFiles.paymentModel,'single',{'course_id':getCourseData._id,'status':'paid','user_id':req.body.user_id});
                        
                        if(getpaymentData !== null && getpaymentData !== ''){
                              var getAgoraData = await commonService.getData(requiredFiles.agoraModel,'multiple',{'course_id':ObjectId(getCourseData._id),'instructor_id':getCourseData.instructorId},'limit');
                              sendResponse.sendSuccess({getAgoraData}, res, 1, "Fetched  Successfully.");
                        }
                        else{
                              sendResponse.sendCustomError({}, res, 0, "You are not eligible for this event");
                        }   
                  }
                  
           }
           else{
                  sendResponse.sendCustomError({}, res, 0, "This event is already completed or you are not eligible for this event");
            }                                               
           
       }

       /* * *********************************************************************
	 * * Function name : sendMessage
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      sendMessage = async(req,res) => {
            
            var getCourseData = await commonService.getData(requiredFiles.coursesModel,'single',{'alias':req.body.course_id});
            
            var agora = requiredFiles.liveChatModel
            let params = new agora({
                channel_name : req.body.channel_name,
                user_id : req.body.user_id,
                user_name : req.body.user_name,
                course_id : getCourseData._id,
                message : req.body.message,
            })
            var agoraData = await commonService.insertData(params);
            if(agoraData !== 'null'){
                  var getChatdata = await commonService.getData(requiredFiles.liveChatModel,'multiple',{'channel_name':req.body.channel_name,'course_id':getCourseData._id});
                  var eventStatus = getCourseData.eventStatus;
            }
            sendResponse.sendSuccess({getChatdata,eventStatus}, res, 1, "Message Sent.");
       }

       /* * *********************************************************************
	 * * Function name : getChatData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getChatData = async(req,res) => {
            
            var getCourseData = await commonService.getData(requiredFiles.coursesModel,'single',{'alias':req.body.course_id});
            
            if(getCourseData !== 'null'){
                  var getChatdata = await commonService.getData(requiredFiles.liveChatModel,'multiple',{'channel_name':req.body.channel_name,'course_id':getCourseData._id});
                  var eventStatus = getCourseData.eventStatus;
            }
            sendResponse.sendSuccess({getChatdata,eventStatus}, res, 1, "Recieved Data.");
       }

}
module.exports = new webCmsController()