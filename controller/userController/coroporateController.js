const sendResponse = require('../../util/CustomResponse')
const corporateModel = require('../../services/userServices/corporateService')
const corporateCmsService = require('../../services/userServices/corporateCmsService')
const uspService = require('../../services/userServices/uspService')
const solutionsService = require('../../services/userServices/solutionsService')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
class corporateController{
    constructor(){}

    getCorporatePageData = async(req,res) => {
        var corporate_banners = await corporateModel.getCorporateBanner() 
        var corporate_headings = await corporateCmsService.getCorporateCms() 
        var our_usp = await uspService.getUspData()
        var solutions = await solutionsService.getSolutionData()
        var getFounderallData= await commonService.getData(requiredFiles.aboutUsWebModel,'single');
        if(getFounderallData != ""){
            var FounderData    = {
                  "home_about_founder_title":getFounderallData.home_about_founder_title,
                  "home_about_founder_description":getFounderallData.home_about_founder_description,
                  "home_about_founder_image":getFounderallData.home_about_founder_image
            }
      }
       sendResponse.sendSuccess({corporate_banners,corporate_headings,our_usp,solutions,FounderData}, res, 1, "Data fetched successfully.");
    }
}
module.exports = new corporateController()