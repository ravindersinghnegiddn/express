const userService = require('../../services/userServices/userServices')
const admin= require('../../services/adminServices/adminServices')
const videoService = require('../../services/userServices/videoService')
const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

const helper = require('../../helper/helper')
class  video{

constructor(){}


viewPurchasedCourseVideo = async(req,res) => {
    try{
        let {page_no,id,requested_count,search_value} = req.body
        if(!req.body.id){
            sendResponse.sendSuccess('', res, 0, "please provide courseId");
        }
        else{ 
            var purchasedCourse = await videoService.viewPurchasedCourse(req.body.id,req.data.user_id)   //get purchased course  detail by id
            console.log(purchasedCourse)
            if(true){
                
                var requested_count1 = requested_count ? parseInt(requested_count) : 10  
                var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
                var searchControl={} 
                searchControl={
                            $and:[
                            {courseId:id}  
                            ]
                         }
                const pageControl = {skip1,requested_count1} 
                console.log(searchControl,pageControl)
                var videoList = await videoService.listPurchasedCourseVideo(searchControl,pageControl)
                sendResponse.sendSuccess(videoList, res, 1, "purchased video list fetched  successfully");
        }
         
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view purchased video catch block");
        
    }
}



viewPurchasedCourseVideoDetails = async(req,res) => {
    try
    {
        let {videoId,userId} = req.body
        if(!videoId){
            sendResponse.sendSuccess('', res, 0, "please provide video Id");
        }
        else
        { 
            var videoDetails = await videoService.viewVideoDetails1(videoId,userId)   //get purchased course  detail by id
            console.log(videoDetails)
            if(videoDetails){
                
                sendResponse.sendSuccess(videoDetails, res, 1, "purchased video details fetched  successfully");
            }
            else{
            sendResponse.sendSuccess({lastSeen:0}, res, 1, "purchased video details fetched  successfully");
            }  
        } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view purchased video detail catch block");
        
    }
}




viewPurchasedCourseVideo1 = async(req,res) => {
    try{
        let {page_no,id,requested_count,search_value} = req.body
        if(!req.body.id){
            sendResponse.sendSuccess('', res, 0, "please provide courseId");
        }
        else{ 
            var purchasedCourse = await videoService.viewPurchasedCourse(req.body.id,req.data.user_id)   //get purchased course  detail by id
            console.log(purchasedCourse);
            if(true){
                
                var requested_count1 = requested_count ? parseInt(requested_count) : 10  
                var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
                var searchControl={} 
                searchControl={
                            $and:[
                            {courseId:id}  
                            ]
                         }
                const pageControl = {skip1,requested_count1} 
               
                var videoList = await videoService.listPurchasedCourseVideo(searchControl,pageControl)
                var watchCourseList = await videoService.listWatchedVideo(req.data.user_id)
                const videoListNew= await helper.videoSelection(videoList,watchCourseList)
               
 
                console.log("videolist",videoList)
                var result = videoList.videoList
                var grouped = _.mapValues(_.groupBy(result, 'sectionNumber'),
                          clist => clist.map(results => _.omit(results, 'x')));

                var a = _.map(
                        _.groupBy(result, elem => elem.sectionNumber), 
                            (vals, key) => {
                                return { section: vals}
                            }
                        )  
                  var resultData ={}
                  resultData.coursevideo = a          

                    
                sendResponse.sendSuccess(resultData, res, 1, "purchased video list fetched  successfully");
        }
         
    } 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view purchased video catch block");
        
    }
}












generateToken = async(data) =>{
    return jwt.sign({id:data._id},'*sssktAf9VcK6%Pv')
}

updateVideoLastSeen = async(req,res) => {
     
    try{
        let {userId,videoId,lastSeen} = req.body;
         
 
        var updateData ={}
        updateData.lastSeen = lastSeen
        updateData.updatedAt =Date.now()
           
        var result = await videoService.updateLastSeen(userId,videoId,updateData)
        if(result){    
            sendResponse.sendSuccess({}, res, 1, "video last seen time updated successfully");
            }
            else{
             sendResponse.sendCustomError('unable to update last seen ', res, 0, "internal server error..");
            }
    }
    catch(err){
    console.log(err)
      //res.send({"status":"false","message":"error while inserting data ","error":err.message})
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in update last seen catch block");
    }
}





}

module.exports = new video()