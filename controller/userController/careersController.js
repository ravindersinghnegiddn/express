const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
const imageService = require('../../services/adminServices/imageServices')
const _ = require('lodash')
const ObjectId = require('mongoose')

class careers{
    constructor(){}

    /* * *********************************************************************
	 * * Function name : getCareersAbout
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for Career About
	 * * Date : 24 MAY 2021
	 * * **********************************************************************/

    getCareersAbout = async(req,res) => {
        
        var title = req.body.title;
        var location = req.body.location;
        var whereCon = {};
        
        if(title){
            whereCon['_id'] = title;
        }
        if(location){
            whereCon['location'] = location;
        }
        console.log(whereCon)
        var getCareersAbout = await commonService.getData(requiredFiles.careerModel,'single');
        var jobOpenings = await commonService.getData(requiredFiles.jobOpeningModel,'multiple',whereCon);
        sendResponse.sendSuccess({getCareersAbout,jobOpenings}, res, 1, "Data fetched successfully.");
       }

    /* * *********************************************************************
	 * * Function name : getJobOpeningDetails
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for Career About
	 * * Date : 24 MAY 2021
	 * * **********************************************************************/

    getJobOpeningDetails = async(req,res) => {
        
        var job_slug = req.body.job_slug;
        var jobOpenings = await commonService.getDataByWhereCondition(requiredFiles.jobOpeningModel,'single','job_slug',job_slug);
        sendResponse.sendSuccess({jobOpenings}, res, 1, "Data fetched successfully.");
       }

    /* * *********************************************************************
	 * * Function name : putJobForm
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for Career About
	 * * Date : 24 MAY 2021
	 * * **********************************************************************/

    putJobForm = async(req,res) => {
        
        //let jobData = {job_id,first_name,last_name,email,phone,highest_education,from_year,to_year,linkedin_profile,cover_letter_link,how_do_you_hear_about_this,why_bliss,portfolio,current_ctc,expected_ctc,notice_period,current_location,ready_to_relocate};
        if(_.isEmpty(req.body.job_id)){
            sendResponse.sendCustomError({}, res, 0, "Please Enter Job Id");
        }
        else if(_.isEmpty(req.body.first_name)){
            sendResponse.sendCustomError({}, res, 0, "Enter First Name");
        }
        else if(_.isEmpty(req.body.last_name)){
            sendResponse.sendCustomError({}, res, 0, "Enter Last Name");
        }
        else if(_.isEmpty(req.body.email)){
            sendResponse.sendCustomError({}, res, 0, "Enter Valid Email Id");
        }
        else if(_.isEmpty(req.body.phone)){
            sendResponse.sendCustomError({}, res, 0, "Provide Valid phone number");
        }
        else if(_.isEmpty(req.body.highest_education)){
            sendResponse.sendCustomError({}, res, 0, "Enter your recent education");
        }
        else if(_.isEmpty(req.body.from_year)){
            sendResponse.sendCustomError({}, res, 0, "Enter your recent education start year");
        }
        else if(_.isEmpty(req.body.to_year)){
            sendResponse.sendCustomError({}, res, 0, "Enter your recent education completed year");
        }
        else if(_.isEmpty(req.body.why_bliss)){
            sendResponse.sendCustomError({}, res, 0, "Please let us know why you choosed Bliss");
        }
        else if(_.isEmpty(req.body.current_ctc)){
            sendResponse.sendCustomError({}, res, 0, "Enter your Current CTC");
        }
        else if(_.isEmpty(req.body.expected_ctc)){
            sendResponse.sendCustomError({}, res, 0, "Enter your Expected CTC");
        }
       
        else if(_.isEmpty(req.body.ready_to_relocate)){
            sendResponse.sendCustomError({}, res, 0, "Let us know if you are ready to relocate");
        }
        else{
            //console.log(req.files.resume);
            var resume = await imageService.uploadCloudinary(req.files.resume.tempFilePath);
            var apply_job = requiredFiles.applicationModel;
            let jobData = new apply_job({
                job_id : req.body.job_id,
                first_name : req.body.first_name,
                last_name : req.body.last_name,
                email : req.body.email,
                phone : req.body.phone,
                highest_education : req.body.highest_education,
                from_year : req.body.from_year,
                to_year : req.body.to_year,
                linkedin_profile : req.body.linkedin_profile,
                cover_letter_link : req.body.cover_letter_link,
                how_do_you_hear_about_this : req.body.how_do_you_hear_about_this,
                why_bliss : req.body.why_bliss,
                portfolio : req.body.portfolio,
                current_ctc : req.body.current_ctc,
                expected_ctc : req.body.expected_ctc,
                notice_period : req.body.notice_period,
                current_location : req.body.current_location,
                ready_to_relocate : req.body.ready_to_relocate,
                resume:resume,
            })
            var jobOpenings = await commonService.insertData(jobData);
            sendResponse.sendSuccess({jobOpenings}, res, 1, "Applied Successfully.");
        }
        return false;
        
       }
}
module.exports = new careers;