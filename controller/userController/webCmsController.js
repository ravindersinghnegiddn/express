const sendResponse = require('../../util/CustomResponse')
const webAboutService = require('../../services/userServices/webAboutService')
const cmsService = require('../../services/userServices/cmsService')
const contactService = require('../../services/userServices/contactService')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var nodemailer = require('nodemailer');
const { ObjectId } = require('mongodb');
class webCmsController{
      constructor(){}

      /* * *********************************************************************
	 * * Function name : getAboutUsWeb
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for get About Us Web
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getAboutUsWeb = async(req,res) => {
        
       var aboutData = await webAboutService.getWebAbout() 
       sendResponse.sendSuccess({aboutData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getTeamData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for get Team Data
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getTeamData = async(req,res) => {
            
            var teamData = await cmsService.getTeamData() 
            sendResponse.sendSuccess({teamData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : putContactUsData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      putContactUsData = async(req,res) => {
            
            var name    = req.body.name;
            var email   = req.body.email;
            var mobile  = req.body.mobile;
            var country = req.body.country;
            var message = req.body.message;
            var creation_date = req.body.creation_date;

            var putContactData = {name,email,mobile,country,message}
            var contactData = await contactService.putContactData(putContactData);
            sendResponse.sendSuccess({contactData}, res, 1, "Query Submitted Successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getHomePageData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getHomePageData = async(req,res) => {
            
            var bannerData    = await commonService.getData(requiredFiles.bannerModel,'multiple',{'page_type':"home"});
            var homePageData  = await commonService.getData(requiredFiles.homeContentModel,'single');
            var getFounderallData= await commonService.getData(requiredFiles.aboutUsWebModel,'single');
            var getTestimonials= await commonService.getData(requiredFiles.testimonialModel,'multiple');
            if(getFounderallData != ""){
                  var FounderData    = {
                        "home_about_founder_title":getFounderallData.home_about_founder_title,
                        "home_about_founder_description":getFounderallData.home_about_founder_description,
                        "home_about_founder_image":getFounderallData.home_about_founder_image
                  }
            }
            else{
                  var FounderData    = {} 
            }
            sendResponse.sendSuccess({bannerData,homePageData,getTestimonials,FounderData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getSupportCategories
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getSupportCategories = async(req,res) => {
            
            var supportCategories    = await commonService.getData(requiredFiles.supportCatroriesModel,'multiple');
           
            sendResponse.sendSuccess({supportCategories}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getSupportFaq
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getSupportFaq = async(req,res) => {
            
            var data_type = req.body.data_type;
            var category_id = req.body.category_id;
            if(data_type == 'popular'){
                  var FAQData       = await commonService.getDataByWhereCondition(requiredFiles.supportFaqsModel,'multiple',"is_popular",'Y');
            }
            if(category_id){
                  var FAQData       = await commonService.getDataByWhereCondition(requiredFiles.supportFaqsModel,'multiple',"category_id",category_id);
            }
           
            sendResponse.sendSuccess({FAQData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getRetreats
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getRetreats = async(req,res) => {
            
            var retreat_slug        =     req.body.retreat_slug;
            var retreatData         =     await commonService.getDataByWhereCondition(requiredFiles.retreatModel,'single',"title_slug",retreat_slug);
            
           
            sendResponse.sendSuccess({retreatData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getPodcasts
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 24 MAY 2021
	 * * **********************************************************************/

      getPodcasts = async(req,res) => {
            
            var data_type           =     req.body.data_type;
            if(data_type == 'single'){
                  var podcast_id           =     req.body.podcast_id;
                  var bannerData           =     await commonService.getDataByWhereCondition(requiredFiles.podcastsModel,'single',"podcast_id",podcast_id);
                  var podCastData          =     await commonService.getData(requiredFiles.podcastsModel,'multiple');
                  var popularPodcasts      =     await commonService.getDataByWhereCondition(requiredFiles.podcastsModel,'multiple',"is_popular",'Y');
            }
            else if(data_type == 'multiple'){
                  var bannerData           =     await commonService.getDataByWhereCondition(requiredFiles.podcastsModel,'multiple',"show_banner",'Y');
                  var podCastData          =     await commonService.getData(requiredFiles.podcastsModel,'multiple');
                  var popularPodcasts      =     await commonService.getDataByWhereCondition(requiredFiles.podcastsModel,'multiple',"is_popular",'Y');
            }
           
            sendResponse.sendSuccess({bannerData,podCastData,popularPodcasts}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getTermsPolicy
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getTermsPolicy = async(req,res) => {
            
            var page_type               =     req.body.page_type;
            var termsPolicyData         =     await commonService.getDataByWhereCondition(requiredFiles.termsModel,'single',"policy_type",page_type);
            
           
            sendResponse.sendSuccess({termsPolicyData}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getDailyQuotes
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getDailyQuotes = async(req,res) => {
            
           var todaysQuote         =     await commonService.getDataBySort(requiredFiles.quotesModel,'single',"creation_date","-1");
                      
            sendResponse.sendSuccess({todaysQuote}, res, 1, "Data fetched successfully.");
      }

      /* * *********************************************************************
	 * * Function name : getPageBannerData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getPageBannerData = async(req,res) => {
            
            var bannerData         =     await commonService.getData(requiredFiles.bannerModel,'multiple',{"page_type":req.body.page_type});
                       
             sendResponse.sendSuccess({bannerData}, res, 1, "Data fetched successfully.");
       }

       /* * *********************************************************************
	 * * Function name : getPageSEOData
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      getPageSEOData = async(req,res) => {
            
            var seoData         =     await commonService.getData(requiredFiles.seoModel,'single',{"page_slug":req.body.page_slug});
            if(seoData !== null || seoData !== ''){
                  sendResponse.sendSuccess({seoData}, res, 1, "Data fetched successfully.");
            }
            else{
                  sendResponse.sendCustomError({}, res, 0, "No data found");
            }
             
       }

       /* * *********************************************************************
	 * * Function name : setNewsLetterSubscription
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for retreats
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

      setNewsLetterSubscription = async(req,res) => {
            
            var existData         =     await commonService.getData(requiredFiles.newsLetterModel,'single',{"email":req.body.email});
            if(existData !== null && existData !== ''){
                  sendResponse.sendCustomError({}, res, 0, "You already subscribed.Thank you !!!");
            }
            else{
                  var newsLetter = requiredFiles.newsLetterModel
                  let params = new newsLetter({
                  email : req.body.email,
                  creation_date : req.body.creation_date,
                  })
                  var newsLetterData = await commonService.insertData(params);
                  sendResponse.sendSuccess({newsLetterData}, res, 1, "Thankyou for subscribing.");
                  
            }
             
       }

}
module.exports = new webCmsController()