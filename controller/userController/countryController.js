const sendResponse = require('../../util/CustomResponse');
const countryService = require('../../services/userServices/countryService');
class countryController{
    constructor(){}

    getCountryData = async(req,res) => {
        var countryData = await countryService.getCountryData() 
       sendResponse.sendSuccess({countryData}, res, 1, "Data fetched successfully.");
    }
}
module.exports = new countryController()