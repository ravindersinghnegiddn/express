const eventService = require('../../services/instructorService/eventServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
const Agora = require("agora-access-token");
var slugify = require('slugify')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

class event {
constructor(){}
 
addEvent = async(req,res) => {
    try{
        var eventInsert = requiredFiles.eventModel;
        let params = new eventInsert({
            name            : req.body.name,
            alias           : slugify(req.body.name),
            eventDateTime   : req.body.eventDateTime,
            instructorId    : req.body.instructor_id,
            eventDescription: req.body.eventDescription,
            icon            : req.body.icon,
            creation_ip     : req.body.creation_ip,
            creation_date   : req.body.creation_date,
            created_by      : req.body.created_by,
            status          : req.body.status,
        })
         
        const data = await commonService.insertData(params);
       
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding");
        }
        
    }
    catch(err){
    
		//console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

updateEvent = async(req,res) => {
    try{
       
        var instructorId = req.data.instructor_id
        var id = req.body.id
        
        const eventData = await eventService.viewEvent(id,instructorId)   //check eventId
        
        if(eventData!==null)
        {
            let params = {
                name            : req.body.name,
                alias           : slugify(req.body.name),
                eventDateTime   : req.body.eventDateTime,
                instructorId    : req.body.instructor_id,
                eventDescription: req.body.eventDescription,
                icon            : req.body.icon,
                update_ip       : req.body.update_ip,
                update_date     : req.body.update_date,
                updated_by      : req.body.updated_by,
            }
                const data = await commonService.updateData(requiredFiles.eventModel,'_id',req.body.id,params)  //update events
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "event updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating event");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update event catch block");
        
    }
}

listEvent = async(req,res) => {
    try{
         
        const data = await categoryService.listCategory()
       
        if(data){
        
        sendResponse.sendSuccess({data}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

listEvent1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var instructorId = req.data.instructor_id
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var searchControl={} 
            searchControl={
                $and:[
                    
                    {instructorId:instructorId},
                   
                ],
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await eventService.listEvent1(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "event list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "event list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listevent controller catch block");
        
    }
}

viewEvent = async(req,res) => {
    try{
        var instructorId = req.data.instructor_id
        const data = await eventService.viewEvent(req.query.id,instructorId)
        if(data){
       
        sendResponse.sendSuccess({eventData:data}, res, 0, "event data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "event data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}

deleteEvent = async(req,res) => {
    try{
        console.log("<><><><<><><><><><")
        
        const data = await eventService.deleteEvent(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "event deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleating event");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in delete event catch block");
        
    }
}

generateToken = async(req,res) => {
    try{
       
        let {id} = req.body
        var instructorId = req.data.instructor_id
        const eventData = await eventService.viewEvent(id,instructorId)   //check eventId
         
        if(eventData!==null)
        {
            const tokenData = this.generateRtcToken(eventData.name)

            const data = await eventService.updateEventToken({id,tokenData})  //update event token
        
            if(data){
                console.log(data)
                sendResponse.sendSuccess({channelName:eventData.name,token:tokenData}, res, 0, "event updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while generating token");
            }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in generate token catch block");
        
    }
}

generateRtcToken = (name) => {
  const appID =  "7190d23ee4f5432998d03def0da38805";
  const appCertificate = '4e438ae6c34445c687eb8b4a552d3e64';
  const expirationTimeInSeconds = 36000;
  const uid =0;
  const role = Agora.RtcRole.PUBLISHER 
  const currentTimestamp = Math.floor(Date.now() / 1000);
  const expirationTimestamp = currentTimestamp + expirationTimeInSeconds;

  const token = Agora.RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, name, uid, role, expirationTimestamp);
  
  return(token)
}

updateEventStatus = async(req,res) => {
    try{
       

        let {id,status} = req.body
        
        const eventData = await eventService.viewEvent(id)   //check newsId

        if(eventData!==null)
        {
            
            const data = await eventService.updateEventStatus({id,status})  //update events
        
            if(data){
                onsole.log(data)
                    sendResponse.sendSuccess(data, res, 0, "event status  updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while updating event status");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update event status catch block");
        
    }
}




}

module.exports = new event()