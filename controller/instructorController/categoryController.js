const categoryService = require('../../services/instructorService/categoryServices')
const instructorService = require('../../services/instructorService/instructorServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
var fs = require('fs')
//const categoryServices = require('../services/categoryServices')
const { ObjectId } = require('mongodb');

class category {
constructor(){}
 
addCategory = async(req,res) => {
    try{
        
        let name = req.body.name
        let title = req.body.title 
        let slugedName = slugify(req.body.name)
      
         var basImage= req.body.icon
         console.log(req.body.name)
         //var imageUrl = "fdfrf"
         var imageUrl = await imageService.uploadCloudinary(basImage)
         var instructorId = req.data.instructor_id
         const data = await categoryService.addCategory(name,slugedName,title,imageUrl,instructorId)
       
        if(data){
        sendResponse.sendSuccess(data, res, 0, "category added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding category");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

updateCategory = async(req,res) => {
    try{
        console.log(req.body)
        let {id,name} = req.body
        let slugedName = slugify(req.body.name)
        let title = req.body.title
        var basImage= req.body.icon
        var imageUrl = "fdfrf"
       // var imageUrl = await imageService.uploadCloudinary(basImage)
        console.log("-------------imageurl-----------------",imageUrl)
        const data = await categoryService.checkCategoryNameUpdate(slugedName,id)
        
        if(data){
           
            sendResponse.sendCustomError({ }, res, 4, "category name already exist");
        
        }
        else{

            const data = await categoryService.updateCategory(id,name,slugedName,title,imageUrl)
            sendResponse.sendSuccess(data, res, 0, "category  updated successfully");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

listCategory = async(req,res) => {
    try{
        var instructorId = req.data.instructor_id
       // var categoryData = await instructorService.getInstructorCategoryArray(instructorId)
        const data = await categoryService.listCategory(instructorId)
        console.log("data",data)
        if(data){
        
        sendResponse.sendSuccess({data:data.selectedCategory}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

listCategory1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var instructorId = req.data.instructor_id
        var searchControl={} 
            searchControl={
                $and:[
                    {instructorId:instructorId},
                    
                ],
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await categoryService.listCategory1(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

viewCategory = async(req,res) => {
    try{
 
        const data = await categoryService.viewCategory(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "category data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}

deleteCategory = async(req,res) => {
    try{
        console.log("<><><><<><><><><><")
        const data = await categoryService.deleteCategory(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "category deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing category");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete category catch block");
        
    }
}


listCategoryDropdown = async(req,res) => {
    try{
        var instructorId = ''
       // var categoryData = await instructorService.getInstructorCategoryArray(instructorId)
        const data = await categoryService.listCategoryDropdown()
        console.log("data",data)
        if(data){
        
        sendResponse.sendSuccess({data:data}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}


}

module.exports = new category()