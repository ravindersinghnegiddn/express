const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
const coursesServices = require('../../services/adminServices/courseServices')
const { ObjectId } = require('mongodb');
const { json } = require('body-parser');
const videoService = require('../../services/instructorService/videoServices')



class video {
constructor(){}

addVideo = async(req,res) => {
    try{
        
        let {courseId,videoLink,videoName,episodeNumber,videoDescription,sectionId} = req.body
        var instructorId = req.data.instructor_id
    
        var videoLink1= videoLink
        var c = videoLink1.split('/')
        var videoId1 = c[c.length-1]
        const courseData = await videoService.viewCourse(courseId)  //check course
        
        if(courseData!==null){

            const data = await videoService.addVideo({courseId,videoLink1,videoName,videoId1,episodeNumber,videoDescription,sectionId,instructorId})  //add question
       
            if(data){
                console.log(data)
                sendResponse.sendSuccess(data, res, 0, "video added successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding video");
            }
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }
            
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in add video catch block");
        
    }
}
listCourse = async(req,res) => {
    try{
         
        const courseList = await coursesServices.listCourse()
        //console.log(Date.now())
        if(courseList.length>0)
        {
            console.log(courseList)
            sendResponse.sendSuccess({courseList}, res, 0, "course list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}
listVideo1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        //console.log(req.data.instructor_id)
        var searchControl={} 
            searchControl={
                $and:[
                    {instructorId:req.data.instructor_id},
                    
                ],
                $or:[
                    {videoName:{ $regex: '.*' + search_value+ '.*' }},
                    
                ]
            }
        const pageControl = {skip1,requested_count1} 
                
 
        const courseList = await videoService.listVideos(searchControl,pageControl,req.data.instructor_id)
      
       
        if(courseList.data.length>0)
        {
            var courseData = []
            courseList.data.forEach(element => {
                var obj = {}
                 console.log(element.sectionId)
                 if(element.courseId!==null){
                     obj.videoName = element.videoName
                     obj.videoId = element.videoId
                     obj._id = element._id
                     obj.courseName = element.courseId.courseName
                     obj.videoUrl = element.videoUrl
                     obj.sectionName = element.sectionId.sectionHeading
                     obj.sectionNumber = element.sectionId.sectionNumber
                     obj.createdAt = element.createdAt

                     courseData.push(obj)
                 }
                 else{
                    obj.videoName = element.videoName
                    obj.videoId = element.videoId
                    obj._id = element._id
                    obj.courseName = ''
                    obj.videoUrl = element.videoUrl
                    obj.createdAt = element.createdAt
                    courseData.push(obj)
                 }
            });
            delete courseList.data
            courseList.data = courseData
            console.log(">>>>",courseList)
            sendResponse.sendSuccess(courseList, res, 0, "video list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "video list fetched  successfully");
        }   
        
    }
    catch(err){
    
		//console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list video catch block");
        
    }
}
viewCourse = async(req,res) => {
    try{
         
        const data = await coursesServices.viewCourse(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({courseData:data}, res, 0, "course detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "course detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		//console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}
viewVideo= async(req,res) => {
    try{
         
        const data = await videoService.viewVideo(req.query.id)
        if(data){
      
        sendResponse.sendSuccess({videoData:data}, res, 0, "video detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "video detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view video catch block");
        
    }
}
updateVideo = async(req,res) => {
    try{
        
        let {id,courseId,videoLink,videoName,episodeNumber,videoDescription,sectionId} = req.body
        //console.log(req.body)
        if(videoLink){
            var videoLink1= videoLink
            var c = videoLink1.split('/')
            var videoId1 = c[c.length-1]

        }
        else{
            var videoLink1= '';
            var videoId1 = ''
        }
        
        const courseData = await videoService.viewCourse(courseId)
       
        const videoData = await videoService.viewVideo(id)   //check  videoId
        //console.log('>>>>>>>>>>>>>>>>>>>>Teju>>>>>Video>>>>>'+videoData.videoId+'>>>>>>> '+videoId1+' >>>>>>>>>')
        if(videoData!==null)
        {
            if(videoId1 == '' && videoLink1 == ''){
                var videoLink1= videoData.videoUrl;
                var videoId1 = videoData.videoId
            }
            const courseData = await videoService.viewCourse(courseId)  //check courseId
            if(courseData!==null)
            {
                const data = await videoService.updateVideo({id,courseId,videoLink1,videoName,videoId1,episodeNumber,videoDescription,sectionId})  //update video
                if(data)
                {
                    sendResponse.sendSuccess(data, res, 0, "video updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating video");
                }
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
            }  
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "videoId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update Video catch block");
        
    }
}
deleteVideo = async(req,res) => {
    try{
         
        const data = await videoService.deleteVideo(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({questionData:data}, res, 1, "video deleted successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "video deleted  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete video catch block");
        
    }
}
listCourseVideo = async(req,res) => {
    try{
        let { courseId,page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
        var searchControl={} 
            searchControl={
                courseId:courseId,
                $or:[
                    {videoName:{ $regex: '.*' + search_value+ '.*' }},
                    
                ]
            }
        const pageControl = {skip1,requested_count1} 
                
 
        const courseList = await videoService.listCourseVideos1(searchControl,pageControl)
      
        
        if(courseList.data.length>0)
        {
            var courseData = []
            courseList.data.forEach(element => {
                var obj = {}
                 
                 if(element.courseId!==null){
                     obj.videoName = element.videoName
                     obj.videoId = element.videoId
                     obj._id = element._id
                     obj.courseName = element.courseId.courseName
                     obj.videoUrl = element.videoUrl
                     obj.createdAt = element.createdAt
                     courseData.push(obj)
                 }
                 else{
                    obj.videoName = element.videoName
                    obj.videoId = element.videoId
                    obj._id = element._id
                    obj.courseName = ''
                    obj.videoUrl = element.videoUrl
                    obj.createdAt = element.createdAt
                    courseData.push(obj)
                 }
            });
            delete courseList.data
            courseList.data = courseData
            sendResponse.sendSuccess(courseList, res, 0, "course video list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course video list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list video catch block");
        
    }
}

}

module.exports = new video()