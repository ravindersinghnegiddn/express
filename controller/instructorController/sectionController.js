const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
const categoryServices = require('../../services/adminServices/categoryServices')
const coursesServices = require('../../services/adminServices/courseServices')
const { ObjectId } = require('mongodb');
const { json } = require('body-parser');

const videoService = require('../../services/adminServices/videoServices')
const sectionService = require('../../services/instructorService/sectionServices')



class section {
constructor(){}



addSection = async(req,res) => {
    try{
        
        let {course,sectionHeading,sectionNumber} = req.body
        
    
        const courseData = await videoService.viewCourse(course)  //check course
        
        if(courseData!==null){
           var sectionData = { 
                
                course:course,
                sectionHeading : sectionHeading,
                sectionNumber:sectionNumber,
                instructorId:req.data.instructor_id
               
            }

            const data = await sectionService.addSection(sectionData)  //add section
       
            if(data){
                console.log(data)
                sendResponse.sendSuccess(data, res, 0, "section added successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding section");
            }
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }
            
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addQuestion catch block");
        
    }
}

listSection= async(req,res) => {
    try{
         console.log(req.query)
        const sectionList = await sectionService.listSectionDropdown(req.query.id)
    
        if(sectionList.length>0)
        {
            
            sendResponse.sendSuccess({sectionList}, res, 0, "section list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "section list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}

listSection1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
        var searchControl={} 
            searchControl={
                $and:[
                    {instructorId:req.data.instructor_id},
                    
                ],
                $or:[
                    {sectionHeading:{ $regex: '.*' + search_value+ '.*' }},
                    
                ]
            }
        const pageControl = {skip1,requested_count1} 
                
 
        const sectionList = await sectionService.listSection(searchControl,pageControl)
      
        
        if(sectionList.data.length>0)
        {
            // var courseData = []
            // courseList.data.forEach(element => {
            //     var obj = {}
                 
            //      if(element.courseId!==null){
            //          obj.videoName = element.videoName
            //          obj.videoId = element.videoId
            //          obj._id = element._id
            //          obj.courseName = element.courseId.courseName
            //          obj.videoUrl = element.videoUrl
            //          obj.createdAt = element.createdAt
            //          courseData.push(obj)
            //      }
            //      else{
            //         obj.videoName = element.videoName
            //         obj.videoId = element.videoId
            //         obj._id = element._id
            //         obj.courseName = ''
            //         obj.videoUrl = element.videoUrl
            //         obj.createdAt = element.createdAt
            //         courseData.push(obj)
            //      }
            // });
            // delete courseList.data
            // courseList.data = courseData
            sendResponse.sendSuccess(sectionList, res, 0, "section list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "section list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list section catch block");
        
    }
}

viewCourse = async(req,res) => {
    try{
         
        const data = await coursesServices.viewCourse(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({courseData:data}, res, 0, "course detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "course detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}

viewSection= async(req,res) => {
    try{
         
        const data = await sectionService.viewSection(req.query.id)
        if(data){
        console.log(data)
        sendResponse.sendSuccess({videoSection:data}, res, 0, "section detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "section detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view section catch block");
        
    }
}

updateSection = async(req,res) => {
    try{
        
        let {id,course,sectionHeading,sectionNumber} = req.body
        
       
        const sectionData = await sectionService.viewSection(id)   //check  sectionId
        if(sectionData!==null)
        {
            const courseData = await videoService.viewCourse(course)  //check courseId
            if(courseData!==null)
            {
                const data = await sectionService.updateSection({id,course,sectionHeading,sectionNumber})  //update course
                if(data)
                {
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "section updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating section");
                }
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
            }  
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "sectionId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update section catch block");
        
    }
}

deleteSection = async(req,res) => {
    try{
         
        const data = await videoService.deleteVideo(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({questionData:data}, res, 1, "video deleted successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "video deleted  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete video catch block");
        
    }
}

listCourseVideo = async(req,res) => {
    try{
        let { courseId,page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
        var searchControl={} 
            searchControl={
                courseId:courseId,
                $or:[
                    {videoName:{ $regex: '.*' + search_value+ '.*' }},
                    
                ]
            }
        const pageControl = {skip1,requested_count1} 
                
 
        const courseList = await videoService.listVideos(searchControl,pageControl)
      
        
        if(courseList.data.length>0)
        {
            var courseData = []
            courseList.data.forEach(element => {
                var obj = {}
                 
                 if(element.courseId!==null){
                     obj.videoName = element.videoName
                     obj.videoId = element.videoId
                     obj._id = element._id
                     obj.courseName = element.courseId.courseName
                     obj.videoUrl = element.videoUrl
                     obj.createdAt = element.createdAt
                     courseData.push(obj)
                 }
                 else{
                    obj.videoName = element.videoName
                    obj.videoId = element.videoId
                    obj._id = element._id
                    obj.courseName = ''
                    obj.videoUrl = element.videoUrl
                    obj.createdAt = element.createdAt
                    courseData.push(obj)
                 }
            });
            delete courseList.data
            courseList.data = courseData
            sendResponse.sendSuccess(courseList, res, 0, "course video list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course video list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list video catch block");
        
    }
}

}

module.exports = new section()