const instructorService = require('../../services/instructorService/instructorServices')
const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const Json2csvParser = require('json2csv').Parser;
const sendResponse = require('../../util/CustomResponse');
const categoryService = require('../../services/instructorService/categoryServices')
const courseService = require('../../services/instructorService/courseServices')
const imageService = require('../../services/adminServices/imageServices');
const commonService = require('../../services/userServices/commonService');
const requiredFiles = require('../../database/required');
const { ObjectId } = require('mongodb');

class instructor {
constructor(){}


instructorSignup = async(req,res) => {
    try{
        const{name,email,password,categories}= req.body
        const hashPassword = sha256(password)
        if(categories){
            if((typeof(categories)) == 'string'){
                var categoryArray = JSON.parse(categories) 
                //console.log("categoryArray---------------->",categoryArray)    
            }
            var categoryArray = JSON.parse(JSON.stringify(categories))
            //console.log(categories,categoryArray)
        }
        
        let userData = {name,email,hashPassword,categoryArray}
        var data = await instructorService.instructorSignup(userData)
        var token = await this.generateToken(data)   
       
        sendResponse.sendSuccess({name:data.name,email:data.email,profileImg:data.profileImg,id:data._id,token:token}, res, 0, "instructor signup successfully");
        
    }
    catch(err){
    console.log(err)
    sendResponse.sendCustomError({ "error":err }, res, 4, "error in instructor signup catch block");
    }
}

instructorlogin = async(req,res) => {
    try{
       
        const{email,password}= req.body
        const hashPassword = sha256(password)
        let userData = {email,hashPassword}
        
        var data = await instructorService.instructorLogin(userData)
           
        if(data){
            var token = await this.generateToken(data)
        
           if(data.premium_member == 'Y'){
             var premium_member= 'Y'
           }
           else{
            var premium_member= 'N'
           }

            var result = data
        
           sendResponse.sendSuccess({result,token,"premium_member":premium_member}, res, 0, "instructor login successfully");
        }
        else{
           
            sendResponse.sendCustomError('', res, 4, "invalid credential");
        }
    }
    catch(err){

        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in instructor login catch block");
    }
}

getProfile = async(req,res) => {
    try{
        var userData = await instructorService.instructorProfile(req.query.instructorId)
        if(userData){
            var choosedCategories = await commonService.getDataByQuery(requiredFiles.categories,userData.selectedCategory);
            sendResponse.sendCustomError({userData,choosedCategories}, res, 1, "instructor data fetched successfully");
        }
        else{ 
            sendResponse.sendCustomError({}, res, 1, "unable to fetch data");
        }
    }
    catch(err){
        console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in instructor get profile catch block");
    }
}

updateProfile = async(req,res) => {
     
    try{
        const {name,last_name,tagline,shortDescription,phone_number,city,country,industry,language,aboutMe,website,facebook,twitter,linkedin,dob,workedAt,studiedAt,awards,experience,categories} = req.body
        var updateData ={}
        
        if(req.files){
            if(req.files.profileImg.tempFilePath){
                var profile_img = await imageService.uploadCloudinary(req.files.profileImg.tempFilePath);
                updateData.profileImg = profile_img
            }
        }
        console.log("categoryArray---------------->",categories) 
        if((typeof(categories)) == 'string'){
            var categoryArray = JSON.parse(categories) 
            console.log("categoryArray---------------->",categoryArray)    
        }
        updateData.name = name
        updateData.last_name = last_name
        updateData.shortDescription = shortDescription
        updateData.phone_number = phone_number
        updateData.tagline = tagline
        updateData.experience = experience
        updateData.city = city
        updateData.selectedCategory = categoryArray
        updateData.country = country
        updateData.industry= industry
        updateData.aboutMe= aboutMe
        updateData.website= website
        updateData.facebook= facebook
        updateData.twitter= twitter
        updateData.linkedin= linkedin
        updateData.language=language
        updateData.dob = dob
        updateData.workedAt = workedAt
        updateData.studiedAt = studiedAt
        updateData.awards = awards
        updateData.updatedAt =Date.now()
           
        var result = await instructorService.updateInstructor(req.data.instructor_id,updateData)
        
         if(result){  
            
            sendResponse.sendSuccess({}, res, 1, "profile updated successfully");
            }
            else{
             sendResponse.sendCustomError('unable to update profile', res, 0, "internal server error..");
            }
    }
    catch(err){
    //console.log(err)
      //res.send({"status":"false","message":"error while inserting data ","error":err.message})
      sendResponse.sendCustomError({ "error":err }, res, 0, "error in update profile catch block");
    }
}
dashboard = async(req,res) => {
    try
    {
        var instructorId = req.data.instructor_id
        var dashboardData ={}
        console.log(instructorId)
        const  courseCount1 = await courseService.listCourse(instructorId)
        console.log("dashboard course----------->",courseCount1)
        dashboardData.courseCount = courseCount1.length
        
        sendResponse.sendSuccess({dashboardData},res,0,"dashboard data")
        
 
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in dashboard catch block");
    }
}

generateToken = async(data) =>{
    
    return jwt.sign({id:data._id},'*sssktAf9VcK6%Pv')
}

getInstuctorSessions = async(req,res) =>{

    var instructorSessions = await categoryService.getInstuctorSessions();
    sendResponse.sendSuccess({instructorSessions}, res, 1, "Data fetched successfully.");
}

addInstructorRegistrationForm = async(req,res)=>{
    try{
        const {name,last_name,email,category,why_to_help,website,facebook,twitter,linkedin,instagram,portfolio} = req.body;
        const hashPassword = sha256('blissGenx@2021');
        const status = 'I';
        const isBlocked = true;
        if((typeof(category)) == 'string'){
            var categoryArray = JSON.parse(category);
        }
        let instructorData = {name,last_name,email,categoryArray,why_to_help,website,facebook,twitter,linkedin,instagram,portfolio,status,isBlocked,hashPassword}
        var data = await instructorService.instructorSignup(instructorData)
        console.log(data);
        sendResponse.sendSuccess({id:data._id}, res, 0, "Registration requested successfully.");
    }
    catch(error){
        console.log('Something Went Wrong.');
        sendResponse.sendCustomError({ "error":error }, res, 4, "Unable to submit the form.");
    }
}

/* * *********************************************************************
* * Function name : addCertificate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addCertificate = async(req,res) => {
    try{
        
        var certificate_insert = requiredFiles.certiModel;
        let params = new certificate_insert({
            certificate_name        : req.body.certificate_name,
            provider_name           : req.body.provider_name,
            completion_year         : req.body.completion_year,
            instructor_id           : req.body.instructor_id,
            creation_ip             : req.body.creation_ip,
            creation_date           : req.body.creation_date,
            created_by              : req.body.created_by,
            status                  : req.body.status,
        })
        
        const data = await commonService.insertData(params);
    
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding");
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateCertificate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCertificate = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.certiModel,'single',{_id:req.body.id});
        //console.log(">>>>>>>>>>>>>>>>"+existData._id)
        if(existData._id){
            var params = {
                certificate_name        : req.body.certificate_name,
                provider_name           : req.body.provider_name,
                completion_year         : req.body.completion_year,
                instructor_id           : req.body.instructor_id,
                update_ip               : req.body.update_ip,
                update_date             : req.body.update_date,
                updated_by              : req.body.updated_by,
            }
            
            const data = await commonService.updateData(requiredFiles.certiModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while Updating");
            }
            
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "Invalid Id");
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : listCertificates
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listCertificates = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)

        const data = await commonService.getData(requiredFiles.certiModel,'multiple',{'instructor_id':req.body.instructor_id})
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewCertificate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewCertificate = async(req,res) => {
    try{
        const data = await commonService.getData(requiredFiles.certiModel,'single',{'_id':req.query.id})
        if(data){
       
        sendResponse.sendSuccess({certificateData:data}, res, 0, "Data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "Data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteCertificate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteCertificate = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.certiModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "Deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while Deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete catch block");
        
    }
}



}

module.exports = new instructor()