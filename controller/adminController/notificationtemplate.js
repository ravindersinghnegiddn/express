const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var slugify = require('slugify')
const { ObjectId } = require('mongodb');
const sendNotification = require('../../pushNotification/pushNotification')

class notificationtemplate{
    constructor(){}

/* * *********************************************************************
* * Function name : addNotificationTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addNotificationTemplate = async(req,res) => {
    try{
    
        var notif_inserty = requiredFiles.notifTemplateModel;
        let params = new notif_inserty({
            process_type            :   req.body.process_type,
            content                 :   req.body.content,
            image                   :   req.body.image,
            creation_ip             :   req.body.creation_ip,
            creation_date           :   req.body.creation_date,
            created_by              :   req.body.created_by,
            status                  :   req.body.status,
        })
        
        const data = await commonService.insertData(params);
        const pushResult = sendNotification.sendPush(req.body.content,req.body.image)

        if(data){
            sendResponse.sendSuccess(data, res, 0, "Data Inserted");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
        }
    
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}


/* * *********************************************************************
* * Function name : listNotificationTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listNotificationTemplate = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {process_type:{ $regex: '.*' + search_value+ '.*' }},
                    {content:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.notifTemplateModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.notifTemplateModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}


/* * *********************************************************************
* * Function name : deleteNotificationTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteNotificationTemplate = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.notifTemplateModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({templateData:data}, res, 1, "deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}

}
module.exports = new notificationtemplate()