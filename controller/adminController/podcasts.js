const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

const { ObjectId } = require('mongodb');

class podcasts{
    constructor(){}

/* * *********************************************************************
* * Function name : getPodcastsData
* * Developed By : Tejaswi
* * Purpose  : This function use for get About Us Web
* * Date : 13 MAY 2021
* * **********************************************************************/

getPodcastsData = async(req,res) => {

try{
    let { page_no, requested_count } = req.body
    var requested_count1 = requested_count ? parseInt(requested_count) : 10  
    var skip1 = page_no ? (parseInt(page_no)) : 0
    var search_value = search_value?search_value:'';
    
    var searchControl={} 
        searchControl={
            $or:[
                {podcast_title:{ $regex: '.*' + search_value+ '.*' }},
                {speaker_name:{ $regex: '.*' + search_value + '.*' }}
            ]
        }
    const pageControl = {skip1,requested_count1} 
    
    var Query = '';
    var Query = requiredFiles.podcastsModel.find(searchControl)
    .skip(skip1)
    .limit(requested_count1)
    const data = await commonService.getAllByQuery(Query)
    const totalRows = await commonService.getData(requiredFiles.podcastsModel,'multiple');
    if(data){
    
    sendResponse.sendSuccess({data,totalRows}, res, 1, "Data fetched  successfully");
    }
    else{
        sendResponse.sendSuccess({data:"No Data Found"}, res, 0, "No Data Found");
    }   
    
}
catch(err){

    console.log(err)
    
    sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
    
}
}

/* * *********************************************************************
* * Function name : addPodcast
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addPodcast = async(req,res) => {
    try{
        
        var podcasts_insert = requiredFiles.podcastsModel;
        let params = new podcasts_insert({
            podcast_title           : req.body.podcast_title,
            podcast_sub_title       : req.body.podcast_sub_title,
            description             : req.body.description,
            speaker_name            : req.body.speaker_name,
            podcast_audio           : req.body.podcast_audio,
            podcast_thumbnail       : req.body.podcast_thumbnail,
            podcast_banner          : req.body.podcast_banner,
            show_banner             : 'N',
            is_popular              : 'Y',
            creation_ip             : req.body.creation_ip,
            creation_date           : req.body.creation_date,
            created_by              : req.body.created_by,
            status                  : req.body.status,
        })
        
        const data = await commonService.insertData(params);
    
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Data Inserted");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updatePodcasts
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updatePodcasts = async(req,res) => {
    try{
        var params = {
            podcast_title           : req.body.podcast_title,
            podcast_sub_title       : req.body.podcast_sub_title,
            description             : req.body.description,
            speaker_name            : req.body.speaker_name,
            podcast_audio           : req.body.podcast_audio,
            podcast_thumbnail       : req.body.podcast_thumbnail,
            podcast_banner          : req.body.podcast_banner,
            creation_ip             : req.body.creation_ip,
            creation_date           : req.body.creation_date,
            created_by              : req.body.created_by,
            status                  : req.body.status,
        }
        
        const data = await commonService.updateData(requiredFiles.podcastsModel,'_id',req.body.id,params);
    
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
        }
        
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : makeAudioFree
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
makeAudioFree = async(req,res) => {
    try{
        var params = {
            show_banner             : 'Y',
        }
        console.log(params);
        const data = await commonService.updateData(requiredFiles.podcastsModel,'_id',req.body.id,params);
        if(data){
            var Uparams = {
                show_banner         : 'N',
            }
            const remaining = await commonService.updateAllData(requiredFiles.podcastsModel,'_id',{$ne:req.body.id},Uparams);
            sendResponse.sendSuccess(data, res, 0, "status updated successfully");
        }
        else{
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        }
        
    }
    catch(err){
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : updatePodcastStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updatePodcastStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.podcastsModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : viewPodcast
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewPodcast = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.podcastsModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({podcastData:data}, res, 0, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deletePodcast
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deletePodcast = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.podcastsModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}
}
module.exports = new podcasts();