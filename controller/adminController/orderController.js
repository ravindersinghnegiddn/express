const categoryService = require('../../services/adminServices/categoryServices')
const orderService  = require('../../services/adminServices/orderServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
var fs = require('fs')
//const categoryServices = require('../services/categoryServices')
const { ObjectId } = require('mongodb');
var moment = require('moment')
class order {
constructor(){}
 

listCategory = async(req,res) => {
    try{
         
        const data = await categoryService.listCategory()
       
        if(data){
        
        sendResponse.sendSuccess({data}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

listOrder= async(req,res) => {
    try{
        let { page_no, requested_count, search_value,from_date,to_date} = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        console.log(">>>>>>>>>>>>>>>",req.body.from_date)
        var searchControl={} 
            searchControl={
                $or:[
                    {orderNo:{ $regex: '.*' + search_value+ '.*' }},   
                ]
            }
        if((from_date != '') && (to_date != '')){
             console.log("here")
                var fromDate =  moment(from_date).utcOffset("+05:30").valueOf();
                var toDate =  moment(to_date).utcOffset("+05:30").valueOf();
                searchControl['updatedAt'] ={
                    $gte: fromDate,
                    $lte: toDate
                    }
        }   
          
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await orderService.listOrder(searchControl,pageControl)
        console.log(data.data[0].userId)
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "order list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "order list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in order list catch block");
        
    }
}

viewOrder = async(req,res) => {
    try{
        if(!req.query.id){
            sendResponse.sendCustomError({}, res, 4, "order Id is required")
        }
        else{
        const data = await orderService.viewOrder(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "order data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "order data fetched  successfully");
        }
    }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in order view catch block");
        
    }
}



}

module.exports = new order()