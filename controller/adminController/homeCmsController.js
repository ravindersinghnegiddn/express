const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
class homeCms {
constructor(){}

/* * *********************************************************************
* * Function name : updateHomePage
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateHomePage = async(req,res) => {
    try{
        var params = {
            home_about_title                : req.body.home_about_title,
            home_about_description          : req.body.home_about_description,
            home_about_image                : req.body.home_about_image,
            home_program_title              : req.body.home_program_title,
            home_program_description        : req.body.home_program_description,
            home_category_title             : req.body.home_category_title,
            home_category_sub_title         : req.body.home_category_sub_title,
            home_member_title               : req.body.home_member_title,
            home_member_content             : req.body.home_member_content,
            home_member_image               : req.body.home_member_image,
            home_courses_title              : req.body.home_courses_title,
            home_courses_description        : req.body.home_courses_description,
            home_testimonial_title          : req.body.home_testimonial_title,
            home_user_join_title            : req.body.home_user_join_title,
            home_user_join_sub_title        : req.body.home_user_join_sub_title,
            home_user_join_content          : req.body.home_user_join_content,
            home_user_join_sub_content      : req.body.home_user_join_sub_content,
            home_user_join_image            : req.body.home_user_join_image,
            update_ip                       : req.body.update_ip,
            update_date                     : req.body.update_date,
            updated_by                      : req.body.updated_by,
        }
        
        const data = await commonService.updateData(requiredFiles.homeContentModel,'_id',req.body.id,params)
        
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Home Page updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : lisHomePageCms
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
lisHomePageCms = async(req,res) => {
    try{
        const homePageData = await commonService.getData(requiredFiles.homeContentModel)
        
        if(homePageData){
            sendResponse.sendSuccess(homePageData, res, 0, "Home Page updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : updateAboutUsWeb
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateAboutUsWeb = async(req,res) => {
    try{
        var params = {
            title                           : req.body.title,
            description                     : req.body.description,
            image                           : req.body.image,
            about_success_title             : req.body.about_success_title,
            student_world_wide_title        : req.body.student_world_wide_title,
            student_world_wide_count        : req.body.student_world_wide_count,
            instructor_title                : req.body.instructor_title,
            instructor_count                : req.body.instructor_count,
            events_title                    : req.body.events_title,
            events_count                    : req.body.events_count,
            session_title                   : req.body.session_title,
            session_count                   : req.body.session_count,
            about_success_video_url         : req.body.about_success_video_url,
            join_bliss_title                : req.body.join_bliss_title,
            join_bliss_description          : req.body.join_bliss_description,
            join_bliss_sub_title            : req.body.join_bliss_sub_title,
            join_bliss_sub_description      : req.body.join_bliss_sub_description,
            join_bliss_image                : req.body.join_bliss_image,
            provide_title                   : req.body.provide_title,
            provide_description             : req.body.provide_description,
            provide_image                   : req.body.provide_image,
            home_about_founder_title        : req.body.home_about_founder_title,
            home_about_founder_description  : req.body.home_about_founder_description,
            home_about_founder_image        : req.body.home_about_founder_image,
            update_ip                       : req.body.update_ip,
            update_date                     : req.body.update_date,
            updated_by                      : req.body.updated_by,
        }
        console.log('hhh')
        const data = await commonService.updateData(requiredFiles.aboutUsWebModel,'_id',req.body.id,params)
        
        if(data){
            sendResponse.sendSuccess(data, res, 0, "About us updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listAboutUsWeb
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
listAboutUsWeb = async(req,res) => {
    try{
        const aboutUsWeb = await commonService.getData(requiredFiles.aboutUsWebModel)
        
        if(aboutUsWeb){
            sendResponse.sendSuccess(aboutUsWeb, res, 0, "Data fetched successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : getTestimonials
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
getTestimonials = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {review_for:{ $regex: '.*' + search_value+ '.*' }},
                    {user_name:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.testimonialModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.testimonialModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : getGeneralData
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
getGeneralData = async(req,res) => {
    try{
        const generalData = await commonService.getData(requiredFiles.generalModel)
        
        if(generalData){
            sendResponse.sendSuccess(generalData, res, 0, "Data fetched successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : updateGeneralData
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateGeneralData = async(req,res) => {
    try{
        var params = {
            logo_dark           : req.body.logo_dark,
            logo_white          : req.body.logo_white,
            facebook            : req.body.facebook,
            twitter             : req.body.twitter,
            linkedin            : req.body.linkedin,
            instagram           : req.body.instagram,
            youtube             : req.body.youtube,
            membership          : req.body.membership,
            premium             : req.body.premium,
            
        }
        const data = await commonService.updateData(requiredFiles.generalModel,'_id',req.body.id,params)
        
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}
}
module.exports = new homeCms()