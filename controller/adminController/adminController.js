const adminService = require('../../services/adminServices/adminServices')
const categoryService = require('../../services/adminServices/categoryServices')
const questionService = require('../../services/adminServices/questionServices')
const courseService = require('../../services/adminServices/courseServices')
const courseVideoService = require('../../services/adminServices/videoServices')
const sha256 = require('sha256')
const jwt = require('jsonwebtoken')
const Json2csvParser = require('json2csv').Parser;
const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
const { ObjectId } = require('mongodb');

class admin {
constructor(){}
 adminSignup = async(req,res) => {
    try{
        const{name,email,password,phone,age,subject}= req.body
        const hashPassword = sha256(password)
        let userData = {name,email,hashPassword,phone,age}
        console.log(userData)
        var data = await adminService.adminSignupServices(userData)
        sendResponse.sendSuccess({"message":"true","data":data})
        
    }
    catch(err){
    console.log(err)
      res.send({"status":"false","message":"error while inserting data ","error":err.message})
    }
}

/* * *********************************************************************
* * Function name : login
* * Developed By : Tejaswi
* * Purpose  : This function used for login
* * Date : 08 JUNE 2021
* * **********************************************************************/

adminlogin = async(req,res) => {
    try{
        const{userEmail,userPassword}= req.body
        const hashPassword = sha256(userPassword)
        let userData = {userEmail,hashPassword}
        
        var data = await adminService.adminLoginServices(userData)
           
        if(data){
            var token = await this.generateToken(data)
            
            var result = data
           
           
          // sendResponse.sendSuccess({result,token}, res, 0, "admin login successfully");
           sendResponse.sendSuccess({result,token}, res, 0, "admin login successfully");
        }
        else{
           
            sendResponse.sendCustomError('', res, 4, "invalid credential");
        }
    }
    catch(err){

        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in adminLogin catch block");
    }
}

/* * *********************************************************************
* * Function name : update
* * Developed By : Tejaswi
* * Purpose  : This function used for update
* * Date : 08 JUNE 2021
* * **********************************************************************/
updateAdmin = async(req,res) => {
    try{
        const{last_login_ip,last_login_date}= req.body

        let userData = {last_login_ip,last_login_date}
        
        var data = await commonService.getData(requiredFiles.adminModel,'single',{'admin_id':req.body.admin_id})
           
        if(data){
            var token = await this.generateToken(data)
            
            var result = data
            var param = {
                "last_login_ip":last_login_ip,
                "last_login_date":last_login_date
            }
            var updateData = await commonService.updateData(requiredFiles.adminModel,'admin_id',req.body.admin_id,param)
            sendResponse.sendSuccess({result,token}, res, 0, "admin login successfully");
        }
        else{
           
            sendResponse.sendCustomError('', res, 4, "invalid credential");
        }
    }
    catch(err){

        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in adminLogin catch block");
    }
}

/* * *********************************************************************
* * Function name : getMainDashBoard
* * Developed By : Tejaswi
* * Purpose  : This function used for getMainDashBoard
* * Date : 08 JUNE 2021
* * **********************************************************************/
getMainDashBoard = async(req,res) => {

    if(req.body.main_module_name){
        var param = {
            "main_module_name":req.body.main_module_name
        }
    }
    var getMainDashBoard    = await commonService.getData(requiredFiles.mainModuleModel,'multiple',param);
           
    sendResponse.sendSuccess({getMainDashBoard}, res, 1, "Data fetched successfully.");
}

/* * *********************************************************************
* * Function name : getModuleMenu
* * Developed By : Tejaswi
* * Purpose  : This function used for getModuleMenu
* * Date : 08 JUNE 2021
* * **********************************************************************/
getModuleMenu = async(req,res) => {

    var getModuleMenu    = await commonService.getData(requiredFiles.moduleModel,'multiple',{"main_module_id":req.body.main_module_id});
           
    sendResponse.sendSuccess({getModuleMenu}, res, 1, "Data fetched successfully.");
}


listUser = async(req,res) => {
    try
    { 
         var {skip,limit,searchValue} = req.body
            
            var data1={} 
            data1={
                $or:[
                    {firstName:{ $regex: '.*' + searchValue+ '.*' }},
                    {email:{ $regex: '.*' + searchValue + '.*' }},
                    {lastName:{ $regex: '.*' + searchValue + '.*' }}
                ]
            }
        console.log("data1",data1)
        let skip1 = parseInt(skip)
        let limit1 = parseInt(limit)
        const data = {skip1,limit1}
        console.log(req.body)
        var result = await adminService.listUsers(data,data1)
        
        if(result){
            const json2csvParser = new Json2csvParser({});
			const csv = json2csvParser.parse(result);
            console.log(csv)
           sendResponse.sendSuccess({result}, res, 0, "user list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no user found"}, res, 0, "user list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list user catch block");
    }
}

viewUser = async(req,res) => {
  
        try
        {  
            const data = await adminService.viewUser(req.query.id)
            if(data){
                console.log(data.selectedCategory);
            var choosedCategories = await commonService.getDataByQuery(requiredFiles.categories,data.selectedCategory);
            sendResponse.sendSuccess({userData:data,choosedCategories}, res, 0, "user data fetched  successfully");
            }
            else{
                sendResponse.sendSuccess({data:"no data found"}, res, 0, "user data fetched  successfully");
            }          
        }
        catch(err)
        {
            console.log(err) 
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in view user catch block");       
        }
    
}

dashboard = async(req,res) => {
    try
    {
        var dashboardData ={}
        const userCount1 = await adminService.dashboardUserCount()
        const categoryCount1 = await categoryService.listCategory()
        const questionCount1 = await questionService.listQuestion()
        const courseCount1 = await courseService.listCourse()
        const videoCount1 = await courseVideoService.listVideos1()
        const orderCount1 = await courseVideoService.orderCount()
        dashboardData.userCount = userCount1.length
        dashboardData.categoryCount = categoryCount1.length
        dashboardData.questionCount = questionCount1.length
        dashboardData.courseCount = courseCount1.length
        dashboardData.videoCount = videoCount1.length
        dashboardData.orderCount = orderCount1.length
        console.log(dashboardData)
        sendResponse.sendSuccess({dashboardData},res,0,"dashboard data")
        
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in dashboard catch block");
    }
}

aboutUs = async(req,res) => {
    try
    {
        var key = req.body.key
        var aboutUs = await adminService.getCms(key)
        console.log(">>>>>>")
        sendResponse.sendSuccess(aboutUs,res,0,"aboutUs data")

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

listPolicies= async(req,res) => {
    try
    {
       var PolicyData = await commonService.getData(requiredFiles.termsModel,'multiple')
       
        sendResponse.sendSuccess(PolicyData,res,0,"term and condition data")

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

listContactRequests= async(req,res) => {
    try
    {
       let key = req.body.key
        var contactData = await commonService.getData(requiredFiles.contactsModel,'multiple')
       
        sendResponse.sendSuccess(contactData,res,0,"Data fetched successfully")

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

termCondition= async(req,res) => {
    try
    {
        var termCondition = await commonService.getData(requiredFiles.termsModel,'single',{'_id':ObjectId(req.body.id)})
       
        sendResponse.sendSuccess(termCondition,res,0,"term and condition data")

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

updateTermCondition = async(req,res) => {
    try
    {
        var params = {
            title_en               : req.body.title_en,
            description_en         : req.body.description_en,
            seo_title           : req.body.seo_title,
            seo_keyword         : req.body.seo_keyword,
            seo_description     : req.body.seo_description,
            update_ip           : req.body.update_ip,
            update_date         : req.body.update_date,
            updated_by          : req.body.updated_by,
        }
        
        const data = await commonService.updateData(requiredFiles.termsModel,'_id',req.body.id,params)
        
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Policy updated successfully");
        }
        
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update term cond. catch block");
    }
}

privacyPolicy= async(req,res) => {
    try
    {
       let key = req.body.key
        var termCondition = await adminService.getCms(key)
       
        sendResponse.sendSuccess(termCondition,res,0,"privacy and policy data")

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

updatePrivacyPolicy= async(req,res) => {
    try
    {
        let {key,data} = req.body
        if(!key){
            sendResponse.sendSuccess({},res,1,"key is required")
        }
        else if(!data){
            sendResponse.sendSuccess({},res,1,"privacyPolicy  is required")
        }
        else{
        var privacyPolicyResult = await adminService.updateCms({key,data})
       
        sendResponse.sendSuccess({},res,0,"privacyPolicy  data updated successfully")
        }

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update term cond. catch block");
    }
}

updateAboutUs = async(req,res) => {
    try
    {
        let {key,data} = req.body
        if(!key){
            sendResponse.sendSuccess({},res,0,"key is required")
        }
        else if(!data){
            sendResponse.sendSuccess({},res,0,"about us is required")
        }
        else{
        var aboutUsResult = await adminService.updateCms({key,data})
       
        sendResponse.sendSuccess({},res,0,"aboutUs data updated successfully")
        }

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

addAboutUs = async(req,res) => {
    try
    {
        let {aboutUs} = req.body
        if(!aboutUs){
            sendResponse.sendSuccess({},res,0,"about us is required")
        }
        else{
        var aboutUsResult = await adminService.addAboutUs(aboutUs)
       
        sendResponse.sendSuccess(aboutUsResult,res,0,"data added")
        }

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

addAboutUs1 = async(req,res) => {
    try
    {
        let {aboutUs} = req.body
        if(!aboutUs){
            sendResponse.sendSuccess({},res,0,"about us is required")
        }
        else{
        var aboutUsResult = await adminService.cms(aboutUs)
       
        sendResponse.sendSuccess(aboutUsResult,res,0,"data added")
        }

    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

blockUnblockUser = async(req,res) => {
  
    try
    {  
        console.log("herere",req.query.id,req.body.status)
        const status = req.body.status
        const id= req.query.id
        
        var message =''
        if(status ==1){
          var message ='user blocked '
        }
        else{
         var message = 'user unblocked'
        }
        const data = await adminService.viewUser(id)
        if(data)
        {
            console.log(data)
            let dataObj = {id,status}
            var result = adminService.blockUnblockUser(dataObj)
            if(result){
                sendResponse.sendSuccess('', res, 1, message);
            }
            else{
                sendResponse.sendSuccess({data:"no data found"}, res, 0, "please enter valid user id");
            }

        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "please enter valid user id");
        }          
    }
    catch(err)
    {
        console.log(err) 
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view user catch block");       
    }

}

listUser1 = async(req,res) => {
    try
    { 
         let { page_no, requested_count,  search_value } = req.body
         

         var requested_count1 = requested_count ? parseInt(requested_count) : 10  
         var skip1 = page_no ? (parseInt(page_no)) : 0
        
         var searchControl={} 
            searchControl={
                $or:[
                    {firstName:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }},
                    {lastName:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1}
        var result = await adminService.listUsers(pageControl,searchControl)
        
        if(result){
            // const json2csvParser = new Json2csvParser({});
			// const csv = json2csvParser.parse(result);
            // console.log(csv)
           sendResponse.sendSuccess(result, res, 0, "user list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no user found"}, res, 0, "user list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list user catch block");
    }
}

generateToken = async(data) =>{
    return jwt.sign({id:data._id},'*sssktAf9VcK6%Pv')
}

addFaq = async(req,res) => {
    try
    {
        let {question,answer} = req.body
   
        var addfaqResult = await adminService.addFaq({question,answer})
       
        sendResponse.sendSuccess(addfaqResult,res,0,"faq added")
       
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in about us catch block");
    }
}

listFaq = async(req,res) => {
    try
    { 
       let { page_no, requested_count} = req.body
       var requested_count1 = requested_count ? parseInt(requested_count) : 10  
       var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
       const pageControl = {skip1,requested_count1}
       var result = await adminService.listFaq(pageControl,{})
       if(result){
          sendResponse.sendSuccess(result, res, 0, "faq list fetched  successfully");
       }
       else{
           sendResponse.sendSuccess({result:"no faq found"}, res, 0, "faq list fetched  successfully");
       } 
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list Faq catch block");
    }
}

viewFaq = async(req,res) => {
    try
    { 
       let {id} = req.body
      
       var result = await adminService.viewFaq(id)
       if(result){
          sendResponse.sendSuccess(result, res, 0, "faq detailed fetched  successfully");
       }
       else{
           sendResponse.sendSuccess({result:"no faq found"}, res, 0, "faq detailed fetched  successfully");
       } 
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view faq catch block");
    }
}

updateFaq = async(req,res) => {
    try
    {
        let {id,question,answer} = req.body
   
        var addfaqResult = await adminService.updateFaq({id,question,answer})
       
        sendResponse.sendSuccess(addfaqResult,res,0,"faq updated")
       
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update faq catch block");
    }
}

exportUserCsv = async(req,res) => {
    try
    { 
         let { page_no, requested_count,  search_value } = req.body
         

         var requested_count1 =  10  
         var skip1 =  0
        
         var searchControl={} 
            searchControl={
                $or:[
                    {firstName:{ }},
                    {email:{  }},
                    {lastName:{}}
                ]
            }
        const pageControl = {skip1,requested_count1}
        var result = await adminService.listUsersCsv(pageControl,searchControl)
        
        if(result){
            //const json2csvParser = new Json2csvParser({});
			//const csv = json2csvParser.parse(result);
            //res.set('Content-Type', 'application/octet-stream');
			//res.attachment(`${__dirname}/../public/payments.csv`);
			//res.status(200).send(csv);
           sendResponse.sendSuccess(result, res, 0, "user csv list fetched  successfully");
        }
        else{
           
            sendResponse.sendSuccess({result:"no user found"}, res, 0, "user csv list fetched  successfully");
        }
    }
    catch(err){
        
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list user catch block");
    }
}

getCustomerPayments = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $and:[
                    {"purchase_type":{$ne:"premium"}},
                ],
                $or:[
                    {first_name:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.paymentModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.paymentModel,'multiple',{"purchase_type":{$ne:"premium"}});
           
        if(data !== null)
        {
           
            sendResponse.sendSuccess({data,totalRows}, res, 0, "instructor list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({instructorList:"no data found"}, res, 0, "instructor list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list instructor catch block");
        
    }
}

viewPaymentDetails = async(req,res) => {
  
    try
    {  
        var paymentDetails = await commonService.getData(requiredFiles.paymentModel,'single',{'_id':req.query.id});
        if(paymentDetails._id !== null){
            var courseDetails = await commonService.getData(requiredFiles.coursesModel,'single',{'_id':paymentDetails.course_id});
            sendResponse.sendSuccess({paymentDetails,courseDetails}, res, 0, "Data fetched  successfully");
        }
       else{
        sendResponse.sendCustomError({}, res, 1, "No Data Fount");
       }
                  
    }
    catch(err)
    {
        console.log(err) 
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view instructor catch block");       
    }

}
}

module.exports = new admin()