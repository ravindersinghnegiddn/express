const categoryService = require('../../services/adminServices/categoryServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
var fs = require('fs')
//const categoryServices = require('../services/categoryServices')
const { ObjectId } = require('mongodb');
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
class category {
constructor(){}

/* * *********************************************************************
* * Function name : addCategory
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addCategory = async(req,res) => {
    try{
        var category_insert = requiredFiles.categories;
        let params = new category_insert({
            name            : req.body.name,
            title           : req.body.title ,
            alias           : slugify(req.body.name),
            icon            : req.body.icon,
            category_image  : req.body.category_image,
            web_icon_dark   : req.body.web_icon_dark,
            web_icon_light  : req.body.web_icon_light,
            creation_ip     : req.body.creation_ip,
            creation_date   : req.body.creation_date,
            created_by      : req.body.created_by,
            status          : req.body.status,
        })
         
        const data = await commonService.insertData(params);
       
        if(data){
            sendResponse.sendSuccess(data, res, 0, "category added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding category");
        }
        
    }
    catch(err){
    
		//console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateCategory
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCategory = async(req,res) => {
    try{
        var category_slug   =   slugify(req.body.name);
        var params = {
            name            : req.body.name,
            title           : req.body.title,
            alias           : category_slug,
            icon            : req.body.icon,
            category_image  : req.body.category_image,
            web_icon_dark   : req.body.web_icon_dark,
            web_icon_light  : req.body.web_icon_light,
            update_ip       : req.body.update_ip,
            update_date     : req.body.update_date,
            updated_by      : req.body.updated_by,
        }
        
        const data = await categoryService.checkCategoryNameUpdate(category_slug,req.body.id)
        
        if(data){
            sendResponse.sendCustomError({ }, res, 4, "category name already exist");
        }
        else{
            const data = await commonService.updateData(requiredFiles.categories,'_id',req.body.id,params);
            sendResponse.sendSuccess(data, res, 0, "category  updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : updateCategoryStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCategoryStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.categories,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "category status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

listCategory = async(req,res) => {
    try{
         
        const data = await categoryService.listCategory()
       
        if(data){
        
        sendResponse.sendSuccess({data}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

listCategory1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)

        const data = await categoryService.listCategory1(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

viewCategory = async(req,res) => {
    try{
 
        const data = await categoryService.viewCategory(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "category data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteCategory
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteCategory = async(req,res) => {
    try{
        const data = await categoryService.deleteCategory(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "category deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing category");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete category catch block");
        
    }
}


}

module.exports = new category()