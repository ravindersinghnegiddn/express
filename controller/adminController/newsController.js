const categoryService = require('../../services/adminServices/categoryServices')
const newsServices = require('../../services/adminServices/newsServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
var fs = require('fs')
//const categoryServices = require('../services/categoryServices')
const { ObjectId } = require('mongodb');
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

class news {
constructor(){}

 /* * *********************************************************************
* * Function name : addNews
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

addNews = async(req,res) => {
    try{
        var news_insert = requiredFiles.newsnModel;
        let params = new news_insert({
            newsHeadline    : req.body.newsHeadline,
            newsDetail      : req.body.newsDetail ,
            newsThumbnail   : req.body.newsThumbnail,
            newsUrl         : req.body.newsUrl,
            creation_ip     : req.body.creation_ip,
            creation_date   : req.body.creation_date,
            created_by      : req.body.created_by,
            status          : req.body.status,
        })
        const data = await commonService.insertData(params)

        if(data){
        sendResponse.sendSuccess(data, res, 0, "news added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding news");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in add news controller catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateNews
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

updateNews = async(req,res) => {
    try{
        const newsData = await newsServices.viewNews(req.body.id)   //check newsId
        if(newsData!==null)
        {
            var params = {
                newsHeadline    : req.body.newsHeadline,
                newsDetail      : req.body.newsDetail ,
                newsThumbnail   : req.body.newsThumbnail,
                newsUrl         : req.body.newsUrl,
                update_ip       : req.body.update_ip,
                update_date     : req.body.update_date,
                updated_by      : req.body.updated_by,
            }
                const data = await commonService.updateData(requiredFiles.newsnModel,'_id',req.body.id,params)  //update news
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "News updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating News");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "News Id is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updatNewsStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updatNewsStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.newsnModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "category status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listNews
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listNews= async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:''
        var searchControl={} 
            searchControl={
                $or:[
                    {newsHeadline:{ $regex: '.*' + search_value+ '.*' }},
                    {newsDetail:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await newsServices.listNews(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "news list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "news list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list news catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewNews
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewNews = async(req,res) => {
    try{
 
        const data = await newsServices.viewNews(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({newsData:data}, res, 0, "news data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "news  data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view news catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteNews
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
deleteNews = async(req,res) => {
    try{
       
        const data = await newsServices.deleteNews(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({newsData:data}, res, 1, "news deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing news");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete news catch block");
        
    }
}


}

module.exports = new news()