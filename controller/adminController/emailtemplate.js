const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var slugify = require('slugify')
const { ObjectId } = require('mongodb');

class emailtemplate{
    constructor(){}

/* * *********************************************************************
* * Function name : addEmailTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addEmailTemplate = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.emailTemplateModel,'single',{mail_type_data:req.body.mail_type_data});
        
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Template already exists");
        }
        else{
            var email_inserty = requiredFiles.emailTemplateModel;
            let params = new email_inserty({
                mail_type               :   req.body.mail_type,
                from_email              :   req.body.from_email,
                to_email                :   req.body.to_email,
                bcc_email               :   req.body.bcc_email,
                subject                 :   req.body.subject,
                mail_header             :   req.body.mail_header,
                mail_body               :   req.body.mail_body,
                mail_footer             :   req.body.mail_footer,
                html                    :   req.body.html,
                mail_type_data          :   req.body.mail_type_data,
                creation_ip             :   req.body.creation_ip,
                creation_date           :   req.body.creation_date,
                created_by              :   req.body.created_by,
                status                  :   req.body.status,
            })
            
            const data = await commonService.insertData(params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Inserted");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateEmailTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateEmailTemplate = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.emailTemplateModel,'single',{mail_type_data:req.body.mail_type_data,_id:{$ne:ObjectId(req.body.id)}});
        console.log(req.body.id)
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Country already exists");
        }
        else{
            var params = {
                mail_type               :   req.body.mail_type,
                from_email              :   req.body.from_email,
                to_email                :   req.body.to_email,
                bcc_email               :   req.body.bcc_email,
                subject                 :   req.body.subject,
                mail_header             :   req.body.mail_header,
                mail_body               :   req.body.mail_body,
                mail_footer             :   req.body.mail_footer,
                html                    :   req.body.html,
                update_ip               :   req.body.update_ip,
                update_date             :   req.body.update_date,
                updated_by              :   req.body.updated_by,
            }
            
            const data = await commonService.updateData(requiredFiles.emailTemplateModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateEmailTemplateStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateEmailTemplateStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.emailTemplateModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listEmailTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listEmailTemplate = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {subject:{ $regex: '.*' + search_value+ '.*' }},
                    {mail_type:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.emailTemplateModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.emailTemplateModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewEmailTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewEmailTemplate = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.emailTemplateModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({templateData:data}, res, 0, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteEmailTemplate
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteEmailTemplate = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.emailTemplateModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({templateData:data}, res, 1, "deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}

}
module.exports = new emailtemplate()