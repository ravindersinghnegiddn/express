const eventService = require('../../services/adminServices/eventServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
const Agora = require("agora-access-token");
const sendNotification = require('../../pushNotification/pushNotification')
var slugify = require('slugify')
var fs = require('fs')

const { ObjectId } = require('mongodb');


class event {
constructor(){}
 
addEvent = async(req,res) => {
    try{
        let {name,eventDescription,icon,scheduledOn} = req.body
      
        let slugedName = slugify(req.body.name)
      
        var basImage= icon
        
        var imageUrl = await imageService.uploadCloudinary(basImage)


       // var imageUrl = "jgfhefghjegfhjeg"

        var adminId = req.data.admin_id
        console.log("adminId",adminId)
        const data = await eventService.addEvent({name,slugedName,eventDescription,imageUrl,scheduledOn,adminId})
       
        if(data){
        sendResponse.sendSuccess(data, res, 0, "event added successfully");
        }
        else{
        sendResponse.sendCustomError({ }, res, 4, "error while adding event");
        }  
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in add event catch block");
        
    }
}

updateEvent = async(req,res) => {
    try{
       

        let {id,name,eventDescription,icon,scheduledOn} = req.body
        let slugedName = slugify(req.body.name)
        const eventData = await eventService.viewEvent(id)   //check eventId
        var basImage= icon
        var imageUrl = await imageService.uploadCloudinary(basImage)
         //var imageUrl = 'frgrrrg'
        
        if(eventData!==null)
        {
            
                const data = await eventService.updateEvent({id,name,eventDescription,imageUrl,slugedName,scheduledOn})  //update events
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "event updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating event");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update event catch block");
        
    }
}

updateEventStatus = async(req,res) => {
    try{
       

        let {id,status} = req.body
        
        const eventData = await eventService.viewEvent(id)   //check newsId

        if(eventData!==null)
        {
            
            const data = await eventService.updateEventStatus({id,status})  //update events
        
            if(data){
                onsole.log(data)
                    sendResponse.sendSuccess(data, res, 0, "event status  updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while updating event status");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in update event status catch block");
        
    }
}

listEvent = async(req,res) => {
    try{
         
        const data = await categoryService.listCategory()
       
        if(data){
        
        sendResponse.sendSuccess({data}, res, 0, "category list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "category list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCategory catch block");
        
    }
}

listEvent1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        var searchControl={} 
            searchControl={
                $and:[
                    
                    {isAdmin:true},
                   
                ],
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await eventService.listEvent1(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "event list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "event list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listevent controller catch block");
        
    }
}

viewEvent = async(req,res) => {
    try{
 
        const data = await eventService.viewEvent(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({eventData:data}, res, 0, "event data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "event data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}

deleteEvent = async(req,res) => {
    try{
        console.log("<><><><<><><><><><")
        const data = await eventService.deleteEvent(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "event deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleating event");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in delete event catch block");
        
    }
}

generateToken = async(req,res) => {
    try{
       
        let {id} = req.body
     
        const eventData = await eventService.viewEvent(id)   //check eventId
         
        if(eventData!==null)
        {
            const tokenData = this.generateRtcToken(eventData.name)

            const data = await eventService.updateEventToken({id,tokenData})  //update event token
        
            if(data){
                console.log(eventData)
                sendResponse.sendSuccess({channelName:eventData.name,token:tokenData}, res, 0, "event updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while generating token");
            }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "eventId is invalid");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in generate token catch block");
        
    }
}

generateRtcToken = (name) => {
  const appID =  "7190d23ee4f5432998d03def0da38805";
  const appCertificate = '4e438ae6c34445c687eb8b4a552d3e64';
  const expirationTimeInSeconds = 36000;
  const uid =0;
  const role = Agora.RtcRole.PUBLISHER 
  const currentTimestamp = Math.floor(Date.now() / 1000);
  const expirationTimestamp = currentTimestamp + expirationTimeInSeconds;

  const token = Agora.RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, name, uid, role, expirationTimestamp);
  
  return(token)
}

startRecording = async(req,res) => {
    try{
       

       
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in start recording  catch block");
        
    }
}




}

module.exports = new event()