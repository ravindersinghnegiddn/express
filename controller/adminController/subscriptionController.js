const categoryService = require('../../services/adminServices/categoryServices')
const subscriptionService = require('../../services/adminServices/subscriptionServices')
const imageService = require('../../services/adminServices/imageServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
var fs = require('fs')
//const categoryServices = require('../services/categoryServices')
const { ObjectId } = require('mongodb');

class subscription {
constructor(){}
 
addSubscription = async(req,res) => {
    try{
        
         const {subscriptionType,subscriptionDetail,subscriptionPrice} = req.body
         const data = await subscriptionService.addSubscription({subscriptionType,subscriptionDetail,subscriptionPrice})
       
        if(data){
        sendResponse.sendSuccess(data, res, 0, "subscription added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding subscription");
        }
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in add subscription catch block");
        
    }
}





listSubscription = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
        var searchControl={} 
            searchControl={
                $or:[
                    {subscriptionType :{ $regex: '.*' + search_value+ '.*' }},
                    //{subscriptionPrice:{ $regex: '.*' + parseInt(search_value) + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl)

        const data = await subscriptionService.listSubscription(searchControl,pageControl)
       
        if(data){
        
        sendResponse.sendSuccess(data, res, 0, "subscription list fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "subscription list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list subscription catch block");
        
    }
}

viewSubscription = async(req,res) => {
    try{
 
        const data = await subscriptionService.viewSubscription(req.query.id)
        if(data){
       
        sendResponse.sendSuccess({subscriptionData:data}, res, 0, "subscription data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "subscription data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view subscription catch block");
        
    }
}

deleteSubscription= async(req,res) => {
    try{
        
        const data = await subscriptionService.deleteSubscription(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "subscription deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleating subscription");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete category catch block");
        
    }
}

updateSubscription = async(req,res) => {
    try{
        
        const {id,subscriptionType,subscriptionDetail,subscriptionPrice} = req.body
        
        const subscriptionData = await subscriptionService.viewSubscription(id)   //check questionId
        
        
        if(subscriptionData!==null)
        {
            
                const data = await subscriptionService.updateSubscription({id,subscriptionType,subscriptionDetail,subscriptionPrice})  //update question
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "subscription updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating subscription");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "questionId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updateQuestion catch block");
        
    }
}


}

module.exports = new subscription()