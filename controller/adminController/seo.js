const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var slugify = require('slugify')
const { ObjectId } = require('mongodb');

class Seo{
    constructor(){}

/* * *********************************************************************
* * Function name : addSeo
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addSeo = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.seoModel,'single',{page_slug:req.body.page_slug});
        
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Title already exists");
        }
        else{
            var email_inserty = requiredFiles.seoModel;
            let params = new email_inserty({
                page_name               :   req.body.page_name,
                page_slug               :   req.body.page_slug,
                seo_title               :   req.body.seo_title,
                seo_keyword             :   req.body.seo_keyword,
                seo_description         :   req.body.seo_description,

                creation_ip             :   req.body.creation_ip,
                creation_date           :   req.body.creation_date,
                created_by              :   req.body.created_by,
                status                  :   req.body.status,
            })
            
            const data = await commonService.insertData(params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Inserted");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateSeo
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateSeo = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.seoModel,'single',{page_slug:req.body.page_slug,_id:{$ne:ObjectId(req.body.id)}});
        console.log(req.body.id)
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Seo already exists");
        }
        else{
            var params = {
                page_name               :   req.body.page_name,
                page_slug               :   req.body.page_slug,
                seo_title               :   req.body.seo_title,
                seo_keyword             :   req.body.seo_keyword,
                seo_description         :   req.body.seo_description,

                update_ip               :   req.body.update_ip,
                update_date             :   req.body.update_date,
                updated_by              :   req.body.updated_by,
            }
            
            const data = await commonService.updateData(requiredFiles.seoModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateSeoStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateSeoStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.seoModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listSeo
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listSeo = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {page_name:{ $regex: '.*' + search_value+ '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.seoModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.seoModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewSeo
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewSeo = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.seoModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({seoData:data}, res, 0, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteSeo
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteSeo = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.seoModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({seoData:data}, res, 1, "deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}

}
module.exports = new Seo()