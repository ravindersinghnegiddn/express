const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

const { ObjectId } = require('mongodb');
class retreat{
    constructor(){}
    /* * *********************************************************************
* * Function name : getCorporateData
* * Developed By : Tejaswi
* * Purpose  : This function use for get About Us Web
* * Date : 13 MAY 2021
* * **********************************************************************/

getRetreatData = async(req,res) => {

    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
        
        var searchControl={} 
            searchControl={
                $or:[
                    {title:{ $regex: '.*' + search_value+ '.*' }},
                    {sub_title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        
        var Query = '';
        var Query = requiredFiles.retreatModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.retreatModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "Data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"No Data Found"}, res, 0, "No Data Found");
        }   
        
    }
    catch(err){
    
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
    }
    
   
    /* * *********************************************************************
    * * Function name : updateRetreatOne
    * * Developed By : Tejaswi
    * * Purpose  : This function used for add edit data
    * * Date : 13 FEB 2020
    * * **********************************************************************/
    updateRetreatOne = async(req,res) => {
        try{

            var params = {
                title:req.body.title,
                sub_title:req.body.sub_title,
                description:req.body.description,
                image:req.body.image,
                description_title:req.body.description_title,
                description_sub_title:req.body.description_sub_title,
                why_to_join_title:req.body.why_to_join_title,
                why_to_join_description:req.body.why_to_join_description,
                level_1:req.body.level_1,
                level_2:req.body.level_2,
                level_3:req.body.level_3
            }
            
            const data = await commonService.updateData(requiredFiles.retreatModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
            
        }
        catch(err){
            console.log(err)
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
            
        }
    }

    /* * *********************************************************************
    * * Function name : updateRetreatTwo
    * * Developed By : Tejaswi
    * * Purpose  : This function used for add edit data
    * * Date : 13 FEB 2020
    * * **********************************************************************/
    updateRetreatTwo = async(req,res) => {
        try{

            var benifitsArray = [];
            if((typeof(req.body.benifits)) == 'string'){
                var benifitsArray = JSON.parse(req.body.benifits)     
            }
            var benifitsArray     = JSON.parse(JSON.stringify(benifitsArray));

            var params = {
                title:req.body.title,
                sub_title:req.body.sub_title,
                description:req.body.description,
                image:req.body.image,
                description_title:req.body.description_title,
                description_sub_title:req.body.description_sub_title,
                why_to_join_title:req.body.why_to_join_title,
                why_to_join_description:req.body.why_to_join_description,
                what_you_will_learn:req.body.what_you_will_learn,
                what_you_do:req.body.what_you_do,
                benifits:benifitsArray,
            }
            
            const data = await commonService.updateData(requiredFiles.retreatModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
            
        }
        catch(err){
            console.log(err)
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
            
        }
    }
   
    
    /* * *********************************************************************
    * * Function name : viewRetrat
    * * Developed By : Tejaswi
    * * Purpose  : This function used for add edit data
    * * Date : 13 FEB 2020
    * * **********************************************************************/
    
    viewRetrat = async(req,res) => {
        try{
     
            const data = await commonService.getData(requiredFiles.retreatModel,'single',{'_id':ObjectId(req.query.id)})
            if(data){
           
            sendResponse.sendSuccess({retreatData:data}, res, 0, "data fetched  successfully");
            }
            else{
                sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
            }
     
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
            
        }
    }

}

module.exports = new retreat();