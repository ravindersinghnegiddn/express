
const sendResponse = require('../../util/CustomResponse')
const instructorService = require('../../services/adminServices/instructorService')
const sha256 = require('sha256')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');


class instructor {
constructor(){}



addInstructor = async(req,res) => {
    try{
      
        var {email,name,password} = req.body

        var data= {
            email:email,
            name:name,
            password:sha256(password)
         }


        var instructorResult = await instructorService.addInstructor(data)

        sendResponse.sendSuccess(instructorResult, res, 0, "instructor added successfully");
            
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addQuestion catch block");
        
    }
}

listInstructor = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        
        var searchControl={} 
            searchControl={
                $or:[
                    {name:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value+ '.*' }},
                ]
            }
        const pageControl = {skip1,requested_count1} 
                
 
        const instructorList = await instructorService.listInstructor(searchControl,pageControl)
      
        
        if(instructorList.data.length>0)
        {
           
            sendResponse.sendSuccess(instructorList, res, 0, "instructor list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({instructorList:"no data found"}, res, 0, "instructor list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list instructor catch block");
        
    }
}

viewInstructor = async(req,res) => {
  
    try
    {  
        const data = await instructorService.viewInstructor(req.query.id)
        if(data){
        var choosedCategories = await commonService.getDataByQuery(requiredFiles.categories,data.selectedCategory);
        var certificates = await commonService.getData(requiredFiles.certiModel,'multiple',{'instructor_id':req.query.id});
        sendResponse.sendSuccess({instructorData:data,choosedCategories,certificates}, res, 0, "instructor data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "instructor data fetched  successfully");
        }          
    }
    catch(err)
    {
        console.log(err) 
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view instructor catch block");       
    }

}

blockUnblockInstructor = async(req,res) => {
  
    try
    {  
        //console.log("herere",req.query.id,req.body.status)
        const status = req.body.status
        const id= req.query.id
        
        var message =''
        if(status ==1){
          var message ='instructor blocked '
        }
        else{
         var message = 'instructor unblocked'
        }
        const data = await instructorService.viewInstructor(id)
        if(data)
        {
            console.log(data)
            let dataObj = {id,status}
            var result = instructorService.blockUnblockInstructor(dataObj)
            if(result){
                sendResponse.sendSuccess('', res, 1, message);
            }
            else{
                sendResponse.sendSuccess({data:"no data found"}, res, 0, "please enter valid instructor id");
            }

        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "please enter valid instructor id");
        }          
    }
    catch(err)
    {
        console.log(err) 
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in block unblock instructor catch block");       
    }

}

getInstructorPayments = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $and:[
                    {"purchase_type":"premium"},
                ],
                $or:[
                    {first_name:{ $regex: '.*' + search_value+ '.*' }},
                    {email:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.paymentModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.paymentModel,'multiple',{"purchase_type":"premium"});
           
        if(data !== null)
        {
           
            sendResponse.sendSuccess({data,totalRows}, res, 0, "instructor list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({instructorList:"no data found"}, res, 0, "instructor list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in list instructor catch block");
        
    }
}

viewPaymentDetails = async(req,res) => {
  
    try
    {  
        var paymentDetails = await commonService.getData(requiredFiles.paymentModel,'single',{'_id':req.query.id});
        sendResponse.sendSuccess(paymentDetails, res, 0, "Data fetched  successfully");
                  
    }
    catch(err)
    {
        console.log(err) 
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view instructor catch block");       
    }

}
}

module.exports = new instructor()