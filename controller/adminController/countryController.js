const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var slugify = require('slugify')
const { ObjectId } = require('mongodb');

class country{
    constructor(){}

/* * *********************************************************************
* * Function name : addCountry
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addCountry = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.countryModel,'single',{country_slug:slugify(req.body.country_name).toLowerCase()});
        
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Country already exists");
        }
        else{
            var country_insert = requiredFiles.countryModel;
            let params = new country_insert({
                country_name            : req.body.country_name,
                country_slug            : slugify(req.body.country_name).toLowerCase(),
                country_used            : req.body.country_used,
                creation_ip             : req.body.creation_ip,
                creation_date           : req.body.creation_date,
                created_by              : req.body.created_by,
                status                  : req.body.status,
            })
            
            const data = await commonService.insertData(params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Inserted");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateCountry
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCountry = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.countryModel,'single',{country_slug:slugify(req.body.country_name).toLowerCase(),_id:{$ne:ObjectId(req.body.id)}});
        console.log(req.body.id)
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "Country already exists");
        }
        else{
            var params = {
                country_name            : req.body.country_name,
                country_slug            : slugify(req.body.country_name).toLowerCase(),
                country_used            : req.body.country_used,
                update_ip               : req.body.update_ip,
                update_date             : req.body.update_date,
                updated_by              : req.body.updated_by,
            }
            
            const data = await commonService.updateData(requiredFiles.countryModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateJobStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCountryStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.countryModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listCountry
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listCountry = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {country_name:{ $regex: '.*' + search_value+ '.*' }},
                    {country_slug:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.countryModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.countryModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewCountry
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewCountry = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.countryModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteCountry
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteCountry = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.countryModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}

}
module.exports = new country()