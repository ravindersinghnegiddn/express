const questionService = require('../../services/adminServices/questionServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
const bannerServices = require('../../services/adminServices/bannerServices')
const coursesServices = require('../../services/adminServices/courseServices')
const { ObjectId } = require('mongodb');
const { json } = require('body-parser');
const imageService = require('../../services/adminServices/imageServices')
const videoService = require('../../services/adminServices/videoServices')
const videoController = require('../../videoUpload/videoController')
const courseCategoryService = require('../../services/adminServices/courseCategoryService')
const sendNotification = require('../../pushNotification/pushNotification')
const orderService = require('../../services/adminServices/orderServices')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

class banner {
constructor(){}


addCourse = async(req,res) => {
    try{
      
       let {categories,courseName,courseDescription,courseTagLine,courseGoal,courseThumbnail,coursePrice,courseAuthor,aboutAuthor,startDate,duration,videoLink,authorImage,folderId} = req.body
       var categoryArray =[]
       let slugedName = slugify(courseName)
       var a = folderId.split('/')
       var folderId1 = a[a.length-1]    
       var newArrayVideo = videoLink[0]
       var c = newArrayVideo.split('/')
       var videoId1 = c[c.length-1]
      
       var videoObject=[]
       videoLink.forEach((element) =>{
          videoObject.push({
              videoUrl:element
          })
       })
       console.log(videoObject)
       if((typeof(categories)) == 'string'){
           var categoryArray = JSON.parse(categories)     
       }
       var categoryArray = JSON.parse(JSON.stringify(categories))  //convert into array
       var videoData = await videoService.addVideoTrailer(videoObject)
       console.log("video data---------->",videoData)
        var imageUrl = await imageService.uploadCloudinary(req.body.courseThumbnail)
        var authorImageUrl = await imageService.uploadCloudinary(req.body.authorImage)
        let data = {categoryArray,courseName,courseDescription,courseTagLine,courseGoal,imageUrl,coursePrice,courseAuthor,aboutAuthor,slugedName,startDate,duration,videoData,authorImageUrl,folderId1,videoId1}
        const courseData = await coursesServices.addCourse(data)
        console.log("course Data---------->",courseData)
        if(courseData._id != undefined){
            var courseCategoryObject=[]
            categoryArray.forEach((element) =>{
                courseCategoryObject.push({
                    courseId:courseData._id,
                    categoryId:element
                })
            })
            var courseCategoryData = await courseCategoryService.addCourseCategory(courseCategoryObject)
            console.log("courseCategoryData------",courseCategoryData)
            //console.log("courseCategorydata-----",courseCategoryObject)
            sendResponse.sendSuccess(courseData, res, 0, "course added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding course");
        }     
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addCourse catch block");   
    }
}

updateCourse = async(req,res) => {
    try{
        
        let {id,categories,courseName,courseDescription,courseThumbnail,coursePrice,courseAuthor,startDate,duration,videoLink} = req.body
      
        
        if((typeof(req.body.categories)) == 'string')
        {

            var categoryArray = JSON.parse(categories)     
        }
        else{
          var categoryArray = categories.split(',');
        }
         
        const courseData = await coursesServices.viewCourse(id)   //verify course id
        
        
        if(courseData!==null)
        { 
                let slugedName = slugify(req.body.courseName)
                let courseData = {id,categoryArray,courseName,courseDescription,courseThumbnail,coursePrice,courseAuthor,startDate,duration,videoLink,slugedName}     
                const data = await coursesServices.updateCourse(courseData)  //update course
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "course updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating course");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updatecourse catch block");
        
    }
}

listCourse = async(req,res) => {
    try{
         
        const courseList = await coursesServices.listCourse()
       
        if(courseList.length>0)
        {
            console.log(courseList)
            sendResponse.sendSuccess({courseList}, res, 0, "course list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}

listBanner = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        
        var searchControl={} 
            searchControl={
             
                $or:[
                    
                    {title_1:{ $regex: '.*' + search_value+ '.*' }},
                    {title_2:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
              
 
        const bannerList = await bannerServices.listBanner(searchControl,pageControl)
      
        
        if(bannerList.data.length>0)
        {
            
            sendResponse.sendSuccess(bannerList, res, 0, "banner list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({bannerList:"no data found"}, res, 0, "banner list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}

viewBanner = async(req,res) => {
    try{
         
        const data = await bannerServices.viewBanner(req.query.id)
        if(data){
        console.log(data)
        sendResponse.sendSuccess({bannerData:data}, res, 0, "banner detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "banner detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view banner catch block");
        
    }
}

deleteBanner = async(req,res) => {
    try{
    
       
        const data = await bannerServices.deleteBanner(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({questionData:data}, res, 1, "banner deleted successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "banner deleted  successfully");
        } 

      
        
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete course catch block");
        
    }
}

addBanner = async(req,res) => {
    try{
        
       var banner_insert = requiredFiles.bannerModel;
       let params = new banner_insert({
        title_1         :   req.body.title_1,
        sub_title_1     :   req.body.sub_title_1,
        title_2         :   req.body.title_2,
        sub_title_2     :   req.body.sub_title_2,
        banner_type     :   req.body.banner_type,
        page_url        :   req.body.page_url,
        page_type       :   req.body.page_type,
        video_url       :   req.body.video_url,
        banner_image    :   req.body.banner_image,
        creation_ip     :   req.body.creation_ip,
        creation_date   :   req.body.creation_date,
        created_by      :   req.body.created_by,
        status          :   req.body.status,
        })
       const bannerData = await commonService.insertData(params)
       //console.log('>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<'+bannerData);
       if(bannerData._id != undefined){
        
            sendResponse.sendSuccess(bannerData, res, 0, "banner added successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while adding banner");
        }     
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addBanner catch block");   
    }
}

updateBanner = async(req,res) => {
    try{
        
       var  params = {
        title_1         :   req.body.title_1,
        sub_title_1     :   req.body.sub_title_1,
        title_2         :   req.body.title_2,
        sub_title_2     :   req.body.sub_title_2,
        banner_type     :   req.body.banner_type,
        page_url        :   req.body.page_url,
        page_type       :   req.body.page_type,
        video_url       :   req.body.video_url,
        banner_image    :   req.body.banner_image,
        update_ip       :   req.body.update_ip,
        update_date     :   req.body.update_date,
        updated_by      :   req.body.updated_by,
        }
       const bannerData = await commonService.updateData(requiredFiles.bannerModel,'_id',req.body.id,params);
      
       if(bannerData.nModified == 1){
        
            sendResponse.sendSuccess(bannerData, res, 0, "banner updated successfully");
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "error while updating banner");
        }     
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addBanner catch block");   
    }
}

/* * *********************************************************************
* * Function name : updatBannerStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updatBannerStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.bannerModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "category status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

updateCourse1 = async(req,res) => {
    try{
        
        console.log("update course 1",req.body)
        let {id,categories,courseName,courseDescription,courseTagLine,courseGoal,courseThumbnail,coursePrice,courseAuthor,aboutAuthor,startDate,duration,durationType,authorImage,folderId,trailerLink} = req.body
        //******************************************* */ 
        const courseData = await coursesServices.viewCourse(id)   //verify course id
        if(courseData!==null)
        { 
            var categoryArray =[]
            let slugedName = slugify(courseName)
            var a = folderId.split('/')
            var folderId1 = a[a.length-1] 
            var newArrayVideo = trailerLink
            var c = newArrayVideo.split('/')
            var videoId1 = c[c.length-1]
            var videoObject=[]
            videoObject.push({videoUrl:trailerLink})
            console.log(videoObject)
            if((typeof(categories)) == 'string'){
                var categoryArray = JSON.parse(categories)     
            }
            var categoryArray = JSON.parse(JSON.stringify(categories))  //convert into array
            var videoData = await videoService.addVideoTrailer(videoObject)
            console.log("video data---------->",videoData)
            var imageUrl = await imageService.uploadCloudinary(req.body.courseThumbnail)
            var authorImageUrl = await imageService.uploadCloudinary(req.body.authorImage)
            let courseData = {id,categoryArray,courseName,courseDescription,courseTagLine,courseGoal,imageUrl,coursePrice,courseAuthor,aboutAuthor,slugedName,startDate,duration,durationType,videoData,authorImageUrl,folderId1,videoId1} 
            const data = await coursesServices.updateCourse(courseData)  //update course
        
            if(data){
                    var message = courseName + 'is added .Purchase this course to see all the videos'
                    const pushResult = sendNotification.sendPush(message,courseThumbnail)
                    sendResponse.sendSuccess(data, res, 0, "course updated successfully");
                }
            else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating course");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updatecourse catch block");
        
    }
}

}

module.exports = new banner()