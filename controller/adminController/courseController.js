const questionService = require('../../services/adminServices/questionServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
const categoryServices = require('../../services/adminServices/categoryServices')
const coursesServices = require('../../services/adminServices/courseServices')
const { ObjectId } = require('mongodb');
const { json } = require('body-parser');
const imageService = require('../../services/adminServices/imageServices')
const videoService = require('../../services/adminServices/videoServices')
const videoController = require('../../videoUpload/videoController')
const courseCategoryService = require('../../services/adminServices/courseCategoryService')
const sendNotification = require('../../pushNotification/pushNotification')
const orderService = require('../../services/adminServices/orderServices')
const _ = require('lodash');
const commonService = require('../../services/userServices/commonService');
const requiredFiles = require('../../database/required');
class course {
constructor(){}



updateCourse = async(req,res) => {
    try{
        
        let {id,categories,courseName,courseDescription,courseThumbnail,coursePrice,courseAuthor,startDate,duration,videoLink} = req.body
      
        
        if((typeof(req.body.categories)) == 'string')
        {

            var categoryArray = JSON.parse(categories)     
        }
        else{
          var categoryArray = categories.split(',');
        }
         
        const courseData = await coursesServices.viewCourse(id)   //verify course id
        
        
        if(courseData!==null)
        { 
                let slugedName = slugify(req.body.courseName)
                let courseData = {id,categoryArray,courseName,courseDescription,courseThumbnail,coursePrice,courseAuthor,startDate,duration,videoLink,slugedName}     
                const data = await coursesServices.updateCourse(courseData)  //update course
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "course updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating course");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updatecourse catch block");
        
    }
}

listCourse = async(req,res) => {
    try{
         
        const courseList = await coursesServices.listCourse()
       
        if(courseList.length>0)
        {
            console.log(courseList)
            sendResponse.sendSuccess({courseList}, res, 0, "course list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}

listCourse1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        
        var searchControl={} 
            searchControl={
                
                $or:[
                    
                    {courseName:{ $regex: '.*' + search_value+ '.*' }},
                    {coursePrice:{ $regex: '.*' + search_value + '.*' }},
                    {courseAuthor:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
              
 
        const courseList = await coursesServices.listCourse1(searchControl,pageControl)
      
        
        if(courseList.data.length>0)
        {
            
            sendResponse.sendSuccess(courseList, res, 0, "course list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({courseList:"no data found"}, res, 0, "course list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listCourse catch block");
        
    }
}

viewCourse = async(req,res) => {
    try{
         
        const data = await coursesServices.viewCourse(req.query.id)
        if(data){
        console.log(data)
        sendResponse.sendSuccess({courseData:data}, res, 0, "course detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "course detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view course catch block");
        
    }
}

deleteCourse = async(req,res) => {
    try{
        const courseData = await orderService.viewOrderedCourse(req.body.id)
        if(courseData == null){
        const data = await coursesServices.deleteCourse1(req.body.id)
        if(data){
       
        sendResponse.sendSuccess({questionData:data}, res, 1, "course deleted successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "course deleted  successfully");
        } 

        }
        else{
            sendResponse.sendSuccess({}, res, 0, "this course is alreday purchased .You can not delete this course");
        }
        
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete course catch block");
        
    }
}

addCourse1 = async(req,res) => {
    try{
        
        let {categories,courseName,courseDescription,courseTagLine,courseThumbnail,coursePrice,courseAuthor,aboutAuthor,duration,durationType,authorImage,folderId,trailerLink,course_type,added_by} = req.body
        var categoryArray =[]
        let slugedName = slugify(courseName).toLowerCase()
        if(folderId){
         var a = folderId.split('/')
         var folderId1 = a[a.length-1] 
        }
        if(trailerLink){
         var newArrayVideo = trailerLink
         var c = newArrayVideo.split('/')
         var videoId1 = c[c.length-1]
         var videoObject=[]
         videoObject.push({videoUrl:trailerLink})
         var videoData = await videoService.addVideoTrailer(videoObject)
        //console.log("video data---------->",videoData)
        }
        if(req.body.eventDateTime){
            var eventDateTime = req.body.eventDateTime;
        }
        else{
         var eventDateTime = '';
        }
        console.log("eventDateTime---------->",eventDateTime)
        var instructorId = req.data.instructor_id
 
        if((typeof(categories)) == 'string'){
            var categoryArray = JSON.parse(categories)     
        }
        
        //var categoryArray = JSON.parse(JSON.stringify(categories))  //convert into array
        //console.log(categoryArray);
        
        var imageUrl = req.body.courseThumbnail
        var authorImageUrl = req.body.authorImage
        var course_details_updated = 'N'
        let data = {categoryArray,courseName,courseDescription,courseTagLine,imageUrl,coursePrice,courseAuthor,aboutAuthor,slugedName,duration,durationType,videoData,authorImageUrl,folderId1,videoId1,instructorId,course_type,added_by,course_details_updated,eventDateTime}
        const courseData = await coursesServices.addCourse(data)
        console.log("course Data---------->",courseData)
        if(courseData._id != undefined){
             var courseCategoryObject=[]
             categoryArray.forEach((element) =>{
                 courseCategoryObject.push({
                     courseId:courseData._id,
                     categoryId:element
                 })
             })
             //var courseCategoryData = await courseCategoryService.addCourseCategory(courseCategoryObject)
             //console.log("courseCategoryData------",courseCategoryData)
             var message = courseName + ' ,a new courseis added .Purchase this course to see all the videos'
             const pushResult = sendNotification.sendPush(message,courseThumbnail)
             //console.log("courseCategorydata-----",courseCategoryObject)
             sendResponse.sendSuccess(courseData, res, 0, "course added successfully");
         }
         else{
             sendResponse.sendCustomError({ }, res, 4, "error while adding course");
         }     
     }
     catch(err){
         console.log(err)
         sendResponse.sendCustomError({ "error":err }, res, 4, "error in addCourse catch block");   
     }
 }

 updateCourse1 = async(req,res) => {
    try{
        
        console.log("update course 1",req.body)
        let {id,categories,courseName,courseDescription,courseTagLine,courseThumbnail,coursePrice,courseAuthor,aboutAuthor,startDate,duration,durationType,authorImage,folderId,trailerLink,course_type,added_by} = req.body
        //******************************************* */ 
        const courseData = await coursesServices.viewCourse(id)   //verify course id
       // console.log(">>>>>>>>>>>>>>>>>>"+courseData.videoLink[0]['_id']+">>>>>>qq>>>>>>>>>>");
        if(courseData!==null)
        { 
            var categoryArray =[]
            var videoObject=[]
            console.log('???????????????????????????????'+course_type);
            
            if(folderId){
            var a = folderId.split('/')
            var folderId1 = a[a.length-1] 
            }
            if(trailerLink){
            var newArrayVideo = trailerLink
            var c = newArrayVideo.split('/')
            var videoId1 = c[c.length-1]
            videoObject.push({videoUrl:trailerLink})
            var videoData = await videoService.addVideoTrailer(videoObject)
            }
            else{
                var videoId1 = ''
            }
            
            if((typeof(categories)) == 'string'){
                var categoryArray = JSON.parse(categories)     
            }
            //var categoryArray = JSON.parse(JSON.stringify(categories))  //convert into array
            
            console.log("video data---------->",req.body.courseThumbnail)
            if(req.body.courseThumbnail){
                var imageUrl = req.body.courseThumbnail
            }
            else{
                var imageUrl = '';
            }
            if(req.body.authorImage){
                var authorImageUrl = req.body.authorImage
            }
            else{
                var authorImageUrl = '';
            }

            if(req.body.eventDateTime){
                var eventDateTime = req.body.eventDateTime;
            }
            else{
             var eventDateTime = '';
            }
            console.log("eventDateTime---------->",eventDateTime)
            let courseData = {id,categoryArray,courseName,courseDescription,courseTagLine,imageUrl,coursePrice,courseAuthor,aboutAuthor,startDate,duration,durationType,videoData,authorImageUrl,folderId1,videoId1,course_type,added_by,eventDateTime} 
            const data = await coursesServices.updateCourse(courseData)  //update course
        
            if(data){
                    var message = courseName + 'is added .Purchase this course to see all the videos'
                    //const pushResult = sendNotification.sendPush(message,courseThumbnail)
                    sendResponse.sendSuccess(data, res, 0, "course updated successfully");
                }
            else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating course");
                }
           
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "courseId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updatecourse catch block");
        
    }
}

/* * *********************************************************************
	 * * Function name : addCourseDetails
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for home page
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

addCourseDetails = async(req,res) => {
    let {features,goals,what_you_will_learn} = req.body;
    var featureArray = [];var goalArray = [];var learnArray = [];
    if((typeof(features)) == 'string'){
        var featureArray = JSON.parse(features)     
    }
    var featureArray     = JSON.parse(JSON.stringify(featureArray));

    if((typeof(goals)) == 'string'){
        var goalArray = JSON.parse(goals)     
    }
    var goalArray     = JSON.parse(JSON.stringify(goalArray));

    if((typeof(what_you_will_learn)) == 'string'){
        var learnArray = JSON.parse(what_you_will_learn)     
    }
    var learnArray     = JSON.parse(JSON.stringify(learnArray));
    
    var courseId      = req.body.courseId;
    
    var putCourseDetails = requiredFiles.coursesDetails;
        let courseData = new putCourseDetails({
            course_id : ObjectId(req.body.courseId),
            features : featureArray,
            goals : goalArray,
            what_you_will_learn :  learnArray,
        })

        var courseDetails = await commonService.insertData(courseData);
        if(courseDetails !== null){
            var uparams = {"course_details_updated":"Y"}
            var userUpdate = await commonService.updateData(requiredFiles.coursesModel,'_id',ObjectId(req.body.courseId),uparams)
        }
        sendResponse.sendSuccess({courseDetails}, res, 1, "Details Added Successfully.");
}

/* * *********************************************************************
 * * Function name : updateCourseDetails
 * * Developed By : Tejaswi
 * * Purpose  : This function use for home page
 * * Date : 13 MAY 2021
 * * **********************************************************************/

updateCourseDetails = async(req,res) => {
    let {features,goals,what_you_will_learn,details_id} = req.body;
    var featureArray = [];var goalArray = [];var learnArray = [];
    if((typeof(features)) == 'string'){
        var featureArray = JSON.parse(features)     
    }
    var featureArray     = JSON.parse(JSON.stringify(featureArray));

    if((typeof(goals)) == 'string'){
        var goalArray = JSON.parse(goals)     
    }
    var goalArray     = JSON.parse(JSON.stringify(goalArray));

    if((typeof(what_you_will_learn)) == 'string'){
        var learnArray = JSON.parse(what_you_will_learn)     
    }
    var learnArray     = JSON.parse(JSON.stringify(learnArray));
    
    var courseId      = req.body.courseId;
    
        var params = {
            course_id : ObjectId(req.body.courseId),
            features : featureArray,
            goals : goalArray,
            what_you_will_learn :  learnArray,
        }
        var courseDetails = await commonService.updateData(requiredFiles.coursesDetails,'_id',details_id,params)
        if(courseDetails !== null){
            var uparams = {"course_details_updated":"Y"}
            var userUpdate = await commonService.updateData(requiredFiles.coursesModel,'_id',ObjectId(req.body.courseId),uparams)
        }
        sendResponse.sendSuccess({courseDetails}, res, 1, "Details Updated Successfully.");
}

getCourseDetails = async(req,res) => {
    if(_.isEmpty(req.body.course_id)){
        sendResponse.sendCustomError({}, res, 0, "Please Enter Course Id");
    }
    else{
        var course_id             =     req.body.course_id;
        const courseDetails       =     await commonService.getData(requiredFiles.coursesDetails,'single',{'course_id':course_id});
        sendResponse.sendSuccess({courseDetails}, res, 1, "Data fetched successfully.");
    }
}

}

module.exports = new course()