const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

const { ObjectId } = require('mongodb');
class corporate{
    constructor(){}
    /* * *********************************************************************
* * Function name : getCorporateData
* * Developed By : Tejaswi
* * Purpose  : This function use for get About Us Web
* * Date : 13 MAY 2021
* * **********************************************************************/

getCorporateData = async(req,res) => {

    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
        
        var searchControl={} 
            searchControl={
                $or:[
                    {title:{ $regex: '.*' + search_value+ '.*' }},
                    {sub_title:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        
        var Query = '';
        var Query = requiredFiles.corporateModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.corporateModel,'multiple');
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "Data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"No Data Found"}, res, 0, "No Data Found");
        }   
        
    }
    catch(err){
    
        console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
    }
    
   
    /* * *********************************************************************
    * * Function name : updateCorporate
    * * Developed By : Tejaswi
    * * Purpose  : This function used for add edit data
    * * Date : 13 FEB 2020
    * * **********************************************************************/
    updateCorporate = async(req,res) => {
        try{

            var uspArray = [];
            if((typeof(req.body.usp)) == 'string'){
                var uspArray = JSON.parse(req.body.usp)     
            }
            var uspArray     = JSON.parse(JSON.stringify(uspArray));

            var params = {
                title:req.body.title,
                sub_title:req.body.sub_title,
                description:req.body.description,
                image:req.body.image,
                our_mission_title:req.body.our_mission_title,
                our_mission_sub_title:req.body.our_mission_sub_title,
                our_mission_description:req.body.our_mission_description,
                our_mission_image:req.body.our_mission_image,
                our_usp_title:req.body.our_usp_title,
                our_usp_sub_title:req.body.our_usp_sub_title,
                mental_issue_title:req.body.mental_issue_title,
                mental_issue_sub_title:req.body.mental_issue_sub_title,
                solutions_title:req.body.solutions_title,
                news_title:req.body.news_title,
                news_sub_title:req.body.news_sub_title,
                clients_title:req.body.clients_title,
                corporate_cate_title:req.body.corporate_cate_title,
                corporate_cate_sub_title:req.body.corporate_cate_sub_title,
                wellness_title:req.body.wellness_title,
                wellness_sub_title:req.body.wellness_sub_title,
                created_by:req.body.created_by,
                updated_by:req.body.updated_by,
                status:req.body.status,
                usp:uspArray,
            }
            
            const data = await commonService.updateData(requiredFiles.corporateModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Data Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding Data");
            }
            
        }
        catch(err){
            console.log(err)
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
            
        }
    }
   
    
    /* * *********************************************************************
    * * Function name : viewCorporate
    * * Developed By : Tejaswi
    * * Purpose  : This function used for add edit data
    * * Date : 13 FEB 2020
    * * **********************************************************************/
    
    viewCorporate = async(req,res) => {
        try{
     
            const data = await commonService.getData(requiredFiles.corporateModel,'single',{'_id':ObjectId(req.query.id)})
            if(data){
           
            sendResponse.sendSuccess({corporateData:data}, res, 0, "data fetched  successfully");
            }
            else{
                sendResponse.sendSuccess({data:"no data found"}, res, 0, "data fetched  successfully");
            }
     
        }
        catch(err){
        
            console.log(err)
            
            sendResponse.sendCustomError({ "error":err }, res, 4, "error in view catch block");
            
        }
    }

}

module.exports = new corporate();