const questionService = require('../../services/adminServices/questionServices')
const sendResponse = require('../../util/CustomResponse')
var slugify = require('slugify')
const categoryServices = require('../../services/adminServices/categoryServices')
const { ObjectId } = require('mongodb');
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

class question {
constructor(){}

/* * *********************************************************************
* * Function name : addQuestion
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addQuestion = async(req,res) => {
    try{
        
        const categoryData = await categoryServices.viewCategory(req.body.category_id)  //check category
        if(categoryData!==null){

            var question_insert = requiredFiles.questionModel;
            let params = new question_insert({
                category        : ObjectId(req.body.category_id),
                question        : req.body.question,
                creation_ip     : req.body.creation_ip,
                creation_date   : req.body.creation_date,
                created_by      : req.body.created_by,
                status          : req.body.status,
            })
            const data = await commonService.insertData(params)  //add question
       
            if(data){
                console.log(data)
                sendResponse.sendSuccess(data, res, 0, "question added successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding question");
            }
        }
        else{
            sendResponse.sendCustomError({ }, res, 4, "categoryId is invalid");
        }
            
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in addQuestion catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateQuestion
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

updateQuestion = async(req,res) => {
    try{

        const questionData = await questionService.viewQuestion(req.body.id)   //check questionId
        if(questionData!==null)
        {
            const categoryData = await categoryServices.viewCategory(req.body.category_id)  //check category
            
            if(categoryData!==null){
                let params = {
                    category        : ObjectId(req.body.category_id),
                    question        : req.body.question,
                    update_ip       : req.body.update_ip,
                    update_date     : req.body.update_date,
                    updated_by      : req.body.updated_by,
                }
                const data = await commonService.updateData(requiredFiles.questionModel,'_id',req.body.id,params)  //update question
        
                if(data){
                    console.log(data)
                    sendResponse.sendSuccess(data, res, 0, "question updated successfully");
                }
                else{
                    sendResponse.sendCustomError({ }, res, 4, "error while updating question");
                }
            }
            else
            {
                sendResponse.sendCustomError({ }, res, 4, "categoryId is invalid");
            } 
        }
        else
        {
            sendResponse.sendCustomError({ }, res, 4, "questionId is invalid");
        }

             
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in updateQuestion catch block");
        
    }
}

listQuestion = async(req,res) => {
    try{
         
        const data = await questionService.listQuestion()
        console.log(Date.now())
        if(data)
        {
            console.log(data)
            sendResponse.sendSuccess({data}, res, 0, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listquestion catch block");
        
    }
}

/* * *********************************************************************
* * Function name : listQuestion1
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listQuestion1 = async(req,res) => {
    try{
        let { page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        
        var searchControl={} 
            searchControl={
                $or:[
                    {question:{ $regex: '.*' + search_value+ '.*' }}    
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl,pageControl) 
        const data = await questionService.listQuestion1(searchControl,pageControl)
       
        if(data)
        {
          
            sendResponse.sendSuccess(data, res, 0, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listquestion catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewQuestion
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewQuestion = async(req,res) => {
    try{
         
        const data = await questionService.viewQuestion(req.query.id)
        if(data){
        //console.log(data)
        sendResponse.sendSuccess({questionData:data}, res, 0, "question detail fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question detail fetched  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view question catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updatQuestionStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updatQuestionStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.questionModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "category status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : deleteQuestion
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteQuestion = async(req,res) => {
    try{
         
        const data = await questionService.deleteQuestion(req.body.id)
        if(data){
        console.log(data)
        sendResponse.sendSuccess({questionData:data}, res, 1, "question deleted successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question deleted  successfully");
        }  
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete question catch block");
        
    }
}

listCategoryQuestion = async(req,res) => {
    try{
        let { categoryId,page_no, requested_count,  search_value } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no-1)*requested_count1) : 0
        
        var searchControl={} 
            searchControl={
                category:categoryId,
                $or:[
                    {question:{ $regex: '.*' + search_value+ '.*' }}    
                ]
            }
        const pageControl = {skip1,requested_count1} 
        console.log(searchControl,pageControl) 
        const data = await questionService.listQuestion1(searchControl,pageControl)
       
        if(data)
        {
          
            sendResponse.sendSuccess(data, res, 0, "question list fetched  successfully");
        }
        else
        {
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "question list fetched  successfully");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in listquestion catch block");
        
    }
}

}

module.exports = new question()