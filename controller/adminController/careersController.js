const sendResponse = require('../../util/CustomResponse')
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');
var slugify = require('slugify')
const { ObjectId } = require('mongodb');

class careers{
    constructor(){}

 /* * *********************************************************************
* * Function name : getCareerContent
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

getCareerContent = async(req,res) => {
    try{
        
        const data = await commonService.getData(requiredFiles.careerModel,'multiple');

        if(data){
        
        sendResponse.sendSuccess(data, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateCareersContent
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateCareersContent = async(req,res) => {
    try{
        var workArray = [];
        if(req.body.why_work_at_bliss){
            if((typeof(req.body.why_work_at_bliss)) == 'string'){
                var workArray = JSON.parse(req.body.why_work_at_bliss)     
            }
            var workArray     = JSON.parse(JSON.stringify(workArray));
        }
        console.log(workArray);
        var params = {
            mission_title          : req.body.mission_title,
            mission_sub_title      : req.body.mission_sub_title,
            mission_description    : req.body.mission_description,
            mission_image          : req.body.mission_image,
            employee_title         : req.body.employee_title,
            employee_sub_title     : req.body.employee_sub_title,
            employee_description   : req.body.employee_description,
            employee_image         : req.body.employee_image,
            vision_title           : req.body.vision_title,
            vision_sub_title       : req.body.vision_sub_title,
            vision_description     : req.body.vision_description,
            vision_image           : req.body.vision_image,
            why_work_at_bliss      : workArray,
            update_ip              : req.body.update_ip,
            update_date            : req.body.update_date,
            updated_by             : req.body.updated_by,
        }
        
        const data = await commonService.updateData(requiredFiles.careerModel,'_id',req.body.id,params)
        
        if(data){
            sendResponse.sendSuccess(data, res, 0, "Careers updated successfully");
        }
        
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : addJobOpening
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
addJobOpening = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.jobOpeningModel,'single',{job_slug:slugify(req.body.job_title)});
        console.log(slugify(req.body.job_title));
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "This opening is already available");
        }
        else{
            var job_insert = requiredFiles.jobOpeningModel;
            let params = new job_insert({
                job_title               : req.body.job_title,
                job_slug                : slugify(req.body.job_title),
                short_description       : req.body.short_description,
                description             : req.body.description,
                location                : req.body.location,
                Pacakage                : req.body.Pacakage,
                experience_required     : req.body.experience_required,
                skills_required         : req.body.skills_required,
                creation_ip             : req.body.creation_ip,
                creation_date           : req.body.creation_date,
                created_by              : req.body.created_by,
                status                  : req.body.status,
            })
            
            const data = await commonService.insertData(params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Job Posted successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding JOb");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}
/* * *********************************************************************
* * Function name : updateJobOpening
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateJobOpening = async(req,res) => {
    try{
        const existData = await commonService.getData(requiredFiles.jobOpeningModel,'single',{job_slug:slugify(req.body.job_title).toLowerCase(),_id:{$ne:ObjectId(req.body.id)}});
        console.log(req.body.id)
        if(existData){
            sendResponse.sendCustomError({ }, res, 4, "This opening is already available");
        }
        else{
            var params = {
                job_title               : req.body.job_title,
                job_slug                : slugify(req.body.job_title).toLowerCase(),
                short_description       : req.body.short_description,
                description             : req.body.description,
                location                : req.body.location,
                Pacakage                : req.body.Pacakage,
                experience_required     : req.body.experience_required,
                skills_required         : req.body.skills_required,
                update_ip               : req.body.update_ip,
                update_date             : req.body.update_date,
                updated_by              : req.body.updated_by,
            }
            
            const data = await commonService.updateData(requiredFiles.jobOpeningModel,'_id',req.body.id,params);
        
            if(data){
                sendResponse.sendSuccess(data, res, 0, "Job Updated successfully");
            }
            else{
                sendResponse.sendCustomError({ }, res, 4, "error while adding JOb");
            }
        }
    }
    catch(err){
		console.log(err)
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
        
    }
}

/* * *********************************************************************
* * Function name : updateJobStatus
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/
updateJobStatus = async(req,res) => {
    try{
        var params = {
            status      : req.body.status,
        }
        const data = await commonService.updateData(requiredFiles.jobOpeningModel,'_id',req.body.id,params);
        sendResponse.sendSuccess(data, res, 0, "category status updated successfully");
    }
    catch(err){
        console.log(err);
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in catch block");
    }
}

/* * *********************************************************************
* * Function name : listJobs
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listJobs = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {job_title:{ $regex: '.*' + search_value+ '.*' }},
                    {skills_required:{ $regex: '.*' + search_value + '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.jobOpeningModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.jobOpeningModel,'multiple');

        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewJobOpening
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewJobOpening = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.jobOpeningModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "job data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "job data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}

/* * *********************************************************************
* * Function name : deleteJob
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

deleteJob = async(req,res) => {
    try{
        const data = await commonService.deleteData(requiredFiles.jobOpeningModel,'_id',req.body.id)
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 1, "job deleted  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "error occur while deleteing job");
        }
    
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 0, "error in delete job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : listAppliedJobs
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

listAppliedJobs = async(req,res) => {
    try{
        let { page_no, requested_count } = req.body
        var requested_count1 = requested_count ? parseInt(requested_count) : 10  
        var skip1 = page_no ? (parseInt(page_no)) : 0
        var search_value = search_value?search_value:'';
       // console.log('>>>>'+search_value);
        var searchControl={} 
            searchControl={
                $or:[
                    {current_location:{ $regex: '.*' + search_value+ '.*' }}
                ]
            }
        const pageControl = {skip1,requested_count1} 
        //console.log(searchControl)
        var Query = '';
        var Query = requiredFiles.appliedJobsModel.find(searchControl)
        .skip(skip1)
        .limit(requested_count1)
        const data = await commonService.getAllByQuery(Query)
        const totalRows = await commonService.getData(requiredFiles.appliedJobsModel,'multiple');

        
       
        if(data){
        
        sendResponse.sendSuccess({data,totalRows}, res, 1, "data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "no data found");
        }   
        
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in Job catch block");
        
    }
}

/* * *********************************************************************
* * Function name : viewAppliedJobs
* * Developed By : Tejaswi
* * Purpose  : This function used for add edit data
* * Date : 13 FEB 2020
* * **********************************************************************/

viewAppliedJobs = async(req,res) => {
    try{
 
        const data = await commonService.getData(requiredFiles.applicationModel,'single',{'_id':ObjectId(req.query.id)})
        if(data){
       
        sendResponse.sendSuccess({categoryData:data}, res, 0, "Applied jobs data fetched  successfully");
        }
        else{
            sendResponse.sendSuccess({data:"no data found"}, res, 0, "Applied jobs data fetched  successfully");
        }
 
    }
    catch(err){
    
		console.log(err)
        
        sendResponse.sendCustomError({ "error":err }, res, 4, "error in view category catch block");
        
    }
}


}

module.exports = new careers();