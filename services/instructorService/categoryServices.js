const categoryModel = require('../../database/schema/instructor_category_schema')
const allCategoryModel= require('../../database/schema/categories_shema')
const instructorModel = require('../../database/schema/instructor_schema')
const { ObjectId } = require('mongodb');



class category {
    constructor(){}
    
     
    addCategory = async(name,slugedName,title,imageUrl,instructorId) =>{
        let categoryData = new categoryModel({ 
            instructorId:instructorId,
            name:name,
            alias:slugedName,
            title:title,
            icon:imageUrl
          
        })
        return new Promise((resolve,reject)=>{
            categoryData.save().then(data =>{
                resolve(true)
            }).catch(err =>{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                reject(err)
            })
        
       })
   
    }

    checkCategoryName = async(name) =>{
       
        return new Promise((resolve,reject)=>{
            categoryModel.findOne({alias:name}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkCategoryNameUpdate = async(name,id) =>{
       
        return new Promise((resolve,reject)=>{
            const query = categoryModel.findOne({alias:name});

            // selecting the `name` and `occupation` fields
            query.select('alias name');
            query.where({_id:{$ne:id}})
            //query.where({_id:id})
            
            // execute the query at a later time
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateCategory = async(id,name,slugedName,title,icon,localStorageUrl) =>{
        
        console.log(">>>>>>>>>>>>>",id,name,slugedName,title,icon,localStorageUrl)
        return new Promise((resolve,reject)=>{
            categoryModel.updateOne({_id:ObjectId(id)},{name:name,alias:slugedName,title:title,icon:icon,localUrl:localStorageUrl,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    listCategory = async(instructorId) =>{
        
        return new Promise((resolve,reject)=>{
            instructorModel.findOne({_id:instructorId})
            .skip()
            .limit()
            .populate('selectedCategory',['name'])
            .sort([['createdAt', 'desc']])
            .then(data =>{
              
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    listCategory1 = async(searchControl,pageControl) =>{
        var result ={}
        return new Promise((resolve,reject)=>{
            categoryModel.find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['createdAt', 'desc']])
            .then(data =>{

                categoryModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of category:", count );
                        resolve(result)
                    }
                    
                })

                
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    
    viewCategory= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            categoryModel.findOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(data)
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }
    deleteCategory= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            categoryModel.deleteOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(data)
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }

    listCategoryDropdown = async() =>{
        
        return new Promise((resolve,reject)=>{
            allCategoryModel.find({})
            .skip()
            .limit()
            .sort([['createdAt', 'desc']])
            .then(data =>{
              
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    getInstuctorSessions = async() =>{
        return new Promise((resolve,reject)=>{
            instructorModel.aggregate([
                {    
                    $lookup:
                    {
                        from: "bliss_courses",
                        localField: "_id",
                        foreignField: "instructorId",
                        
                        as: "courses"
                    },
                },
                {"$match": { "courses.instructorId": {$exists: true,$nin:["",null]} }} 
            ]).then(data =>{
              
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
            
        
       })
    }
    }
    
    module.exports = new category()