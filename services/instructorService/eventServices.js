const eventModel = require('../../database/schema/event_schema')
const { ObjectId } = require('mongodb');



class event {
    constructor(){}
    
     
    addEvent = async(data) =>{
        let eventData = new eventModel({ 
            name:data.name,
            alias:data.slugedName,
            eventDescription:data.eventDescription,
            icon:data.imageUrl,
            eventDateTime:data.scheduledOn,
            instructorId:data.instructorId,
            isAdmin:false
        })
        return new Promise((resolve,reject)=>{
            eventData.save().then(data =>{
                resolve(true)
            }).catch(err =>{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                reject(err)
            })
        
       })
   
    }

    checkEventName = async(name,instructorId) =>{
       
        return new Promise((resolve,reject)=>{
            eventModel.findOne({alias:name,instructorId:instructorId}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkEventNameUpdate = async(name,id,instructorId) =>{
        console.log(name,id,instructorId)
        return new Promise((resolve,reject)=>{
            const query = eventModel.findOne({alias:name,
                instructorId:instructorId});

            // selecting the `name` and `occupation` fields
            
            query.select('alias name');
            query.where({_id:{$ne:id}})
            //query.where({_id:id})
            
            // execute the query at a later time
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  console.log(data)
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateEvent= async(data) =>{
        return new Promise((resolve,reject)=>{
            eventModel.updateOne({_id:ObjectId(data.id)},{name:data.name,alias:data.slugedName,eventDescription:data.eventDescription,icon:data.imageUrl,eventDateTime:data.scheduledOn,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
       })
    }

    listCategory = async() =>{
        
        return new Promise((resolve,reject)=>{
            eventModel.find({})
            .skip()
            .limit()
            .sort([['createdAt', 'desc']])
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    listEvent1 = async(searchControl,pageControl) =>{
        var result ={}
        return new Promise((resolve,reject)=>{
            eventModel.find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['createdAt', 'desc']])
            .then(data =>{

                eventModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of event:", count );
                        resolve(result)
                    }
                    
                })

                
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    
    viewEvent= async(id,instructorId) =>{
        console.log(id,instructorId)
        return new Promise((resolve,reject)=>{
            eventModel.findOne({_id:ObjectId(id),instructorId:instructorId}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    deleteEvent= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            eventModel.deleteOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(data)
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }
    updateEventToken= async(data) =>{
        return new Promise((resolve,reject)=>{
            eventModel.updateOne({_id:ObjectId(data.id)},{token:data.tokenData,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
       })
    }

    updateEventStatus= async(data) =>{
        return new Promise((resolve,reject)=>{
            eventModel.updateOne({_id:ObjectId(data.id)},{eventStatus:data.status,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
       })
    }

    }
    
    module.exports = new event()