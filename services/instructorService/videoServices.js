
const videoModel = require('../../database/schema/video_schema')
const courseModel= require('../../database/schema/course_schema')
const courseVideoModel = require('../../database/schema/course_video_schema')
const orderModel = require('../../database/schema/order_schema')
const { ObjectId } = require('mongodb');

const Mongoose = require('mongoose')

class video {
    constructor(){}
    
        addVideoTrailer = (data) =>{
           return new Promise((resolve,reject) =>{
              videoModel.insertMany(data).then(function(data){ 
                var array =[]
                data.forEach(element => {
                    array.push(element._id)
                });

                resolve(array)  // Success 
            }).catch(function(error){ 
                reject(error)      // Failure 
            }); 

           })
        }


        viewCourse= async(id) =>{
        
            return new Promise((resolve,reject)=>{
                courseModel.findOne({_id:ObjectId(id)}).then(data =>{
                    
                    resolve(data)
                }).catch(err =>{
                    
                    reject(err)
                })
            
           })
       
        }

        addVideo = async(data) =>{
            let courseVideoData = new courseVideoModel({ 
                
                courseId:data.courseId,
                videoUrl :data.videoLink1,
                videoName:data.videoName,
                videoDescription:data.videoDescription,
                instructorId:data.instructorId,
                episodeNumber:data.episodeNumber,
                sectionId:data.sectionId,
                videoId:data.videoId1
            })
            return new Promise((resolve,reject)=>{
                courseVideoData.save().then(data =>{
                    resolve(true)
                }).catch(err =>{
                   //console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                    reject(err)
                })
            
           })
        }

        listVideos= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', 'desc']])
                .populate('courseId',['courseName','_id'])
                .populate('sectionId')
                .exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                    console.log("data",data)
                    courseVideoModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            //console.log( "Number of courses:",result);
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }

        viewVideo = async(id) =>{
        
            return new Promise((resolve,reject)=>{
                courseVideoModel.findOne({_id:ObjectId(id)})
                .populate('courseId',['_id','courseName'])
                .then(data =>{
                    
                    resolve(JSON.parse(JSON.stringify(data)))
                    //console.log('*****************'+data+'*****************');
                }).catch(err =>{
                    
                    reject(err)
                })
            
           })
       
        }
        updateVideo = async(data) =>{
        
            return new Promise((resolve,reject)=>{
                courseVideoModel.updateOne(
                    {_id:ObjectId(data.id)},
                    {courseId:ObjectId(data.courseId),
                    videoUrl :data.videoLink1,
                    videoName:data.videoName,
                    videoDescription:data.videoDescription,
                    videoId:data.videoId1,
                    //sectionNumber:data.sectionNumber,
                    episodeNumber:data.episodeNumber,
                    sectionId:data.sectionId,
                    updatedAt:Date.now()})
                    .then(data =>{
                    resolve(true)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        //dashboard count video
        listVideos1= (searchControl,pageControl) =>{
            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find({}).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                  console.log("here",data)
                   resolve(data)
                      
                  }
                
                });
            
           })

        }

        deleteVideo= async(id) =>{ 
            return new Promise((resolve,reject)=>{
                courseVideoModel.deleteOne({_id:ObjectId(id)})
                .then(data =>{
                    resolve(data)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        checkEpisodeNumber= async(data) =>{
            return new Promise((resolve,reject)=>{
                courseVideoModel.findOne({
                    courseId : data.courseId,
                    sectionId :data.sectionId,
                    episodeNumber : data.episodeNumber
                }).then(data =>{
                    resolve(data)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        checkEpisodeNumberUpdate = async(data) =>{
       
            return new Promise((resolve,reject)=>{
                const query = courseVideoModel.findOne({
                    episodeNumber:data.episodeNumber,
                    courseId:data.courseId,
                    sectionId:data.sectionId
                });
                query.where({_id:{$ne:data.id}})
                query.exec(function (err, data) {
                  if (err) {
                      reject(err)
                  }
                  else{
                      resolve(data)
                  }
                  
                });
            
           })
       
        }
        //dashboard count order
        orderCount= () =>{
            var result ={} 
            return new Promise((resolve,reject)=>{
                orderModel.
                find({}).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                  console.log("here",data)
                   resolve(data)
                      
                  }
                
                });
            
           })

        }
        listCourseVideos= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', 'desc']])
                .populate('courseId',['courseName','_id']).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
     
                    courseVideoModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            console.log( "Number of courses:", count );
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }

        listCourseVideos1= (searchControl,pageControl,instructorId) =>{
            return new Promise((resolve,reject) =>{
                //let objectIdArray = selectedCategory.map(s => Mongoose.Types.ObjectId(s));
                courseVideoModel.aggregate(
                    [
                     
                     { "$match": {"instructorId":instructorId}},
                
                        { 
                            "$lookup": 
                            {
                             "from": "bliss_courses",
                             "localField": "courseId",
                             "foreignField": "_id",
                             "as": "courseId",
                             },    
                        },
                        { 
                            "$lookup":
                            {
                            "from": "bliss_instructor_sections",
                            "localField": "sectionId",
                            "foreignField": "_id",
                            "as": "sectionId"
                             }
                        },
                        //{ "$match": { "course.isAdmin": true}},
                         { "$unwind": { "path" : "$courseId" } },
                         { "$unwind": { "path" : "$sectionId" } },
                        // {
                        //     $project: 
                        //     {
                        //      " _id" : 1,
                        //      "instructorId" :1,
                        //       "section._id" : 1,
                        //       "section.sectionHeading" : 1,
                        //        "videos.name" : 1,
                        //     //  "category._id":1,
                        //     //  "course.courseThumbnail":1,
                        //     //  "course.courseAuthor":1,
                        //     //  "course.authorImage":1,
                          
                        //     }
                        //   },
                          //{ $limit: 3 }      
                    ],
                    function(err,results) {
                        if (err){
                            reject(err)
                        }
                        else{
                        
                            resolve(results)
                        }
                        
                    }
                )
        
            })
           

        }

        listVideosNew= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', 'desc']])
                .populate('courseId',['courseName','_id'])
                .populate('sectionId')
                .exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
     
                    courseVideoModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            //console.log( "Number of courses:",result);
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }

    }
    
    module.exports = new video()