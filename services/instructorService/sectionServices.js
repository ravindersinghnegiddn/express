
const courseModel= require('../../database/schema/course_schema')
const courseVideoModel = require('../../database/schema/course_video_schema')
//const sectionModel = require('../../database/schema/instructor_section_schema')
const sectionModel = require('../../database/schema/section_schema')
const orderModel = require('../../database/schema/order_schema')
const { ObjectId } = require('mongodb');



class section {
    constructor(){}
    
        viewCourse= async(id) =>{
        
            return new Promise((resolve,reject)=>{
                courseModel.findOne({_id:ObjectId(id)}).then(data =>{
                    
                    resolve(data)
                }).catch(err =>{
                    
                    reject(err)
                })
            
           })
       
        }

        addSection = async(data) =>{
            let sectionData = new sectionModel(data)
            return new Promise((resolve,reject)=>{
                sectionData.save().then(data =>{
                    resolve(true)
                }).catch(err =>{
                    console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                    reject(err)
                })
            
           })
        }

        listSection= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                sectionModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', 'desc']])
                .populate('course',['courseName','_id']).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                    sectionModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            console.log( "Number of courses:", count );
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }

        viewSection = async(id) =>{
        
            return new Promise((resolve,reject)=>{
                sectionModel.findOne({_id:ObjectId(id)})
                .then(data =>{
                    resolve(data)
                }).catch(err =>{ 
                    reject(err)
                })
            
           })
       
        }
        updateSection = async(data) =>{
        
            return new Promise((resolve,reject)=>{
                sectionModel.updateOne(
                    {_id:ObjectId(data.id)},
                    {course:ObjectId(data.course),
                    sectionHeading:data.sectionHeading,
                    sectionNumber:data.sectionNumber,
                    updatedAt:Date.now()})
                    .then(data =>{
                    resolve(true)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        //dashboard count video
        listVideos1= (searchControl,pageControl) =>{
            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find({}).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                  console.log("here",data)
                   resolve(data)
                      
                  }
                
                });
            
           })

        }

        deleteVideo= async(id) =>{ 
            return new Promise((resolve,reject)=>{
                courseVideoModel.deleteOne({_id:ObjectId(id)})
                .then(data =>{
                    resolve(data)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        checkSectionNumber= async(data) =>{
            return new Promise((resolve,reject)=>{
                sectionModel.findOne({
                    course : data.course,
                    sectionNumber :data.sectionNumber,
                    instructorId :data.instructorId
                }).then(data =>{
                    resolve(data)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
        checkSectionNumberUpdate = async(data) =>{
       
            return new Promise((resolve,reject)=>{
                const query = sectionModel.findOne({
                    course:data.course,
                    sectionNumber:data.sectionNumber
                });
                query.where({_id:{$ne:data.id}})
                query.exec(function (err, data) {
                  if (err) {
                      reject(err)
                  }
                  else{
                      resolve(data)
                  }
                  
                });
            
           })
       
        }
        //dashboard count order
        orderCount= () =>{
            var result ={} 
            return new Promise((resolve,reject)=>{
                orderModel.
                find({}).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                  console.log("here",data)
                   resolve(data)
                      
                  }
                
                });
            
           })

        }
        listCourseVideos= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                courseVideoModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', 'desc']])
                .populate('courseId',['courseName','_id']).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
     
                    courseVideoModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            console.log( "Number of courses:", count );
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }
        //dropdown
        listSectionDropdown= (courseId) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                sectionModel.
                find({course:courseId})
                .sort([['createdAt', 'desc']])
                .exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
                      console.log(data)
                   resolve(JSON.parse(JSON.stringify(data)))
                      
                  }
                
                });
            
           })

        }

    }
    
    module.exports = new section()