const instructorModel = require('../../database/schema/instructor_schema')
const { ObjectId } = require('mongodb');


class instructor {
    constructor(){}
   

    instructorSignup = async(data) => {
        console.log(data)
        let instructorData = new instructorModel({
            email:data.email,
            name:data.name,
            password:data.hashPassword,
            selectedCategory:data.categoryArray,
            last_name:data.last_name,
            why_to_help:data.why_to_help,
            status:data.status,
            isBlocked:data.isBlocked
        })
        return new Promise((resolve,reject)=>{
            instructorData.save().then(data =>{
                 resolve(data)
             }).catch(err =>{
                 reject(err)
             })
       })
        }

    checkEmail = async(email) => {
        return new Promise((resolve,reject)=>{
            instructorModel.findOne({ 'email': email}, function (err, person) {
                if (err) reject(err);
               else{
                resolve(person)
               }
              }); 
       })
    }

    instructorLogin = async(data) => {
        return new Promise((resolve,reject)=>{
            instructorModel.findOne({ 'email': data.email,'password':data.hashPassword }, function (err, person) {
                if (err) reject(err);
               else{
                resolve(JSON.parse(JSON.stringify(person)))
               }
              });   
       })
    }

    instructorProfile = async(id) => {
        //console.log(">>>>>>>>>id",id) 
        return new Promise((resolve,reject)=>{
            instructorModel.findOne({_id:id},['name', 'last_name',
                'profileImg', 
                'tagline',
                'city',
                'country',
                'experience',
                'shortDescription',
                'phone_number',
                'industry',
                'aboutMe',
                'website',
                'facebook',
                'twitter',
                'linkedin',
                'language',
                'workedAt',
                'studiedAt',
                'awards',
                'selectedCategory',
                'dob'], function (err, person) {
                if (err) reject(err);
               else{
                resolve(JSON.parse(JSON.stringify(person)))
               }
              }); 
        
       })
    }
    updateInstructor = async(id,data) =>{
        
        return new Promise((resolve,reject)=>
        {
            instructorModel.updateOne({_id:ObjectId(id)},data)
                                  .then(data =>{
                                        //console.log(data)
                                        resolve(true)
                                    }).catch(err =>{
                                        reject(err)
                                })
        
       })
    
    }
    getInstructorCategoryArray = async(id) =>{
        return new Promise((resolve,reject)=>{
            instructorModel.find({ '_id':id})
            .select('selectedCategory')
            .then(data =>{
                //console.log("selected Category",data)
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })

 
    
       }
    

   

   

    



    

    
  
   

   
   
   
   



    

    }
    
    module.exports = new instructor()