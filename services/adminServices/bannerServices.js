const courseModel = require('../../database/schema/course_schema')
const courseCategoryModel = require('../../database/schema/courseCategory_schema')
const courseVideoModel = require('../../database/schema/course_video_schema')
const bannerModel = require('../../database/schema/banner_schema')
const { ObjectId } = require('mongodb');



class banner {
    constructor(){}
    
     
    addBanner = async(data) =>{
      
        let bannerData = new bannerModel(data)
        return new Promise((resolve,reject)=>{
            bannerData.save().then(data =>{
                console.log(data)
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkCourseName = async(name) =>{
        console.log("hrerere")
        return new Promise((resolve,reject)=>{
            courseModel.findOne({alias:name}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkCourseNameUpdate = async(name,id) =>{
       
        return new Promise((resolve,reject)=>{
            const query = courseModel.findOne({alias:name});
            query.select('alias name');
            query.where({_id:{$ne:id}})
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateCourse = async(data) =>{
        //let courseData = {id,categoryArray,courseName,courseDescription,courseTagLine,courseGoal,imageUrl,coursePrice,courseAuthor,aboutAuthor,slugedName,duration,videoData,authorImageUrl,folderId1,videoId1} 
        return new Promise((resolve,reject)=>
        {
            courseModel.updateOne({_id:ObjectId(data.id)},
                                 { categories:data.categoryArray,
                                    courseName:data.courseName,
                                    alias:data.slugedName,
                                    courseDescription:data.courseDescription,
                                    courseTagLine :data.courseTagLine,
                                    courseGoal: data.courseGoal,
                                    courseThumbnail:data.imageUrl,
                                    coursePrice:data.coursePrice,
                                    courseAuthor:data.courseAuthor,
                                    aboutAuthor:data.aboutAuthor,
                                    duration:data.duration,
                                    durationType:data.durationType,
                                    videoLink:data.videoData,
                                    authorImage:data.authorImageUrl,
                                    folderId:data.folderId1,
                                    videoId:data.videoId1,  
                                    updatedAt:(new Date()).getTime()})
                                  .then(data =>{
                                        resolve(true)
                                    }).catch(err =>{
                                        reject(err)
                                })
        
       })
   
    }

    listCourse = async() =>{   
        return new Promise((resolve,reject)=>{
            courseModel.
            find({isAdmin:true})
            .skip()
            .limit()
            .sort([['createdAt', 'desc']])
            .populate('categories','name').
            exec(function (err, data) {
              if(err){
                  reject(err)
              }
              else{
                  resolve(data)
              }
            
            });
        
       })
   
    }

    listBanner = async(searchControl,pageControl) =>{  
        var result ={} 
        return new Promise((resolve,reject)=>{
            bannerModel.
            find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['creation_date', 'desc']])
            .exec(function (err, data) {
              if(err){
                  reject(err)
              }
              else{
 
                bannerModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of banner:", count );
                        resolve(result)
                    }
                    
                })
                  
              }
            
            });
        
       })
   
    }
    
    viewBanner= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            bannerModel.findOne({_id:ObjectId(id)})
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    deleteBanner= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            bannerModel.deleteOne({_id:ObjectId(id)})
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    deleteCourse1= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            courseModel.deleteOne({_id:ObjectId(id)})
            .then(data =>{
                courseCategoryModel.deleteMany({courseId:ObjectId(id)}).then(data =>{
                  console.log("courseCategoryDelete>>",data)
                }).catch(err =>{
                console.log("error")
                })
                courseVideoModel.deleteMany({courseId:ObjectId(id)}).then(data =>{
                    console.log("courseVideoDelete>>",data)
                  }).catch(err =>{
                  console.log("error")
                  })
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    
    }
    
    module.exports = new banner()