const adminModel = require('../../database/schema/admin_schema')
const categoryModel = require('../../database/schema/categories_shema')
const newsModel = require('../../database/schema/news_schema')
const { ObjectId } = require('mongodb');



class news {
    constructor(){}
    
     
    addNews = async(data) =>{
       
        let newsData = new newsModel({ 
            newsHeadline : data.newsHeadline,
            newsDetail :data.newsDetail,
            newsThumbnail: data.imageUrl,
            newsUrl :data.newsUrl
          
        })
        return new Promise((resolve,reject)=>{
            newsData.save().then(data =>{
                resolve(true)
            }).catch(err =>{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                reject(err)
            })
        
       })
   
    }

    checkNewsName = async(newsHeadline) =>{
       
        return new Promise((resolve,reject)=>{
            newsModel.findOne({newsHeadline:newsHeadline}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkNewsNameUpdate = async(headline,id) =>{
       
        return new Promise((resolve,reject)=>{
            const query = categoryModel.findOne({newsHeadline:headline});

            
            query.select('newsHeadline');
            query.where({_id:{$ne:id}})
            //query.where({_id:id})
            
            // execute the query at a later time
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateNews = async(id,name,slugedName,title,icon,localStorageUrl) =>{
        
        console.log(">>>>>>>>>>>>>",id,name,slugedName,title,icon,localStorageUrl)
        return new Promise((resolve,reject)=>{
            categoryModel.updateOne({_id:ObjectId(id)},{name:name,alias:slugedName,title:title,icon:icon,localUrl:localStorageUrl,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
 
    listNews = async(searchControl,pageControl) =>{
        var result ={}
        return new Promise((resolve,reject)=>{
            newsModel.find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['createdAt', 'desc']])
            .then(data =>{

                newsModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of news:", count );
                        resolve(result)
                    }
                    
                })

                
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    
    viewNews = async(id) =>{
        
        return new Promise((resolve,reject)=>{
            newsModel.findOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }

    deleteNews= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            newsModel.deleteOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(data)
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }


    }
    
    module.exports = new news()