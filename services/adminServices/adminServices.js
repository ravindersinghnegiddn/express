const adminModel = require('../../database/schema/admin_schema')
const userModel = require('../../database/schema/user_schema')
const categoryModel = require('../../database/schema/categories_shema')
const faqModel = require('../../database/schema/faq_schema')
const subscriptionModel = require('../../database/schema/subscription_payment_schema')

const cmsModel = require('../../database/schema/cms_schema')
const { ObjectId } = require('mongodb');

class admin {
    constructor(){}
    adminSignupServices = async(data) => {
     console.log(data)
     let adminData = new adminModel({
         email:data.email,
         name:data.name,
         phone:data.phone,
         password:data.hashPassword
     })
     return new Promise((resolve,reject)=>{
          adminData.save().then(data =>{
              resolve(data)
          }).catch(err =>{
              reject(err)
          })
    })
     }

    adminLoginServices = async(data) => {
        return new Promise((resolve,reject)=>{
            adminModel.findOne({
                $and:
                [
                   {"admin_email":data.userEmail},
                   {"admin_password":data.hashPassword},
                   {$or: [{"admin_type": "Super Admin"}, {"admin_type": "Sub Admin"}]},
                ]
               }, function (err, person) {
                if (err) reject(err);
               else{
                resolve(person)
               }
              });   
       })
    }

    checkEmail = async(email) => {
        return new Promise((resolve,reject)=>{
            adminModel.findOne({ 'email': email}, function (err, person) {
                if (err) reject(err);
               else{
                resolve(person)
               }
              }); 
       })
    }
     
    addCategory = async(name) =>{
        let categoryData = new categoryModel({ 
            name:name
        })
        return new Promise((resolve,reject)=>{
            categoryData.save().then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })    
       })
        
    }

    dashboardUserCount = async() =>{  
         
        return new Promise((resolve,reject)=>{
            userModel.find({})
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                console.log(err)
                reject(err)
            })
        
       })
   
    }

    listUsers = async(data,data1) =>{ 
       console.log("paginationvalue",data)
        var result ={}
        console.log(data1)
        return new Promise((resolve,reject)=>{
            userModel.find(data1)
            .skip(data.skip1)
            .limit(data.requested_count1)
            .sort([['createdAt',-1]])
            .then(data =>{
                userModel.countDocuments(data1, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of users:", count );
                        resolve(result)
                    }
                    
                })
                
            }).catch(err =>{
                console.log(err)
                reject(err)
            })
        
       })
   
    }

    viewUser= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            userModel.findOne({_id:ObjectId(id)})
            .populate('selectedQuestion','question')
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    addFaq= async(data) =>{ 
        return new Promise((resolve,reject)=>{
           var faqData = new faqModel({
               question:data.question,
               answer:data.answer
           })
           faqData.save()
           .then(data =>{
               resolve(JSON.parse(JSON.stringify(data)))
           })
           .catch(err =>{
               reject(err)
           })
        
       })
   
    }

    listFaq = async(data,data1) =>{ 
         var result ={}
         console.log(data1)
         return new Promise((resolve,reject)=>{
            faqModel.find({})
             .skip(data.skip1)
             .limit(data.requested_count1)
             .sort([['createdAt', 'desc']])
             .then(data =>{
                 faqModel.countDocuments(data1, function( err, count){
                     if(err){
                         reject(err)
                     }
                     else{
                         result.data=data
                         result.total_count = count
                         console.log( "Number of q&a:", count );
                         resolve(result)
                     }
                     
                 })
                 
             }).catch(err =>{
                 console.log(err)
                 reject(err)
             })
         
        })
    
     }

    viewFaq= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            faqModel.findOne({_id:ObjectId(id)})
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    updateFaq= async(data) =>{
        return new Promise((resolve,reject)=>{
            faqModel.updateOne({_id:data.id},
                {
                 question:data.question,
                 answer:data.answer,
                 updatedAt:Date.now()})
                 .then(data =>{
                       console.log(data)
                       resolve(true)
                   }).catch(err =>{
                       reject(err)
               })
       })
   
    }
  
    blockUnblockUser = async(data) =>{  
        return new Promise((resolve,reject)=>{
            userModel.updateOne({_id:ObjectId(data.id)},{isBlocked:data.status,updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    cms= async(data) =>{ 
        return new Promise((resolve,reject)=>{
           var aboutUsdata = new cmsModel({
               data:data,
               key :'privacyPolicy'
           })
           aboutUsdata.save()
           .then(data =>{
               resolve(JSON.parse(JSON.stringify(data)))
           })
           .catch(err =>{
               reject(err)
           })
        
       })
   
    }
    getCms= async(key) =>{

        return new Promise((resolve,reject)=>{
            cmsModel.findOne({key:key})
            .then(data =>{
                console.log(data)
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    updateCms= async(data) =>{

        return new Promise((resolve,reject)=>{
            cmsModel.updateOne({key:data.key},
                {data:data.data,     
                 updatedAt:Date.now()})
                 .then(data =>{
                       resolve(true)
                   }).catch(err =>{
                       reject(err)
               })
        
       })
   
    }
    listUsersCsv = async(data,data1) =>{ 
        
         var result ={}
         console.log(data1)
         return new Promise((resolve,reject)=>{
             userModel.find({})
             .skip()
             .limit()
             .sort([['createdAt', 'desc']])
             .then(data =>{
                 resolve(JSON.parse(JSON.stringify(data)))
                 
             }).catch(err =>{
                 console.log(err)
                 reject(err)
             })
         
        })
    
     }


     listAndroidDeviceToken = async() =>{ 
         var result ={}
       
         return new Promise((resolve,reject)=>{
             userModel.find({deviceType:'android',isBlocked:0},'deviceToken')
             .skip()
             .limit()
             .sort([['createdAt', 'desc']])
             .then(data =>{
                 resolve(data)
                 
             }).catch(err =>{
                 console.log(err)
                 reject(err)
             })
         
        })
    
     }

     listUserToken = async() =>{ 
        var result ={}
      
        return new Promise((resolve,reject)=>{
            subscriptionModel.find({},'updatedAt')
            .populate('userId','deviceToken')
            .skip()
            .limit()
            .sort([['createdAt', 'desc']])
            .then(data =>{
                resolve(data)
                
            }).catch(err =>{
                console.log(err)
                reject(err)
            })
        
       })
   
    } 

    }
    
    module.exports = new admin()