


const instructorModel = require('../../database/schema/instructor_schema')

const { ObjectId } = require('mongodb');



class instructor {
    constructor(){}
    

        addInstructor = async(data) =>{
            let instructorData = new instructorModel(data)
            return new Promise((resolve,reject)=>{
                instructorData.save().then(data =>{
                    resolve(true)
                }).catch(err =>{
                    console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                    reject(err)
                })
            
           })
        }
 
        checkInstructorEmail= async(data) =>{
            return new Promise((resolve,reject)=>{
                instructorModel.findOne({
                   email:data.email
                }).then(data =>{
                    resolve(JSON.parse(JSON.stringify(data)))
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }

        listInstructor= (searchControl,pageControl) =>{

            var result ={} 
            return new Promise((resolve,reject)=>{
                instructorModel.
                find(searchControl)
                .skip(pageControl.skip1)
                .limit(pageControl.requested_count1)
                .sort([['createdAt', -1]])
                .populate('courseId',['courseName','_id']).
                exec(function (err, data) {
                  if(err){
                      reject(err)
                  }
                  else{
     
                    instructorModel.countDocuments(searchControl, function( err, count){
                        if(err){
                            reject(err)
                        }
                        else{
                            result.data=data
                            result.total_count = count
                            console.log( "Number of courses:", count );
                            resolve(JSON.parse(JSON.stringify(result)))
                        }
                        
                    })
                      
                  }
                
                });
            
           })

        }
        viewInstructor= async(id) =>{ 
            return new Promise((resolve,reject)=>{
                instructorModel.findOne({_id:ObjectId(id)})
                .then(data =>{
                    resolve(data)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }

        blockUnblockInstructor = async(data) =>{  
            return new Promise((resolve,reject)=>{
                instructorModel.updateOne({_id:ObjectId(data.id)},{isBlocked:data.status,updatedAt:Date.now()}).then(data =>{
                    resolve(true)
                }).catch(err =>{
                    reject(err)
                })
            
           })
       
        }
       
    
    }
    
    module.exports = new instructor()