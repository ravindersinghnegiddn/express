const adminModel = require('../../database/schema/admin_schema')
const questionModel = require('../../database/schema/question_schema')
const { ObjectId } = require('mongodb');



class question {
    constructor(){}
    
     
    addQuestion = async(data) =>{
        let questionData = new questionModel({ 
            category:data.category_id,
            question:data.question,
            createdAt:(new Date()).getTime(),
            updatedAt:(new Date()).getTime()
           // subQuestion:data.subQuestion
        })
        return new Promise((resolve,reject)=>{
            questionData.save().then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkCategoryName = async(name) =>{
       
        return new Promise((resolve,reject)=>{
            categoryModel.findOne({alias:name}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkCategoryNameUpdate = async(name,id) =>{
       
        return new Promise((resolve,reject)=>{
            const query = categoryModel.findOne({alias:name});
            query.select('alias name');
            query.where({_id:{$ne:id}})
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateQuestion = async(data) =>{
        
        return new Promise((resolve,reject)=>{
            questionModel.updateOne(
                {_id:ObjectId(data.id)},
                {category:ObjectId(data.category_id),
                question:data.question,
                //subQuestion:data.subQuestion,
                updatedAt:(new Date()).getTime()})
                .then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    listQuestion = async() =>{   
        return new Promise((resolve,reject)=>{
            questionModel.find({})
            .skip()
            .limit()
            .sort([['creation_date', 'desc']])
            .populate('category','name').then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }


    listQuestion1 = async(searchControl,pageControl) =>{ 
        var result ={}  
        return new Promise((resolve,reject)=>{
            questionModel.find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['createdAt', 'desc']])
            .populate('category','name').then(data =>{

                questionModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of question:", count );
                        resolve(result)
                    }
                    
                })
                
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    
    viewQuestion= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            questionModel.findOne({_id:ObjectId(id)})
            .populate('category','name')
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
    deleteQuestion= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            questionModel.deleteOne({_id:ObjectId(id)})
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }



    }
    
    module.exports = new question()