const adminModel = require('../../database/schema/admin_schema')
const categoryModel = require('../../database/schema/categories_shema')
const subscriptionModel = require('../../database/schema/subscription_schema')
const { ObjectId } = require('mongodb');



class subscription {
    constructor(){}
    
     
    addSubscription= async(data) =>{
        let subscriptionData = new subscriptionModel({ 
            subscriptionType:data.subscriptionType,
            subscriptionDetail: data.subscriptionDetail,
            subscriptionPrice: data.subscriptionPrice
        })
        return new Promise((resolve,reject)=>{
            subscriptionData.save().then(data =>{
                resolve(true)
            }).catch(err =>{
                console.log(">>>>>>>>>>>>>>>>>>>>>>>",err)
                reject(err)
            })
        
       })
   
    }

    checkSubscriptionName = async(name) =>{
       
        return new Promise((resolve,reject)=>{
            subscriptionModel.findOne({subscriptionType:name}).then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    checkSubscriptionNameUpdate = async(name,id) =>{
       
        return new Promise((resolve,reject)=>{
            const query = subscriptionModel.findOne({subscriptionType:name});

            // selecting the `name` and `occupation` fields
            query.select('subscriptionType');
            query.where({_id:{$ne:id}})
            //query.where({_id:id})
            
            // execute the query at a later time
            query.exec(function (err, data) {
              if (err) {
                  reject(err)
              }
              else{
                  resolve(data)
              }
              
            });
        
       })
   
    }

    updateSubscription = async(data) =>{
        return new Promise((resolve,reject)=>{
            subscriptionModel.updateOne({_id:ObjectId(data.id)},
             {   
                subscriptionType:data.subscriptionType,
                subscriptionDetail: data.subscriptionDetail,
                subscriptionPrice: data.subscriptionPrice,
                updatedAt:Date.now()}).then(data =>{
                resolve(true)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    // listCategory = async() =>{
        
    //     return new Promise((resolve,reject)=>{
    //         categoryModel.find({})
    //         .skip()
    //         .limit()
    //         .sort([['createdAt', 'desc']])
    //         .then(data =>{
    //             resolve(JSON.parse(JSON.stringify(data)))
    //         }).catch(err =>{
    //             reject(err)
    //         })
        
    //    })
   
    // }
    listSubscription= async(searchControl,pageControl) =>{
        var result ={}
        return new Promise((resolve,reject)=>{
            subscriptionModel.find(searchControl)
            .skip(pageControl.skip1)
            .limit(pageControl.requested_count1)
            .sort([['createdAt', 'desc']])
            .then(data =>{
                subscriptionModel.countDocuments(searchControl, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        result.data=data
                        result.total_count = count
                        console.log( "Number of subscription:", count );
                        resolve(result)
                    }                
                })     
            }).catch(err =>{
                reject(err)
            })  
       })
    }
    
    viewSubscription= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            subscriptionModel.findOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }
    deleteSubscription= async(id) =>{
        
        return new Promise((resolve,reject)=>{
            subscriptionModel.deleteOne({_id:ObjectId(id)}).then(data =>{
                
                resolve(data)
            }).catch(err =>{
                
                reject(err)
            })
        
       })
   
    }


    }
    
    module.exports = new subscription()