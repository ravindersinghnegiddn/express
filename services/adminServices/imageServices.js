const { promiseImpl } = require('ejs');
const fs = require('fs');
const { resolve } = require('path');
var cloudinary = require('cloudinary').v2;

cloudinary.config({  
    cloud_name:'dvnywgio4',  
    api_key:'476256983675831',  
    api_secret:'BVV7TO4ZPe4cx59-Y4WWjK9Ypx4'  
});









class image{
 constructor(){}
 uploadImage = async(baseImage) =>{

     /*path of the folder where your project is saved. (In my case i got it from config file, root path of project).*/
     const uploadPath = "/Users/algosofttechnologies/Desktop/blissGenX/bliss_API";
     //path of folder where you want to save the image.
     const localPath = `${uploadPath}/uploads/categoryImages/`;
     //Find extension of file
     const ext = baseImage.substring(baseImage.indexOf("/")+1, baseImage.indexOf(";base64"));
     const fileType = baseImage.substring("data:".length,baseImage.indexOf("/"));
     //Forming regex to extract base64 data of file.
     const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
     //Extract base64 data.
     const base64Data = baseImage.replace(regex, "");
     const rand = Math.ceil(Math.random()*1000);
     //Random photo name with timeStamp so it will not overide previous images.
     const filename = `Photo_${Date.now()}_${rand}.${ext}`;
     
     //Check that if directory is present or not.
     if(!fs.existsSync(`${uploadPath}/uploads/`)) {
         fs.mkdirSync(`${uploadPath}/uploads/`);
     }
     if (!fs.existsSync(localPath)) {
         fs.mkdirSync(localPath);
     }
     fs.writeFileSync(localPath+filename, base64Data, 'base64');
    
     var imageUrl = "http://localhost:3000/categoryImages/"+filename
     var localStorage = localPath+filename
     return [imageUrl,localStorage];
 
 }
 uploadUpdatedImage = async(localUrl,baseImage) =>{
   

    return new Promise((resolve,rejects)=>{
    
    
    fs.unlink(localUrl, (err => { 
        if (err) {
            console.log("unlink error",err)
           
        }
        else { 
            console.log("else block------------------")
            /*path of the folder where your project is saved. (In my case i got it from config file, root path of project).*/
            const uploadPath = "/Users/algosofttechnologies/Desktop/blissGenX/bliss_API";
            //path of folder where you want to save the image.
            const localPath = `${uploadPath}/uploads/categoryImages/`;
            //Find extension of file
            const ext = baseImage.substring(baseImage.indexOf("/")+1, baseImage.indexOf(";base64"));
            const fileType = baseImage.substring("data:".length,baseImage.indexOf("/"));
            //Forming regex to extract base64 data of file.
            const regex = new RegExp(`^data:${fileType}\/${ext};base64,`, 'gi');
            //Extract base64 data.
            const base64Data = baseImage.replace(regex, "");
            const rand = Math.ceil(Math.random()*1000);
            //Random photo name with timeStamp so it will not overide previous images.
            const filename = `Photo_${Date.now()}_${rand}.${ext}`;
            
            //Check that if directory is present or not.
            if(!fs.existsSync(`${uploadPath}/uploads/`)) {
                fs.mkdirSync(`${uploadPath}/uploads/`);
            }
            if (!fs.existsSync(localPath)) {
                fs.mkdirSync(localPath);
            }
            fs.writeFileSync(localPath+filename, base64Data, 'base64');
        
            var imageUrl = "http://localhost:3000/categoryImages/"+filename
            var localStorage = localPath+filename
            
             resolve([imageUrl,localStorage])

        } 
      }));
    })

    
   

}

uploadCloudinary = async(image) => {
 
  return new Promise((resolve,rejects) =>{
     cloudinary.uploader.upload(image,{ 
         folder: "category", 
         },function(err,result){
         if(err){
            console.log("error",err)
             rejects(err)
         }
         else{
           // console.log("success",result)
            resolve(result.url)
         }
               
     })
    })
}


//https://medium.com/@_BooToo_/using-cloudinary-with-angular-4-and-firebase-3db8314da3f1

}


module.exports = new image()