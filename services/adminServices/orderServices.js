const adminModel = require('../../database/schema/admin_schema')
const userModel = require('../../database/schema/user_schema')
const categoryModel = require('../../database/schema/categories_shema')
const faqModel = require('../../database/schema/faq_schema')
const orderModel = require('../../database/schema/order_schema')
const cmsModel = require('../../database/schema/cms_schema')
const { ObjectId } = require('mongodb');

class order {
    constructor(){}
    
    listOrder = async(data,data1) =>{ 
       
        var result ={}
        console.log(data1)
        return new Promise((resolve,reject)=>{
            orderModel.find(data)
            .populate('userId',['firstName','lastName'])
            .skip(data1.skip1)
            .limit(data1.requested_count1)
            .sort([['createdAt', 'desc']])
            .then(data =>{
                orderModel.countDocuments(data, function( err, count){
                    if(err){
                        reject(err)
                    }
                    else{
                        JSON.parse(JSON.stringify(data))
                        result.data=data
                        result.total_count = count
                        console.log( "Number of users:", count );
                        resolve(result)
                    }
                    
                })
                
            }).catch(err =>{
                console.log(err)
                reject(err)
            })
        
       })
   
    }

    viewOrder= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            orderModel.findOne({_id:ObjectId(id)})
            .populate('userId',['firstName','lastName'])
            .populate('courseId','courseName')
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

    viewOrderedCourse= async(id) =>{ 
        return new Promise((resolve,reject)=>{
            orderModel.findOne({courseId:ObjectId(id)})
            .then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }
   
    }
    
    module.exports = new order()