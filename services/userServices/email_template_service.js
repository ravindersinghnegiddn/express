const { ObjectId } = require('mongodb');
var nodemailer = require('nodemailer');
const commonService = require('../../services/userServices/commonService')
const requiredFiles = require('../../database/required');

class email_template_service{
    constructor(){}
    sendMailToUser = async(email,subject,html)=>{
            // create transporter object with smtp server details
        const transporter = nodemailer.createTransport({
                service:"webmail",
                host: "www.blissgenx.com",
                port: 465,
                secure: true,
                auth: {
                user: 'noreply@blissgenx.com',
                pass: 'vQUQ*nr*jhI6'
                }
        });
        
        // send email
        await transporter.sendMail({
                from: 'noreply@blissgenx.com',
                to: email,
                subject: subject,
                html: html
        });
    }

    /* * *********************************************************************
	 * * Function name : sendMailToUserForSignUp
	 * * Developed By : Tejaswi
	 * * Purpose  : This function use for get About Us Web
	 * * Date : 13 MAY 2021
	 * * **********************************************************************/

    sendMailToUserForSignUp = async(username,email) => {
        
        var getEmaildata = await commonService.getData(requiredFiles.emailTemplateModel,'single',{"mail_type_data":"SignUpSuccessMailToCustomer"})
        
        if(getEmaildata._id !== null && getEmaildata !== ''){
            var toEmail         =       email;
            var toName          =       username;
            var subject         =       getEmaildata.subject;
            var MHTML           =       '';

            var MHData          =       getEmaildata.mail_header;
            
            var MBData          =       getEmaildata.mail_body;
            var MBData          =       MBData.replace("{USER_NAME}",toName);
            
            var MFData          =       getEmaildata.mail_footer;
            
            var MHTML           =       getEmaildata.html;
            var MHTML           =       MHTML.replace("{mail-header}",MHData);
            var MHTML           =       MHTML.replace("{mail-body}",MBData);
            var MHTML           =       MHTML.replace("{mail-footer}",MFData);
            var MHTML           =       MHTML.replace(/\\/g, '')
            
            await this.sendMailToUser(toEmail,subject,MHTML)
        }
        
       }
}
module.exports = new email_template_service()