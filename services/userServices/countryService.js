const countryModel = require('../../database/schema/country_scheme')
class countryService{
    constructor(){}
    getCountryData = ()=>{
        return new Promise((resolve,reject)=>{
            countryModel.find()
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

}
module.exports = new countryService()