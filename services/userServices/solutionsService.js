const solutionModel = require('../../database/schema/solutions_scheme')
class solutionService{
    constructor(){}
    getSolutionData = ()=>{
        return new Promise((resolve,reject)=>{
            solutionModel.find()
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

}
module.exports = new solutionService()