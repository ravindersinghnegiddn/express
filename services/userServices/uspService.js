const uspModel = require('../../database/schema/usp_scheme')
class uspService{
    constructor(){}
    getUspData = ()=>{
        return new Promise((resolve,reject)=>{
            uspModel.find()
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

}
module.exports = new uspService()