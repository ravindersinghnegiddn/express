const userModel = require('../../database/schema/user_schema')
const userCategoryModel = require('../../database/schema/user_category_schema')
const userQuestionModel = require('../../database/schema/user_question_schema')
const categoryModel = require('../../database/schema/categories_shema')
const courseModel = require('../../database/schema/course_schema')
const questionModel = require('../../database/schema/question_schema')
const courseCategoryModel = require('../../database/schema/courseCategory_schema')
const orderModel = require('../../database/schema/payment_schema')
const cmsModel = require('../../database/schema/cms_schema')
const subscriptionModel = require('../../database/schema/subscription_payment_schema')
const adminSubscriptionModel = require('../../database/schema/subscription_schema')
const newsModel = require('../../database/schema/news_schema')
const instructorModel = require('../../database/schema/instructor_schema')
const eventModel = require('../../database/schema/event_schema')
const { ObjectId } = require('mongodb');
const Mongoose = require('mongoose')
class user {
    constructor(){}
    userSignupServices = async(data) => {
     console.log(data)
     let userData = new userModel({
         email:data.email,
         firstName:data.firstName,
         phone:data.phone,
         password:data.hashPassword,
         age:data.age,
         selectedCategory:data.categoryArray,
         selectedQuestion:data.questionArray
     })
     return new Promise((resolve,reject)=>{
          userData.save().then(data =>{
              resolve(data)
          }).catch(err =>{
              console.log("herere")
              reject(err)
          })
    })
    }

    userLoginServices = async(data) => {
        return new Promise((resolve,reject)=>{
            userModel.findOne({ 'email': data.email,'password':data.hashPassword },['firstName','lastName','email','is_membership_active'], function (err, person) {
                if (err) reject(err);
               else{
                resolve(person)
               }
              }); 
       })
    }

    checkEmail = async(email) => {
        return new Promise((resolve,reject)=>{
            userModel.findOne({ 'email': email}, function (err, person) {
                if (err) reject(err);
               else{
                resolve(person)
               }
              }); 
       })
    }

    userAddCategoryServices = async(data) => {
        console.log(data)
        let categoryData = new userCategoryModel({
            user:data.user,
            categories:data.categoryArray
        })
        return new Promise((resolve,reject)=>{
             categoryData.save().then(data =>{
                 resolve(data)
             }).catch(err =>{
                 reject(err)
             })
       })
    }

   userAddQuestionServices = async(data) => {
    console.log(data)
    let questionData = new userQuestionModel({
        user:data.user,
        questions:data.questionArray
    })
    return new Promise((resolve,reject)=>{
         questionData.save().then(data =>{
             resolve(data)
         }).catch(err =>{
             reject(err)
         })
   })
   }

   userGetCategoryListServices = async() =>{ 
   }

   userGetCategoryArray = async(id) =>{
    return new Promise((resolve,reject)=>{
        userModel.find({ '_id':id})
        .select('selectedCategory')
        .then(data =>{
            resolve(data)
        }).catch(err =>{
            reject(err)
        })
    
   })

   }

   getAboutUs = async(id) =>{
    return new Promise((resolve,reject)=>{
        cmsModel.findOne({'key':'aboutUs'})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

   }

   userProfileServices = async(id) => {
    console.log(">>>>>>>>>id",id) 
    return new Promise((resolve,reject)=>{
        userModel.findOne({_id:id},['firstName', 
            'lastName',
            'profileImg', 
            'title',
            'city',
            'country',
            'industry',
            'aboutMe',
            'website',
            'facebook',
            'twitter',
            'linkdIn',
            'pincode',
            'state',
            'address',
            'phone_number',
            'shortDescription',
            'dob'], function (err, person) {
            if (err) reject(err);
           else{
            resolve(person)
           }
          }); 
    
   })
}
   listCategory = async() =>{
    return new Promise((resolve,reject)=>{
        categoryModel.find({}).then(data =>{
            resolve(data)
        }).catch(err =>{
            reject(err)
        })
   })
}

   listQuestion = async() =>{   
    return new Promise((resolve,reject)=>{
        questionModel.find({})
        .skip()
        .limit()
        .sort([['createdAt', 'desc']])
        .populate('category','name').then(data =>{
            resolve(data)
        }).catch(err =>{
            reject(err)
        })
   })

}

listQuestion1 = async(categoryArray) =>{   
    return new Promise((resolve,reject)=>{
        questionModel.find({
            category :{$in:categoryArray}
        })
        .skip()
        .limit()
        .sort([['createdAt', 'desc']])
        .populate('category',['name','title']).then(data =>{
            resolve(data)
        }).catch(err =>{
            reject(err)
        })
   })

}
   
 selectedCategoryCourse = async(searchControl,pageControl,categoryArray) =>{
    var result ={}
    return new Promise((resolve,reject)=>{
        const query =   courseModel.find({
          categories: { $in: categoryArray }
        })
        query.select(['courseName','courseThumbnail','courseAuthor','coursePrice'])
        query.populate('categories','name')
        query.exec(function (err, data) {
            if (err) {
                reject(err)
            }
            else{
                resolve(data)
            }  
          });
   })

}


selectedCategoryCourse1 = async(searchControl,pageControl,categoryArray) =>{
    return new Promise((resolve,reject)=>{
        var result = []
        for(var i = 0 ;i<categoryArray.length ; i++){
            const query =   courseModel.find({
                categories: { $in: categoryArray[i] }
              })
              query.select(['courseName','courseThumbnail','courseAuthor','coursePrice'])
              query.populate('categories','name')
               query.limit(1)
              query.exec(function (err, data) {
                  if (err) {
                      reject(err)
                  }
                  else{
                   result.push(data[0])
                   console.log(result)
                  }     
                });  
            }
   })
}

selectedCategoryCourse2 = async() =>{
    return new Promise((resolve,reject) =>{
       var categoryArray = [ObjectId("6023d07f5f6712111a2f5461"),ObjectId("60223eab5773d4102a85ce43")]
        courseCategoryModel.aggregate(
            [
                { "$match": { "categoryId": {$in: categoryArray} }},
        
                { "$lookup": {
                     "from": "categories",
                     "localField": "categoryId",
                     "foreignField": "_id",
                     "as": "category",
                   
                },
                
            },
                { "$lookup": {
                     "from": "bliss_courses",
                     "localField": "courseId",
                     "foreignField": "_id",
                     "as": "course"
                }},
                  { "$unwind": { "path" : "$category" } },
                  { "$unwind": { "path" : "$course" } }
            ],
            function(err,results) {
                if (err){
                    reject(err)
                }
                else{
                    resolve(results)
                }
                
            }
        )

    })
   


}

selectedCategoryCourse3 = async(searchControl,pageControl,selectedCategory) =>{
    return new Promise((resolve,reject) =>{
        let objectIdArray = selectedCategory.map(s => Mongoose.Types.ObjectId(s));
        courseCategoryModel.aggregate(
            [
             
                { "$match": { "categoryId": {$in: objectIdArray} }},
        
                { 
                    "$lookup": 
                    {
                     "from": "categories",
                     "localField": "categoryId",
                     "foreignField": "_id",
                     "as": "category",
                     },    
                },
                { 
                    "$lookup":
                    {
                    "from": "bliss_courses",
                    "localField": "courseId",
                    "foreignField": "_id",
                    "as": "course"
                     }
                },
                { "$match": { "course.isAdmin": true}},
                { "$unwind": { "path" : "$course" } },
                { "$unwind": { "path" : "$category" } },
                {
                    $project: 
                    {
                     " _id" : 1,
                     "course._id" : 1,
                     "course.courseName" : 1,
                     "category.name" : 1,
                     "category._id":1,
                     "course.courseThumbnail":1,
                     "course.courseAuthor":1,
                     "course.authorImage":1,
                  
                    }
                  },
                  //{ $limit: 3 }      
            ],
            function(err,results) {
                if (err){
                    reject(err)
                }
                else{
                    
                    resolve(results)
                }
                
            }
        )

    })
   


}



allProgram = async() =>{
    var result ={}
    return new Promise((resolve,reject)=>{
        const query =   courseModel.find({isAdmin:true})
        query.select(['courseName','courseThumbnail','courseAuthor','coursePrice','authorName','authorImage','enrolledCount'])
        query.exec(function (err, data) {
            if (err) {
                reject(err)
            }
            else{
                resolve(data)
            }
            
          });
    
   })

}
listCourse = async(searchControl,pageControl) =>{  
    var result ={} 
    return new Promise((resolve,reject)=>{
        courseModel.
        find(searchControl)
        .skip()
        .limit(pageControl.requested_count1)
        .sort([['createdAt', 'desc']])
        .populate('categories','name')
        .populate('videoLink','videoUrl')
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{

            courseModel.countDocuments({}, function( err, count){
                if(err){
                    reject(err)
                }
                else{
                    result.data=data
                    result.total_count = count
                    console.log( "Number of courses:", count );
                    resolve(result)
                }
                
            })
              
          }
        
        });
    
   })

}

listPurchasedCourse = async(searchControl,pageControl) =>{  
    var result ={} 
    return new Promise((resolve,reject)=>{
        orderModel.
        find(searchControl)
        .select('status')
        .skip()
        .limit()
        .sort([['creation_date', 'desc']])
        .populate({ path: 'course_id' ,select: ['courseAuthor','courseName','courseThumbnail','videoId'] })
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{
            orderModel.countDocuments(searchControl, function( err, count){
                if(err){
                    reject(err)
                }
                else{
                    result.purchasedProgram=data
                    result.total_count = count
                   // console.log( "Number of courses:", count );
                    resolve(result)
                }
                
            })
              
          }
        
        });
    
   })

}
viewCourse= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        courseModel.findOne({_id:id})
        .populate('categories','name')
        .populate('videoLink','videoUrl')
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}
//view purchased course
viewPurchasedCourse= async(id,userId) =>{ 
    return new Promise((resolve,reject)=>{
        orderModel.findOne({course_id:id,
            status:'paid',
            user_id:userId
        })
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}


//update user
updateUser = async(id,data) =>{
        
    return new Promise((resolve,reject)=>
    {
        userModel.updateOne({_id:ObjectId(id)},data)
                              .then(data =>{
                                    //console.log(data)
                                    resolve(true)
                                }).catch(err =>{
                                    reject(err)
                            })
    
   })

}

userDetail= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        userModel.findOne({_id:id})
        .then(data =>{
            resolve(data)
        }).catch(err =>{
            reject(err)
        })
    
   })

}
// subscriptionDetail= async(id) =>{ 
//     return new Promise((resolve,reject)=>{
//         subscriptionModel.findOne({userId:id})
//         .then(data =>{
//             resolve(JSON.parse(JSON.stringify(data)))
//         }).catch(err =>{
//             reject(err)
//         })
//    })

// }


subscriptionDetail= async(id,updatedAt) =>{ 
    //console.log(">>>>>",id)
    return new Promise((resolve,reject)=>{
        orderModel.findOneAndUpdate({user_id:id,purchase_type:"membership",'status':'paid'},{updatedAt:updatedAt})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
   })

}

monthlySubscriptionDetail= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        adminSubscriptionModel.findOne({subscriptionType:'Monthly'})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

allSubscriptionDetail= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        adminSubscriptionModel.find({})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

//news
listNews = async(searchControl,pageControl) =>{  
    var result ={} 
    return new Promise((resolve,reject)=>{
        newsModel.
        find({})
        .skip()
        .limit()
        .sort([['createdAt', 'desc']])
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{

            newsModel.countDocuments({}, function( err, count){
                if(err){
                    reject(err)
                }
                else{
                    result.data=data
                    result.total_count = count
                    console.log( "Number of news:", count );
                    resolve(result)
                }
                
            })
              
          }
        
        });
    
   })

}

viewNews= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        newsModel.findOne({_id:id})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}
viewNewsRelated= async(id) =>{ 
    return new Promise((resolve,reject)=>{
        newsModel.find({_id:{'$ne':{_id:id}}}).limit(5)
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

listInstructor = async(data,data1,instructorId) =>{ 
     var result ={}
     if(instructorId == undefined || instructorId == ''){}
     else{
        var WhereCon = {_id:ObjectId(instructorId)}
     }
     return new Promise((resolve,reject)=>{
        instructorModel.find(WhereCon)
         .skip()
         .limit()
         .populate('selectedCategory','name')
         .sort([['createdAt', 'desc']])
         .then(data =>{ 
            instructorModel.countDocuments({WhereCon}, function( err, count){
                 if(err){
                     reject(err)
                 }
                 else{
                     result.instructorData=data
                     result.total_count = count
                     console.log( "Number of instructor:", count );
                     resolve(JSON.parse(JSON.stringify(result)))
                 }
                 
             })
             
         }).catch(err =>{
             console.log(err)
             reject(err)
         })
     
    })

}

listInstructorCourse = async(searchControl,pageControl,instructorId) =>{  
    var result ={} 
    console.log(instructorId)
    return new Promise((resolve,reject)=>{
        courseModel.
        find({isAdmin:false,instructorId:ObjectId(instructorId)})
        .skip()
        .limit()
        .sort([['createdAt', 'desc']])
        .populate('categories','name')
        .populate('videoLink','videoUrl')
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{

            courseModel.countDocuments({isAdmin:false,instructorId:ObjectId(instructorId)}, function( err, count){
                if(err){
                    reject(err)
                }
                else{
                    result.instructorProgram=data
                    result.total_count = count
                    console.log( "Number of courses:", count );
                    resolve(result)
                }
                
            })
              
          }
        
        });
    
   })

}

listEvent = async(data,data1,instructorId) =>{ 
    console.log("paginationvalue",data)
     var result ={}
     if(instructorId == undefined || instructorId == ''){
        var WhereCon = {}
     }
     else{
        var WhereCon = {instructorId:instructorId}
     }
     
     return new Promise((resolve,reject)=>{
        eventModel.find(WhereCon,['name','instructorId','eventDateTime','eventDescription','icon','eventStatus','token'])
         .skip()
         .limit()
         .sort([['eventOrder', 'asc']])
         .then(data =>{
            eventModel.countDocuments({WhereCon}, function( err, count){
                 if(err){
                     reject(err)
                 }
                 else{
                     result.eventData=data
                     result.total_count = count
                     console.log( "Number of event:", count );
                     resolve(result)
                 }
                 
             })
             
         }).catch(err =>{
             console.log(err)
             reject(err)
         })
     
    })

}

viewEvent = async(data) =>{ 
    console.log("paginationvalue",data)
   
     
     return new Promise((resolve,reject)=>{
        eventModel.findOne({_id:ObjectId(data.id)},['name','eventDateTime','eventDescription','eventStatus','icon','token'])
         .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
         }).catch(err =>{
             console.log(err)
             reject(err)
         })
     
    })

}


listEventInstructor = async(data,data1,instructorId) =>{ 
    console.log("paginationvalue",data)
     var result ={}
     console.log(data1)
     return new Promise((resolve,reject)=>{
        eventModel.find({isAdmin:false,instructorId:instructorId},['name','eventDateTime','eventDescription','icon','token'])
         .skip()
         .limit()
         .sort([['createdAt', 'desc']])
         .then(data =>{
            eventModel.countDocuments({isAdmin:false,instructorId:instructorId}, function( err, count){
                 if(err){
                     reject(err)
                 }
                 else{
                     result.eventData=data
                     result.total_count = count
                     console.log( "Number of event:", count );
                     resolve(result)
                 }
                 
             })
             
         }).catch(err =>{
             console.log(err)
             reject(err)
         })
     
    })

}

viewEventInstructor = async(data) =>{ 
  
   
     
     return new Promise((resolve,reject)=>{
        eventModel.findOne({_id:ObjectId(data.id)},['name','eventDateTime','eventDescription','icon','token'])
         .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
         }).catch(err =>{
             console.log(err)
             reject(err)
         })
     
    })

}
   
}
    
    module.exports = new user()