const { ObjectId } = require('mongodb');
class commonService{
    constructor(){}
    getData = (tableName,action,whereCon,sort)=>{
        if(action == 'single'){
            var action = tableName.findOne(whereCon);
        }
        else{
            if(sort){
                var action = tableName.find().sort({'creation_date':-1}).limit(1);
            }else{
                var action = tableName.find(whereCon);
            }
            
        }
        
        return new Promise((resolve,reject)=>{
            action
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

    getDataByWhereCondition= (tableName,action,fieldName,fieldValue,whereCon,sort) =>{ 
        if(whereCon){
            var whereCon = whereCon;
        }
        else{
            var whereCon = {};
            whereCon[fieldName] = fieldValue;

        }
        
        if(action == 'single'){
            if(sort){
                var dataQuery = tableName.findOne(whereCon).sort(sort);
            }
            else{
                var dataQuery = tableName.findOne(whereCon).sort(sort)
            }
            
        }
        else{
            var dataQuery = tableName.find(whereCon);
        }
        return new Promise((resolve,reject)=>{
            dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    
    }

    insertData = async(query) => {
        
        return new Promise((resolve,reject)=>{
            query.save().then(data =>{
                 resolve(data)
             }).catch(err =>{
                 reject(err)
             })
       })
       }

    getDataByQuery = async(tableName,data) => {
        
        var action = tableName.find({"_id":{$in: data}});
        console.log(action);
        return new Promise((resolve,reject)=>{
            action
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

    getAllByQuery = async(action,data) => {
        
        console.log(action);
        return new Promise((resolve,reject)=>{
            action
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

    updateData= async(tableName,fieldName,fieldValue,params) =>
    {
       var whereCon = {};
       whereCon[fieldName] = fieldValue;
       var UpdateQuery = {$set: params}
       var dataQuery = tableName.update(whereCon,UpdateQuery);

       return new Promise((resolve,reject)=>
       {
        dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
          
       })
    }

    updateAllData= async(tableName,fieldName,fieldValue,params) =>
    {
       var whereCon = {};
       whereCon[fieldName] = fieldValue;
       var UpdateQuery = {$set: params}
       var dataQuery = tableName.updateMany(whereCon,UpdateQuery);

       return new Promise((resolve,reject)=>
       {
        dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
          
       })
    }

    deleteData= async(tableName,fieldName,fieldValue) =>
    {
       var whereCon = {};
       whereCon[fieldName] = fieldValue;
       var dataQuery = tableName.remove(whereCon);

       return new Promise((resolve,reject)=>
       {
        dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
          
       })
    }
    getDataByLike = (tableName,action,fieldName,fieldValue)=>{
        
        var whereCon = {};
        whereCon[fieldName] = {$regex: fieldValue};
        //console.log(whereCon);
        if(action == 'single'){
            var dataQuery = tableName.findOne(whereCon);
        }
        else{
            var dataQuery = tableName.find(whereCon);
        }
        return new Promise((resolve,reject)=>{
            dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

    getDataBySort = (tableName,action,fieldName,fieldValue)=>{
        
        var whereCon = {};
        whereCon[fieldName] = fieldValue;
        //console.log(whereCon);
        if(action == 'single'){
            var dataQuery = tableName.findOne().sort(whereCon);
        }
        else{
            var dataQuery = tableName.find().sort(whereCon);
        }
        return new Promise((resolve,reject)=>{
            dataQuery
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

    listJobs = async(model) =>{   
        return new Promise((resolve,reject)=>{
            model.find({})
            .skip()
            .limit()
            .sort([['creation_date', 'desc']])
            .populate('job_id','job_title').then(data =>{
                resolve(data)
            }).catch(err =>{
                reject(err)
            })
        
       })
   
    }

}
module.exports = new commonService()