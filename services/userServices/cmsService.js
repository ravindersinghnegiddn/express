const teamModel = require('../../database/schema/team_scheme')
class cmsService{
    constructor(){}
    getTeamData = ()=>{
        return new Promise((resolve,reject)=>{
            teamModel.find()
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
        
       })
    }

}
module.exports = new cmsService()