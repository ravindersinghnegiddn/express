const orderModel = require('../../database/schema/payment_schema')
const courseVideoModel = require('../../database/schema/course_video_schema')
const lastSeenModel = require('../../database/schema/watch_history_schema')
const { ObjectId } = require('mongodb');
const Mongoose = require('mongoose')
class video {
    constructor(){}

viewPurchasedCourse= async(id,userId) =>{ 
    return new Promise((resolve,reject)=>{
        orderModel.findOne({course_id:id,
            status:'paid',
            user_id:userId
           })
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

listPurchasedCourseVideo = async(searchControl,pageControl) =>{  
    var result ={} 
    return new Promise((resolve,reject)=>{
        courseVideoModel.
        find(searchControl)
        .select(['videoId','videoName','episodeNumber','videoDescription'])
        .populate('sectionId',['sectionHeading','_id','sectionNumber'])
        .skip()
        .limit()
        .sort([['episodeNumber', 'asc']])
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{
            
            courseVideoModel.countDocuments({}, function( err, count){
                if(err){
                    reject(err)
                }
                else{
                    var resultArray = []
                    data.forEach(element => {
                        var obj = {}
                        obj.videoName = element.videoName
                        obj.videoDescription= element.videoDescription
                        obj.episodeNumber= element.episodeNumber
                        obj.videoId= element.videoId
                        obj._id = element._id
                        obj.sectionHeading = element.sectionId.sectionHeading
                        obj.sectionNumber = element.sectionId.sectionNumber
                        resultArray.push(obj)
                    });
                    
                   // console.log(">>>>>result array",resultArray)
                    result.videoList=resultArray
                    result.total_count = count
                    //console.log( "Number of videos:", count );
                    resolve(JSON.parse(JSON.stringify(result)))
                }
                
            })
              
          }
        
        });
    
   })

}

viewVideoDetails= async(id,userId) =>{ 
    return new Promise((resolve,reject)=>{
        courseVideoModel.findOne({_id:id})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

viewVideoDetails1= async(videoId,userId) =>{ 
    return new Promise((resolve,reject)=>{
        lastSeenModel.findOne({videoId:videoId,userId:userId},'lastSeen')
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
    
   })

}

updateLastSeen = async(userId,videoId,data) =>{
        
    return new Promise(async (resolve,reject)=>
    {
        const filter = { userId: userId,videoId:videoId };
        const update = data;
        lastSeenModel.findOneAndUpdate(filter, update, {
                 new: true,
                 upsert: true // Make this update into an upsert
        }).then(data =>{
           resolve(data)
        }).catch(err =>{
           reject(err)
        })
    })

}

listPurchasedCourseVideo1 = async(searchControl,pageControl) =>{  
    var result ={} 
    return new Promise((resolve,reject) =>{
        let courseId = Mongoose.Types.ObjectId("603f3f886d8945113e608dbe")
       // let objectIdArray = selectedCategory.map(s => Mongoose.Types.ObjectId(s));
        //console.log(">>>>>>>",objectIdArray)
      // var categoryArray = ["6023d07f5f6712111a2f5461","60223eab5773d4102a85ce43"]
      courseVideoModel.aggregate(
            [
             
                { "$match": {courseId :courseId} },
        
                { 
                    "$lookup": 
                    {
                     "from": "bliss_watch_histories",
                     "localField": "_id",
                     "foreignField": "videoId",
                     "as": "video",
                     },    
                },
                
                // { 
                //     "$lookup":
                //     {
                //     "from": "bliss_courses",
                //     "localField": "courseId",
                //     "foreignField": "_id",
                //     "as": "course"
                //      }
                // },
                 // { "$unwind": { "path" : "$video" } },
                  { "$match": {'video.userId' :Mongoose.Types.ObjectId("601be05dae36dd11063322e5")} },
                // { "$unwind": { "path" : "$category" } },
                // {
                //     $project: 
                //     {
                //      " _id" : 1,
                //      "course._id" : 1,
                //      "course.courseName" : 1,
                //      "category.name" : 1,
                //      "category._id":1,
                //      "course.courseThumbnail":1,
                //      "course.courseAuthor":1,
                //      "course.authorImage":1,
                  
                //     }
                //   },

                  {
                    $project: 
                    {
                     " _id" : 1,
                     //"videoId":1,
                     "userId" : 1,
                     "courseId" :1,
                     "video._id" :1,
                     "video.userId":1,
                    }
                  },
                  //{ $limit: 3 }      
            ],
            function(err,results) {
                if (err){
                    reject(err)
                }
                else{
                     console.log(results)
                    //resolve(results)
                }
                
            }
        )

    })

}

listWatchedVideo = async(id) =>{  
    var result ={} 
    return new Promise((resolve,reject)=>{
        lastSeenModel.
        find({userId:id})
        .skip()
        .limit()
        .exec(function (err, data) {
          if(err){
              reject(err)
          }
          else{
           resolve(JSON.parse(JSON.stringify(data)))
              
          }
        
        });
    
   })

}
  
}
    
    module.exports = new video()