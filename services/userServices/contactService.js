const contactModel = require('../../database/schema/contact_us_scheme')
class contactUsData{
    constructor(){}
    
    putContactData = async(data) => {
        
        let putContactData = new contactModel({
            name    : data.name,
            email   : data.email,
            mobile  : data.mobil,
            country : data.country,
            message : data.message

        })
        return new Promise((resolve,reject)=>{
            putContactData.save().then(data =>{
                 resolve(JSON.parse(JSON.stringify(data)))
             }).catch(err =>{
                 reject(err)
             })
       })
    }

}
module.exports = new contactUsData()