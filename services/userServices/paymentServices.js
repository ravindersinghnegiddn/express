const userModel = require('../../database/schema/user_schema')
const orderModel = require('../../database/schema/order_schema')
const paymentModel = require('../../database/schema/payment_schema')
const subscriptionPaymentModel = require('../../database/schema/subscription_payment_schema')
const courseModel = require('../../database/schema/course_schema')
const { ObjectId } = require('mongodb');
const Mongoose = require('mongoose')
class payment {
    constructor(){}
  
   
    userCreateOrderServices = async(data) => {
       // console.log(data)
        let orderData = new orderModel({
            orderNo:data.order_no,
            userId:data.user_id,
            courseId : data.course_id,
            totalPrice :data.total_price
        })
        return new Promise((resolve,reject)=>{
            orderData.save().then(data =>{
                 resolve(JSON.parse(JSON.stringify(data)))
             }).catch(err =>{
                 reject(err)
             })
       })
    }

    initOrderPaymentService = async(data) => {
        //var data ={user_id,order_id,total_price,currency,status}
        console.log(data)
        let paymentData = new paymentModel({
            first_name:data.first_name,
            last_name:data.last_name,
            email:data.email,
            phone_number:data.phone_number,
            address:data.address,
            country:data.country,
            state:data.state,
            pincode:data.pincode,
            purchase_type:data.purchase_type,
            course_id:data.course_id,
            course_price:data.course_price,
            tax:data.tax,
            total_price:data.total_price,
            user_id:data.user_id,
            order_id:data.order_id,
            currency:data.currency,
            amount_in_choosed_currency:data.amount_in_choosed_currency,
            creation_date:Date.now()
        })
        return new Promise((resolve,reject)=>{
            paymentData.save().then(data =>{
                 resolve(JSON.parse(JSON.stringify(data)))
             }).catch(err =>{
                 reject(err)
             })
       })
    }

    paymentDetail= async(user_id,order_id) => {
        return new Promise((resolve,reject)=>{
            paymentModel.findOne({'order_id':order_id,'user_id':user_id})
            .populate('orderId','courseId')
            .then(data =>{
                resolve(JSON.parse(JSON.stringify(data)))
            }).catch(err =>{
                reject(err)
            })
       })
    }

    updatePaymentByid= async(data) =>
     {
         
        return new Promise((resolve,reject)=>
        {
            paymentModel.updateOne({order_id:data.order_id},
                                 {
                                    user_id:data.user_id,
                                    order_id:data.order_id,
                                    status:data.status,
                                    stripeToken:data.stripeToken,
                                    customerId:data.customerId,
                                    captureAmount:data.captureAmount,
                                    stripeChargeId:data.stripeChargeId,  
                                    update_date:Date.now()
                                  }).then(data =>{
                                        resolve(data)
                                    }).catch(err =>{
                                        reject(err)
                                    })
        })
    }

    updateOrderByid= async(data) =>
    {
        
       return new Promise((resolve,reject)=>
       {
           orderModel.updateOne({_id:ObjectId(data.orderId)},
                                {
                                 status :data.status,   
                                 updatedAt:Date.now()
                                 }).then(data =>{
                                       resolve(data)
                                   }).catch(err =>{
                                       reject(err)
                                   })
       })
    }

    initSubscriptionPaymentService = async(data) => {
    //var data ={user_id,order_id,total_price,currency,status}
    console.log(data)
    let subscriptionPaymentdata = new subscriptionPaymentModel({
        userId:data.user_id,
        status : data.status,
        totalPrice :data.total_price,
        orderNo :data.order_no
    })
    return new Promise((resolve,reject)=>{
        subscriptionPaymentdata.save().then(data =>{
             resolve(JSON.parse(JSON.stringify(data)))
         }).catch(err =>{
             reject(err)
         })
   })
    }
  
    subscriptionPaymentDetail= async(paymentId) => {
    return new Promise((resolve,reject)=>{
        subscriptionPaymentModel.findOne({'_id':paymentId})
        .then(data =>{
            resolve(JSON.parse(JSON.stringify(data)))
        }).catch(err =>{
            reject(err)
        })
   })
    }
    
    updateSubscriptionPaymentByid= async(data) =>
     {
         
        return new Promise((resolve,reject)=>
        {
            subscriptionPaymentModel.
            updateOne({_id:ObjectId(data.payment_id)},
                                 {stripePaymentId:data.stripePaymentId,
                                  status :data.status, 
                                  purchasedDate:data.purchasedDate, 
                                  expiryDate:data.expiryDate1,  
                                  updatedAt:Date.now()
                                  })
                                  .then(data =>{
                                        resolve(data)
                                   }).catch(err =>{
                                        reject(err)
                                   })
        })
    }

    updateCourseByid= async(data) =>
    {
        
       return new Promise((resolve,reject)=>
       {

        courseModel.findByIdAndUpdate({_id:ObjectId(data)}, {$inc: { enrolledCount: 1} }, function(error, counter)   {
            if(error){
                reject(error)
            }
            else{
                resolve(true)
            } 
           
        });
          
       })
    }


   
}
    
    module.exports = new payment()