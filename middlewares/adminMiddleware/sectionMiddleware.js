const sectionMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var courseVideoService = require('../../services/adminServices/videoServices')
var sectionServices = require('../../services/adminServices/sectionServices')




sectionMiddleware.checkSectionNumber= async(req, res, next) => {
	try{
        let {course,sectionHeading,sectionNumber} = req.body
		if(_.isEmpty(sectionNumber)){
			sendResponse.sendCustomError({data:"section Number required"},res,4,"please provide section Number")
		}
		else{
			let data = await sectionServices.checkSectionNumber({course,sectionNumber})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"section number already exist in this course")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

sectionMiddleware.checkSectionNumberUpdate= async(req, res, next) => {
	try{
        let {id,course,sectionHeading,sectionNumber} = req.body
        
		if(!sectionNumber){
			console.log("here")
			sendResponse.sendCustomError({data:"sectionNumber required"},res,4,"please provide sectionNumber")
		}
		else{
		
			let data = await sectionServices.checkSectionNumberUpdate({id,course,sectionNumber})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"section number already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = sectionMiddleware;