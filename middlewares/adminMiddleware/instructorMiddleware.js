const instructorMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var sectionServices = require('../../services/adminServices/sectionServices')
var instructorService  = require('../../services/adminServices/instructorService')




instructorMiddleware.checkInstructorEmail= async(req, res, next) => {
	try{
        let {email} = req.body
		if(_.isEmpty(email)){
			sendResponse.sendCustomError({data:"instructor email required"},res,4,"please provide instructor email")
		}
		else{
			let data = await instructorService.checkInstructorEmail({email})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"instructor email already exist ")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

instructorMiddleware.checkSectionNumberUpdate= async(req, res, next) => {
	try{
        let {id,course,sectionHeading,sectionNumber} = req.body
        
		if(!sectionNumber){
			console.log("here")
			sendResponse.sendCustomError({data:"sectionNumber required"},res,4,"please provide sectionNumber")
		}
		else{
		
			let data = await sectionServices.checkSectionNumberUpdate({id,course,sectionNumber})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"section number already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = instructorMiddleware;