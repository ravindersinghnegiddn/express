const courseMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var courseService = require('../../services/adminServices/courseServices')





courseMiddleware.check_course_name= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.courseName)){
			sendResponse.sendCustomError({data:"course name required"},res,4,"please provide course name")
		}
		else{
			let slugedName = slugify(req.body.courseName).toLowerCase()
			
			let data = await courseService.checkCourseName(slugedName)

			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"course name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

courseMiddleware.check_course_name_update= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.courseName)){
			sendResponse.sendCustomError({data:"course name required"},res,4,"please provide course name")
		}
		else{
			let slugedName = slugify(req.body.courseName).toLowerCase()
			
			let data = await courseService.checkCourseNameUpdate(slugedName,req.body.id)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"course name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = courseMiddleware;