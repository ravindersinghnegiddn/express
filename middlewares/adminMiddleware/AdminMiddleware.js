const AdminMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var categoryService = require('../../services/adminServices/categoryServices')


AdminMiddleware.check_login=(req, res, next) => {
	let errors = [];
	var token = req.headers.token;
	if (!token) {
		errors.push('other','401 unauthorized request.');
		return sendResponse.sendInvalidTokenError(res, {}, errors, false);
	}
	jwt.verify(token,'*sssktAf9VcK6%Pv', function (err, decoded) {
		
		if (err) {
			res.json({
		        data : {user_logout:true, show_message:"Your session has expired. Please login again."}, 
				message : "Your session has expired. Please login again.",
		        api_status : false,
		        status_code : 202
		    },202);	
		}
			
		else {
			console.log(">>>>",decoded.id)
			req.data={
			
				admin_id: decoded.id
			}
			next();
		}
	});
};


AdminMiddleware.check_category_name= async(req, res, next) => {
	try{
	 
		if(_.isEmpty(req.body.name)){
			sendResponse.sendCustomError({data:"category name required"},res,4,"please provide category name")
		}
		else{
			let slugedName = slugify(req.body.name)
			
			let data = await categoryService.checkCategoryName(slugedName)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"category name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = AdminMiddleware;