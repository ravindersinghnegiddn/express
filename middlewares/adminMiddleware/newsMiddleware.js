const newsMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');

var newsService = require('../../services/adminServices/newsServices')

newsMiddleware.check_news_name= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.newsHeadline)){
			sendResponse.sendCustomError({data:"news headline is required"},res,4,"please provide news headline")
		}
		else{
			let data = await newsService.checkNewsName(req.body.newsHeadline)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"news headline already exist")
			}
		}
	}
	catch(err){
		console.log(">>>>>>>>.",err)
		sendResponse.sendCustomError({data:err},res,4,"error in news middleware catch block")
	}
	
	
};

newsMiddleware.check_news_name_update= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.newsHeadline)){
			sendResponse.sendCustomError({data:"news headline required"},res,4,"please provide news headline")
		}
		else{
			
			
			let data = await newsService.checkNewsNameUpdate(req.body.newsHeadline,req.body.id)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"news headline  already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = newsMiddleware;