const subscriptionMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');

var subscriptionService = require('../../services/adminServices/subscriptionServices')

subscriptionMiddleware.check_subscription_name= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.subscriptionType)){
			sendResponse.sendCustomError({data:"subscription name required"},res,4,"please provide subscription name")
		}
		else{
			let data = await subscriptionService.checkSubscriptionName(req.body.subscriptionType)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"subscription name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

subscriptionMiddleware.check_subscription_name_update= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.subscriptionType)){
			sendResponse.sendCustomError({data:"subscription Type  required"},res,4,"please provide subscription Type ")
		}
		else{
			
			
			let data = await subscriptionService.checkSubscriptionNameUpdate(req.body.subscriptionType,req.body.id)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"subscription Type  already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = subscriptionMiddleware;