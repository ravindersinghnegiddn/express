const bannerMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var courseService = require('../../services/adminServices/courseServices')





bannerMiddleware.check_banner_name= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.courseName)){
			sendResponse.sendCustomError({data:"course name required"},res,4,"please provide course name")
		}
		else{
			let slugedName = slugify(req.body.courseName)
			
			let data = await courseService.checkCourseName(slugedName)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"course name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

bannerMiddleware.check_course_name_update= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.courseName)){
			sendResponse.sendCustomError({data:"course name required"},res,4,"please provide course name")
		}
		else{
			let slugedName = slugify(req.body.courseName)
			
			let data = await courseService.checkCourseNameUpdate(slugedName,req.body.id)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"course name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = bannerMiddleware;