const courseVideoMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var courseVideoService = require('../../services/adminServices/videoServices')





courseVideoMiddleware.checkEpisodeNumber= async(req, res, next) => {
	try{
        let {courseId,videoLink,episodeNumber,sectionId} = req.body
		if(_.isEmpty(episodeNumber)){
			sendResponse.sendCustomError({data:"episode Number required"},res,4,"please provide episode number")
		}
		else{
			let data = await courseVideoService.checkEpisodeNumber({episodeNumber,courseId,sectionId})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"this episode number already exist in this section")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

courseVideoMiddleware.checkEpisodeNumberUpdate= async(req, res, next) => {
	try{
        let {id,courseId,videoLink,sectionId,episodeNumber} = req.body
        
		if(!episodeNumber){
			console.log("here")
			sendResponse.sendCustomError({data:"episodeNumber required"},res,4,"please provide episodeNumber")
		}
		else{
		
			let data = await courseVideoService.checkEpisodeNumberUpdate({id,episodeNumber,courseId,sectionId})
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({},res,4,"episode number already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = courseVideoMiddleware;