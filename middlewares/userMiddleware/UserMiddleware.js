const userMiddleware = {};
const _ = require('lodash');
const userService  = require('../../services/userServices/userServices')


const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');



userMiddleware.check_login=async(req,res,next) => {
	console.log(">>>>>>>")
	let errors = [];
	var token = req.headers.token;
	if (!token) {
		errors.push('other','401 unauthorized request.');
		sendResponse.sendInvalidTokenError({}, res, 4, "401 unauthorized request. Please login again.");

		// res.json({
		// 	data : {user_logout:true, show_message:"'401 unauthorized request. Please login again."}, 
		// 	message : "401 unauthorized request. Please login again.",
		// 	api_status : false,
		// 	status_code : 202
		// },202);	
	}
	else{
	jwt.verify(token,'*sssktAf9VcK6%Pv', async function (err, decoded) {
		
		if (err) {

			sendResponse.sendInvalidTokenError({}, res, 4, "Your session has expired. Please login again.");
			// res.json({
		    //     data : {user_logout:true, show_message:"Your session has expired. Please login again."}, 
			// 	message : "Your session has expired. Please login again.",
		    //     api_status : false,
		    //     status_code : 202
		    // },202);	
		}	
		else {
			
			let loggedin_userid = decoded.id;
            //get user detail
			var userData = await userService.userDetail(loggedin_userid)
			
           // console.log(userData)
            if(userData == null ||token!=userData.token){
				sendResponse.sendInvalidTokenError({}, res, 4, "Your session has expired. Please login again.");
				// res.status(202).json({
				// 	data : {user_logout:true, show_message:"Your session has expired. Please login again."}, 
				// 	message : "Your session has expired. Please login again.",
				// 	api_status : false,
				// 	status_code : 202
				// })
			}
			else if(userData.isBlocked ==true || userData.isBlocked == 1){

				sendResponse.sendInvalidTokenError({}, res, 4, "Your account is blocked...Please contact your admin.");
				// res.json({
				// 	data : {user_logout:true, show_message:"Your account is blocked...Please contact your admin"}, 
				// 	message : "",
				// 	api_status : false,
				// 	status_code : 202
				// },202);
			}
			else if(userData.isActive ==false || userData.isActive == 0){

				sendResponse.sendInvalidTokenError({}, res, 4, "Your account is InActive...Please contact your admin.");

				// res.json({
				// 	data : {user_logout:true, show_message:"Your account is InActive...Please contact your admin"}, 
				// 	message : "",
				// 	api_status : false,
				// 	status_code : 202
				// },202);
			}
			else{
				req.data={
					
					user_id: decoded.id
				}
				next();
			}

			
		}
	});
    }
};

// userMiddleware.verifyAccount=async(req, res, next) => {
// 	let errors = [];
// 	var token = req.body.email;
// 	if (!token) {
// 		errors.push('other','401 unauthorized request.');
// 		return sendResponse.sendInvalidTokenError(res, {}, errors, false);
// 	}
	
		
		
// 		else{
			
		
// 			let loggedin_userid = decoded.id;
//             //get user detail
// 			var userData = await userService.userDetail(loggedin_userid)
//             console.log(userData)
//             if(token!=userData.token){
// 				res.json({
// 					data : {user_logout:true, show_message:"Your session has expired. Please login again."}, 
// 					message : "Your session has expired. Please login again.",
// 					api_status : false,
// 					status_code : 202
// 				},202);
// 			}
// 			else if(userData.isBlocked ==true || userData.isBlocked == 1){
// 				res.json({
// 					data : {user_logout:true, show_message:"Your account is blocked...Please contact your admin"}, 
// 					message : "",
// 					api_status : false,
// 					status_code : 202
// 				},202);
// 			}
// 			else if(userData.isActive ==false || userData.isActive == 0){
// 				res.json({
// 					data : {user_logout:true, show_message:"Your account is InActive...Please contact your admin"}, 
// 					message : "",
// 					api_status : false,
// 					status_code : 202
// 				},202);
// 			}
// 			else{
// 				req.data={
			
// 					user_id: decoded.id
// 				}
// 				next();
// 			}
// 		}
			
		
	
// };





module.exports = userMiddleware;