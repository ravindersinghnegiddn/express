const instructorMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var categoryService = require('../../services/instructorService/categoryServices')


instructorMiddleware.check_login=(req, res, next) => {
	let errors = [];
	var token = req.headers.token;
	if (!token) {
		errors.push('other','401 unauthorized request.');
		sendResponse.sendInvalidTokenError({}, res, 4, "401 unauthorized request. Please login again.");
	}
	jwt.verify(token,'*sssktAf9VcK6%Pv', function (err, decoded) {
		
		if (err) {
			sendResponse.sendInvalidTokenError({}, res, 4, "Your session has expired. Please login again.");
		}
			
		else {
			
			req.data={
			
				instructor_id: decoded.id
			}
			next();
		}
	});
};


instructorMiddleware.check_category_name= async(req, res, next) => {
	try{
	 
		if(_.isEmpty(req.body.name)){
			sendResponse.sendCustomError({data:"category name required"},res,4,"please provide category name")
		}
		else{
			let slugedName = slugify(req.body.name)
			
			let data = await categoryService.checkCategoryName(slugedName)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"category name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = instructorMiddleware;