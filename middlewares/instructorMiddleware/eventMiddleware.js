const eventMiddleware = {};
const _ = require('lodash');
var slugify = require('slugify')

const jwt = require('jsonwebtoken');

var sendResponse = require('../../util/CustomResponse');
var eventService = require('../../services/instructorService/eventServices')





eventMiddleware.check_event_name= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.name)){
			sendResponse.sendCustomError({data:"event name required"},res,4,"please provide event name")
		}
		else{
			let slugedName = slugify(req.body.name)
			var instructorId = req.data.instructor_id
			let data = await eventService.checkEventName(slugedName,instructorId)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"event name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};

eventMiddleware.check_event_name_update= async(req, res, next) => {
	try{
		if(_.isEmpty(req.body.name)){
			sendResponse.sendCustomError({data:"event name required"},res,4,"please provide event name")
		}
		else{
			let slugedName = slugify(req.body.name)
			var instructorId = req.data.instructor_id
			let data = await eventService.checkEventNameUpdate(slugedName,req.body.id,instructorId)
			if(data == null){
				next()
			}
			else{
				sendResponse.sendCustomError({data:data},res,4,"event name already exist")
			}
		}
	}
	catch(err){
		console.log(err)
		sendResponse.sendCustomError({data:err},res,4,"error in catch block")
	}
	
	
};



module.exports = eventMiddleware;