
var _ = require('lodash');

exports.sendSuccess = (data, res, status, message) => {
  // console.log("________________", data, status, message);
  let response = {
    "status": status,
    "message": message,
    "result": data,
    "responseCode": 200
  }
  let errorCode = 200;
  // console.log("_____", response);
  res.status(errorCode || 200).json(response);
}

exports.sendCustomError = (data, res, status, message) => {
  // console.log("_____", data);
  let response = {
    "status": status,
    "message": message,
    "result": {
      data: data
    },
    "responseCode": 200
  }
  let errorCode = 200;
  // console.log("_____", response);
  res.status(errorCode || 400).json(response);
}

exports.sendInvalidTokenError = (data, res, status, message) => {
  // console.log("_____", data);
  let response = {
    "status": 0,
    "message": 'failure',
    "result": {
      code: status,
      message: message,
      data: data
    },
    "responseCode": 402
  }
  let errorCode = 202;
  // console.log("_____", response);
  res.status(errorCode || 202).json(response);
}

