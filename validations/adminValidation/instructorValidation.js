
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const addInstructorValidation = (req, res, next) => {
    let {email,name,password} = req.body
   
    let error = [];
    console.log(req.files)
    
    if (_.isEmpty(email)) {
        error.push({ "field name": "email", "message": "email" + ' is required' });
    }
   
    if (_.isEmpty(name)) {
        error.push({ "field name": "name", "message": "name" + ' is required' });
    }

    if (_.isEmpty(password)) {
        error.push({ "field name": "password", "message": "password" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateInstructorValidation = (req, res, next) => {

    let {id,courseId,videoLink} = req.body
    console.log(req.body)
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "videoId", "message": "videoId" + ' is required' });
    }

    if (_.isEmpty(courseId)) {
        error.push({ "field name": "courseId", "message": "courseId" + ' is required' });
    }
   
    if (_.isEmpty(videoLink)) {
        error.push({ "field name": "videoLink", "message": "videoLink" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    
};
const viewInstructorValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "instructor_id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    addInstructorValidation,
    updateInstructorValidation,
    viewInstructorValidation
}