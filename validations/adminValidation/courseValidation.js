
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const addCourseValidation = (req, res, next) => {
    let {categories,courseName,courseDescription,courseTagLine,courseGoal,courseThumbnail,coursePrice,courseAuthor,startDate,duration,trailerLink} = req.body
   
    let error = [];
  
    
    if((typeof(req.body.categories)) == 'string'){
        var categoryArray = JSON.parse(categories)     
    }
    // else{
    //   var categoryArray = categories.split(',');
    // }
    var categoryArray = JSON.parse(JSON.stringify(categories))
    if (_.isEmpty(categories) || categoryArray.length<=0) {

        error.push({ "field name": "categories", "message": "categories" + ' are required' });
    }
   
    if (_.isEmpty(courseName)) {
      
        error.push({ "field name": "courseName", "message": "courseName" + ' is required' });
    }
    if (_.isEmpty(courseDescription)) {
        error.push({ "field name": "courseDescription", "message": "courseDescription" + ' is required' });
    }
    if (_.isEmpty(courseTagLine)) {
        error.push({ "field name": "courseTagLine", "message": "courseTagLine" + ' is required' });
    }
    
    if (_.isEmpty(courseThumbnail)) {
        error.push({ "field name": "courseThumbnail", "message": "courseThumbnail" + ' is required' });
    }
    if (_.isEmpty(coursePrice)) {
        error.push({ "field name": "coursePrice", "message": "coursePrice" + ' is required' });
    }
    if (_.isEmpty(courseAuthor)) {
        error.push({ "field name": "courseAuthor", "message": "courseAuthor" + ' is required' });
    }
    // if (_.isEmpty(startDate)) {
    //     error.push({ "field name": "startDate", "message": "startDate" + ' is required' });
    // }
    if (_.isEmpty(duration)) {
        error.push({ "field name": "duration", "message": "duration" + ' is required' });
    }
    if (_.isEmpty(trailerLink)) {
        error.push({ "field name": "trailerLink", "message": "trailerLink" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateCourseValidation = (req, res, next) => {

    let {id,categories,courseName,courseDescription,courseTagLine,courseGoal,courseThumbnail,coursePrice,courseAuthor,startDate,duration,trailerLink} = req.body
   
    let error = [];
    console.log(req.files)
    
    if((typeof(req.body.categories)) == 'string'){
        var categoryArray = JSON.parse(categories)     
    }
    // else{
    //   var categoryArray = categories.split(',');
    // }
    var categoryArray = JSON.parse(JSON.stringify(categories))
    if (_.isEmpty(categories) || categoryArray.length<=0) {

        error.push({ "field name": "categories", "message": "categories" + ' are required' });
    }
    if (_.isEmpty(id)) {
        error.push({ "field name": "courseId", "message": "courseId" + ' is required' });
    }
    if (_.isEmpty(courseName)) {
      
        error.push({ "field name": "courseName", "message": "courseName" + ' is required' });
    }
    if (_.isEmpty(courseDescription)) {
        error.push({ "field name": "courseDescription", "message": "courseDescription" + ' is required' });
    }
    if (_.isEmpty(courseTagLine)) {
        error.push({ "field name": "courseTagLine", "message": "courseTagLine" + ' is required' });
    }
    
    if (_.isEmpty(courseThumbnail)) {
        error.push({ "field name": "courseThumbnail", "message": "courseThumbnail" + ' is required' });
    }
    if (_.isEmpty(coursePrice)) {
        error.push({ "field name": "coursePrice", "message": "coursePrice" + ' is required' });
    }
    if (_.isEmpty(courseAuthor)) {
        error.push({ "field name": "courseAuthor", "message": "courseAuthor" + ' is required' });
    }
    // if (_.isEmpty(startDate)) {
    //     error.push({ "field name": "startDate", "message": "startDate" + ' is required' });
    // }
    if (_.isEmpty(duration)) {
        error.push({ "field name": "duration", "message": "duration" + ' is required' });
    }
    if (_.isEmpty(trailerLink)) {
        error.push({ "field name": "trailerLink", "message": "trailerLink" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    
};
const viewCourseValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "course_id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    addCourseValidation,
    updateCourseValidation,
    viewCourseValidation
}