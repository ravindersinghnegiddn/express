
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const subscriptionValidation = (req, res, next) => {
    var {subscriptionType,subscriptionDetail,subscriptionPrice} = req.body
    console.log(req.body)
    let error = [];
    if (_.isEmpty(subscriptionType)) {
        error.push({ "field name": "subscriptionType", "message": "subscriptionType" + ' is required' });
    }
    if (_.isEmpty(subscriptionDetail)) {
      
        error.push({ "field name": "subscriptionDetail", "message": "subscriptionDetail" + ' is required' });
    }
    if (_.isEmpty(subscriptionPrice)) {
        error.push({ "field name": "subscriptionPrice", "message": "subscriptionPrice" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateSubscriptionValidation = (req, res, next) => {
   
    var {id,subscriptionType,subscriptionDetail,subscriptionPrice} = req.body
    console.log(req.body)
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "id", "message": "id" + ' is required' });
    }
    if (_.isEmpty(subscriptionType)) {
        error.push({ "field name": "subscriptionType", "message": "subscriptionType" + ' is required' });
    }
    if (_.isEmpty(subscriptionDetail)) {
      
        error.push({ "field name": "subscriptionDetail", "message": "subscriptionDetail" + ' is required' });
    }
    // if (_.isEmpty(subscriptionPrice)) {
    //     error.push({ "field name": "subscriptionPrice", "message": "subscriptionPrice" + ' is required' });
    // }

    if (error.length > 0) {
        console.log(error)
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};
const viewSubscriptionValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "subscription id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    subscriptionValidation,
    updateSubscriptionValidation,
    viewSubscriptionValidation
}