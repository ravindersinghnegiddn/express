
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const addSectionValidation = (req, res, next) => {
    let {course,sectionHeading,sectionNumber} = req.body
   
    let error = [];
    console.log(req.files)
    
    if (_.isEmpty(course)) {
        error.push({ "field name": "courseId", "message": "courseId" + ' is required' });
    }
   
    if (_.isEmpty(sectionHeading)) {
        error.push({ "field name": "sectionHeading", "message": "sectionHeading" + ' is required' });
    }

    // if (_.isEmpty(sectionNumber)) {
    //     error.push({ "field name": "sectionNumber", "message": "sectionNumber" + ' is required' });
    // }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateSectionValidation = (req, res, next) => {

    let {id,course,sectionHeading,sectionNumber} = req.body
    console.log(sectionNumber,(_.isEmpty(sectionNumber)))
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "section Id", "message": "section Id" + ' is required' });
    }

    if (_.isEmpty(course)) {
        error.push({ "field name": "courseId", "message": "courseId" + ' is required' });
    }
   
    if (_.isEmpty(sectionHeading)) {
        error.push({ "field name": "sectionHeading", "message": "sectionHeading" + ' is required' });
    }
    // if (_.isEmpty(sectionNumber)) {
    //     error.push({ "field name": "sectionNumber", "message": "sectionNumber" + ' is required' });
    // }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    
};
const viewSectionValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "section_id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    addSectionValidation,
    updateSectionValidation,
    viewSectionValidation
}