
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const addFaqValidation = (req, res, next) => {
    let {question,answer} = req.body
   
    let error = [];
    
    
  
    if (_.isEmpty(question)) {

        error.push({ "field name": "question", "message": "question" + ' is required' });
    }
   
    if (_.isEmpty(answer)) {
      
        error.push({ "field name": "answer", "message": "answer" + ' is required' });
    }
  

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateFaqValidation = (req, res, next) => {

    let {id,question,answer} = req.body
   
    let error = [];
   
    
    if (_.isEmpty(id)) {

        error.push({ "field name": "faqId", "message": "faqid" + ' is required' });
    }
    if (_.isEmpty(question)) {

        error.push({ "field name": "question", "message": "question" + ' is required' });
    }
   
    if (_.isEmpty(answer)) {
      
        error.push({ "field name": "answer", "message": "answer" + ' is required' });
    }
  

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    
};
const viewFaqValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "course_id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    addFaqValidation ,
    updateFaqValidation ,
    viewFaqValidation 
}