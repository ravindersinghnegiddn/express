
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const questionValidation = (req, res, next) => {
    let { category_id,question,subQuestion} = req.body;
    console.log(req.body)
    let error = [];
    if (_.isEmpty(category_id)) {
        error.push({ "field name": "category_id", "message": "category_id" + ' is required' });
    }
    if (_.isEmpty(question)) {
      
        error.push({ "field name": "question", "message": "question" + ' is required' });
    }
    // if (_.isEmpty(subQuestion)) {
    //     error.push({ "field name": "subQuestion", "message": "subQuestion" + ' is required' });
    // }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateQuestionValidation = (req, res, next) => {
    let { id,category_id,question,subQuestion} = req.body;
    console.log(req.body)
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "id", "message": "id" + ' is required' });
    }
    if (_.isEmpty(category_id)) {
        error.push({ "field name": "category_id", "message": "category_id" + ' is required' });
    }
    if (_.isEmpty(question)) {
      
        error.push({ "field name": "question", "message": "question" + ' is required' });
    }
    // if (_.isEmpty(subQuestion)) {
    //     error.push({ "field name": "subQuestion", "message": "subQuestion" + ' is required' });
    // }

    if (error.length > 0) {
        console.log(error)
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};
const viewQuestionValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "question id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    questionValidation,
    updateQuestionValidation,
    viewQuestionValidation
}