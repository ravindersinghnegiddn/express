
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const addEventValidation = (req, res, next) => {
    let {name,eventDescription,icon} = req.body
   
    let error = [];
    console.log(req.files)
    
    if (_.isEmpty(name)) {
        error.push({ "field name": "name", "message": "event name" + ' is required' });
    }
   
    if (_.isEmpty(eventDescription)) {
        error.push({ "field name": "eventDescription", "message": "eventDescription" + ' is required' });
    }

    if (_.isEmpty(icon)) {
        error.push({ "field name": "icon", "message": "icon" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateEventValidation = (req, res, next) => {

    
    let {id,name,eventDescription,icon} = req.body
    
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "eventId", "message": "eventid" + ' is required' });
    }

    if (_.isEmpty(name)) {
        error.push({ "field name": "name", "message": "event name" + ' is required' });
    }
   
    if (_.isEmpty(eventDescription)) {
        error.push({ "field name": "eventDescription", "message": "eventDescription" + ' is required' });
    }

    if (_.isEmpty(icon)) {
        error.push({ "field name": "icon", "message": "icon" + ' is required' });
    }

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    
};
const viewEventValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "instructor_id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    addEventValidation,
    updateEventValidation,
    viewEventValidation
}