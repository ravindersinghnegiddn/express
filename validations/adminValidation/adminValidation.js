//const userService = require('../services/userServices')
const adminService = require('../../services/adminServices/adminServices')
const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

class adminValidation{
    constructor(){}
   
    adminValidation =async(req,res,next) =>{
      let email = req.body.email

      let data = await adminService.checkEmail(email)
      console.log(data)
      if(!data){
        next()
      }
      else{
        sendResponse.sendCustomError("error", res, 4, "email already exist");
      }
  }
  isEmailExist =async(req,res,next) =>{
    let email = req.body.email

    let data = await adminService.checkEmail(email)
    console.log(data)
    if(!data){
      sendResponse.sendCustomError("error", res, 4, "email not found");
      
    }
    else{
      next()
    }
}



    userIdValidation = (req, res, next) => {
        let { id} = req.query;
      
        let error = [];
        if (_.isEmpty(id)) {
            error.push({ "field name": " id", "message": " user id" + ' is required' });
        }
       
    
        if (error.length > 0) {
            //send response validation error
            console.log('---------1--------->Entered values validated');
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            console.log('----------2-------->Entered values validated');
            next();
        }
    }





}
module.exports = new adminValidation()