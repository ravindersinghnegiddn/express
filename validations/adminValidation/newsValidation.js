
var writeResponse = require('../../util/CustomResponse');
var _ = require('lodash')

const newsValidation = (req, res, next) => {
    var {newsHeadline,newsDetail,newsThumbnail,newsUrl} = req.body
    console.log(req.body)
    let error = [];
    console.log(_.isEmpty(newsHeadline))
    if (_.isEmpty(newsHeadline)) {
        error.push({ "field name": "newsHeadline", "message": "newsHeadline" + ' is required' });
    }
    if (_.isEmpty(newsDetail)) {
      
        error.push({ "field name": "newsDetail", "message": "newsDetail" + ' is required' });
    }
    if (_.isEmpty(newsThumbnail)) {
        error.push({ "field name": "newsThumbnail", "message": "newsThumbnail" + ' is required' });
    }
    
    console.log(error.length)
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};

const updateNewsValidation = (req, res, next) => {
   
    var {id,newsHeadline,newsDetail,newsThumbnail,newsUrl} = req.body
    console.log(req.body)
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": "id", "message": "id" + ' is required' });
    }
    if (_.isEmpty(newsHeadline)) {
        error.push({ "field name": "newsHeadline", "message": "newsHeadline" + ' is required' });
    }
    if (_.isEmpty(newsDetail)) {
      
        error.push({ "field name": "newsDetail", "message": "newsDetail" + ' is required' });
    }
    if (_.isEmpty(newsThumbnail)) {
        error.push({ "field name": "newsThumbnail", "message": "newsThumbnail" + ' is required' });
    }
    
    console.log(error.length)
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};
const viewNewsValidation = (req, res, next) => {
    let { id} = req.query;
  
    let error = [];
    if (_.isEmpty(id)) {
        error.push({ "field name": " id", "message": "news id" + ' is required' });
    }
   

    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated');
        writeResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
};


module.exports = {
    newsValidation,
    updateNewsValidation,
    viewNewsValidation
}