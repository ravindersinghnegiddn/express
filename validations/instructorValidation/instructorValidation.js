//const userService = require('../services/userServices')
const adminService = require('../../services/adminServices/adminServices')
const instructorService = require('../../services/instructorService/instructorServices')
const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

class instructorValidation{
    constructor(){}
   
    instructorValidation =async(req,res,next) =>{
      let email = req.body.email
      let data = await instructorService.checkEmail(email)
      console.log(data)
      if(!data){
        next()
      }
      else{
        sendResponse.sendCustomError("error", res, 4, "email already exist");
      }
    }
    isEmailExist =async(req,res,next) =>{
    let email = req.body.email

    let data = await instructorService.checkEmail(email)
    
    if(!data){
      sendResponse.sendCustomError("error", res, 4, "email not found");
      
    }
    else{
        if(data.isBlocked == true || data.isBlocked ==1){
            sendResponse.sendCustomError("error", res, 4, "your account is blocked by admin");
        }
        else{
            next()
        }
      
    }
    }
    instructorSignupValidation = (req, res, next) => {
        let {name,email,password} = req.body
        
        let error = [];
        
        if (_.isEmpty(name)) {
    
            error.push({ "field name": "name", "message": "name" + ' are required' });
        }
       
        if (_.isEmpty(email)) {
          
            error.push({ "field name": "email", "message": "email" + ' is required' });
        }
        if (_.isEmpty(password)) {
            error.push({ "field name": "password", "message": "password" + ' is required' });
        }
     
        if (error.length > 0) {
            //send response validation error
            console.log('---------1--------->Entered values validated',error);
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            console.log('----------2-------->Entered values validated');
            next();
        }
    };
    instructorJoinFormValidation = (req, res, next) => {
        let {name,last_name,category,why_to_help,email} = req.body
        
        let error = [];
        
        if (_.isEmpty(name)) {error.push({ "field name": "name", "message": 'First name is required' });}
        else if (_.isEmpty(last_name)) {error.push({ "field name": "last_name", "message": 'Last name is required' });}
        else if (_.isEmpty(email)) {error.push({ "field name": "email", "message": 'Enter valid email address.' });}
        else if (_.isEmpty(category)) {error.push({ "field name": "category", "message": 'Please choose category.' });}
        else if (_.isEmpty(why_to_help)) {error.push({ "field name": "why_to_help", "message": 'Please explain Why do you love helping people.' });}
     
        if (error.length > 0) {
            //send response validation error
            //console.log('---------1--------->Entered values validated',error);
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            //console.log('----------2-------->Entered values validated');
            next();
        }
    };
    userIdValidation = (req, res, next) => {
        let { id} = req.query;
      
        let error = [];
        if (_.isEmpty(id)) {
            error.push({ "field name": " id", "message": " user id" + ' is required' });
        }
       
    
        if (error.length > 0) {
            //send response validation error
            console.log('---------1--------->Entered values validated');
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            console.log('----------2-------->Entered values validated');
            next();
        }
    }
    instructorLoginValidation = (req, res, next) => {
        let {email,password} = req.body;
      
        let error = [];
        if (_.isEmpty(email)) {
            error.push({ "field name": " email", "message": " email " + ' is required' });
        }

        if (_.isEmpty(password)) {
            error.push({ "field name": " password", "message": " password " + ' is required' });
        }
       
    
        if (error.length > 0) {
            //send response validation error
            console.log('---------1--------->Entered values validated');
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            console.log('----------2-------->Entered values validated');
            next();
        }
    }
    instructorGetProfileValidation = (req, res, next) => {
        let id = req.query.instructorId
        console.log(">>>>>>>>",req.query.userId)
        let error = [];
  
        if (_.isEmpty(id)){
  
            error.push({ "field name": "instructorId", "message": "instructorId" + ' is required' });
        }
      
        if (error.length > 0) {
            //send response validation error
            console.log('---------1--------->Entered values validated',error);
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            console.log('----------2-------->Entered values validated');
            next();
        }
    };
    instructorUpdateProfileValidation = (req, res, next) => {
        let {name,profileImg,title,city,country,industry,aboutMe,website,facebook,twitter,linkedin,dob} = req.body
        
        let error = [];
    
        if (_.isEmpty(name)) {
    
            error.push({ "field name": "name", "message": "name" + ' is required' });
        }

    
       
        if (error.length > 0) {
            //send response validation error
            //console.log('---------1--------->Entered values validated',error);
            sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
        }
        else {
            //console.log('----------2-------->Entered values validated');
            next();
        }
    };




}
module.exports = new instructorValidation()