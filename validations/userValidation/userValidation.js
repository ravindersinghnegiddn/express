const userService = require('../../services/userServices/userServices')
const adminService = require('../../services/adminServices/adminServices')
const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

class userValidation{
    constructor(){}
    userValidation =async(req,res,next) =>{
        let email = req.body.email

        let data = await userService.checkEmail(email)
        
        if(!data){
          next()
        }
        else{
        
        sendResponse.sendCustomError({}, res, 4, "email already exist");
        }
    }
    passwordValidation =async(req,res,next) =>{
      let password = req.body.password;
      let user_id  = req.body.user_id;

      if(_.isEmpty(password)){
        sendResponse.sendCustomError({}, res, 4, "Please Enter Password");
      }
      else if(_.isEmpty(user_id)){
        sendResponse.sendCustomError({}, res, 4, "Please Enter User Id");
      }
      else{
      next();
      }
  }
    adminValidation =async(req,res,next) =>{
      let email = req.body.email

      let data = await adminService.checkEmail(email)
      console.log(data)
      if(!data){
        next()
      }
      else{
        sendResponse.sendCustomError("error", res, 4, "email already exist");
      }
    }
    isEmailExist =async(req,res,next) =>{  
      let email = req.body.email

      let data = await userService.checkEmail(email)
      //console.log(data)
      if(!data){
        sendResponse.sendCustomError({}, res, 4, "email not found");
        
      }
      else{
        if((data.isBlocked ==true || data.isBlocked == 1)){
          sendResponse.sendInvalidTokenError({}, res, 4, "Your account is blocked.Please contact you admin");
        }
        else if(data.isActive ==false || data.isActive == 0){
          sendResponse.sendInvalidTokenError({}, res, 4, "Your account is inactive.Please contact you admin");
        }
        else{
        next()
        }
      }
    }
    userSignupValidation = (req, res, next) => {
      let {firstName,email,password,categories,questions} = req.body
      
      let error = [];
      

      if((typeof(req.body.categories)) == 'string'){
        var categoryArray = JSON.parse(JSON.stringify(categories))   
      }
      else{
        //console.log(req.body.categories[0])
        //var categoryArray = categories.split(',');
      }
      

      if((typeof(req.body.questions)) == 'string'){
        var questionArray = JSON.parse(JSON.stringify(questions))     
      }
      else{
        var questionArray = questions.split(',');
      }
      if (_.isEmpty(firstName)) {
  
          error.push({ "field name": "firstName", "message": "firstName" + ' are required' });
      }
     
      if (_.isEmpty(email)) {
        
          error.push({ "field name": "email", "message": "email" + ' is required' });
      }
      if (_.isEmpty(password)) {
          error.push({ "field name": "password", "message": "password" + ' is required' });
      }
      if(_.isEmpty(categories) || categoryArray.length<=0){
        error.push({ "field name": "categories", "message": "categories" + ' are required' });
      }
      if(categoryArray.length<3 || categories.length <3 ){
        error.push({ "field name": "categories", "message": "at least 3 categories" + ' are required' });
      }

      if(_.isEmpty(questions) || questionArray.length<=0){
        error.push({ "field name": "question", "message": "questions" + ' are required' });
      }
      if(questionArray.length<3 || questions.length<3){
        error.push({ "field name": "question", "message": "at least 3 question" + ' are required' });
      }
    
  
      if (error.length > 0) {
          //send response validation error
          console.log('---------1--------->Entered values validated',error);
          sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
      }
      else {
          console.log('----------2-------->Entered values validated');
          next();
      }
    };
    userUpdateProfileValidation = (req, res, next) => {
    let {firstName,profileImg,lastName,title,city,country,industry,aboutMe,website,facebook,twitter,linkedin,dob} = req.body
    
    let error = [];

    if (_.isEmpty(firstName)) {

        error.push({ "field name": "firstName", "message": "firstName" + ' are required' });
    }
   
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
    };
    userGetProfileValidation = (req, res, next) => {
      let id = req.query.userId
      console.log(">>>>>>>>",req.query.userId)
      let error = [];

      if (_.isEmpty(id)){

          error.push({ "field name": "userId", "message": "user id" + ' is required' });
      }
    
      if (error.length > 0) {
          //send response validation error
          console.log('---------1--------->Entered values validated',error);
          sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
      }
      else {
          console.log('----------2-------->Entered values validated');
          next();
      }
    };
}
module.exports = new userValidation()