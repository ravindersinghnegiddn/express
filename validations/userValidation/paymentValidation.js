const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

class paymentValidation{
    constructor(){}
  

createOrderValidation = (req, res, next) => {
  let {user_id,course_id,total_price} = req.body;
  let error = [];

  if (_.isEmpty(user_id)){
      error.push({ "field name": "userId", "message": "user id" + ' is required' });
  }
  if (_.isEmpty(course_id)){
    error.push({ "field name": "course_id", "message": "course_id" + ' is required' });
  }
  if (_.isEmpty(total_price)){
    error.push({ "field name": "total_price", "message": "total_price" + ' is required' });
}

  if (error.length > 0) {
      //send response validation error
      console.log('---------1--------->Entered values validated',error);
      sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
  }
  else {
      console.log('----------2-------->Entered values validated');
      next();
  }
};



OrderPaymentInitValidation = (req, res, next) => {
    let {first_name,last_name,email,phone_number,address,country,state,pincode,purchase_type,course_id,course_price,tax,total_price,user_id,order_id,currency,browse_type,membership_price} = req.body;
    let error = [];
    if (_.isEmpty(first_name)){
      error.push({ "field name": "first_name", "message": "First name" + ' is required' });
    }
    if (_.isEmpty(email)){
      error.push({ "field name": "email", "message": "Email" + ' is required' });
    }
    // if (_.isEmpty(phone_number)){
    //   error.push({ "field name": "phone_number", "message": "Phone number" + ' is required' });
    // }
    if (_.isEmpty(address)){
      error.push({ "field name": "address", "message": "Address" + ' is required' });
    }
    if (_.isEmpty(country)){
      error.push({ "field name": "country", "message": "Country" + ' is required' });
    }
    if (_.isEmpty(state)){
      error.push({ "field name": "state", "message": "State" + ' is required' });
    }
    // if (_.isEmpty(pincode)){
    //   error.push({ "field name": "pincode", "message": "Pincode" + ' is required' });
    // }
    if (_.isEmpty(purchase_type)){
      error.push({ "field name": "purchase_type", "message": "Purchase Type" + ' is required' });
    }
    if(purchase_type == 'single'){
      if (_.isEmpty(course_id)){
        error.push({ "field name": "course_id", "message": "Course id" + ' is required' });
      }
      if (_.isEmpty(course_price)){
        error.push({ "field name": "course_price", "message": "course_price" + ' is required' });
      }
    }

    if(purchase_type == 'membership'){
      if (_.isEmpty(membership_price)){
        error.push({ "field name": "membership_price", "message": "membership_price" + ' is required' });
      }
    }
    
    if (_.isEmpty(user_id)){
        error.push({ "field name": "userId", "message": "user id" + ' is required' });
    }
    if (_.isEmpty(order_id)){
      error.push({ "field name": "order_id", "message": "order_id" + ' is required' });
    }
    
    if (_.isEmpty(tax)){
      error.push({ "field name": "tax", "message": "tax" + ' is required' });
    }
    if (_.isEmpty(total_price)){
      error.push({ "field name": "total_price", "message": "total_price" + ' is required' });
    }
    if (_.isEmpty(currency)){
        error.push({ "field name": "currency ", "message": "currency " + ' is required' });
      }
    if (_.isEmpty(browse_type)){
      error.push({ "field name": "browse_type ", "message": "browse_type " + ' is required' });
    }
  
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
  };

OrderPaymentFinalValidation = (req, res, next) => {
    let {user_id,order_id,status,stripeToken,customerId,captureAmount,stripeChargeId} = req.body;
    let error = [];
  
    if (_.isEmpty(user_id)){
        error.push({ "field name": "userId", "message": "user id" + ' is required' });
    }
    if (_.isEmpty(order_id)){
      error.push({ "field name": "order_id", "message": "order_id" + ' is required' });
    }
    if (_.isEmpty(status)){
        error.push({ "field name": "status ", "message": "status(failed/paid)" + ' is required' });
      }
    // if (_.isEmpty(customerId)){
    //     error.push({ "field name": "customerId", "message": "customerId" + ' is required' });
    // }  
    if (_.isEmpty(captureAmount)){
        error.push({ "field name": "captureAmount", "message": "captureAmount" + ' is required' });
    }  
    if (_.isEmpty(stripeChargeId)){
        error.push({ "field name": "stripeChargeId", "message": "stripeChargeId" + ' is required' });
    }  
  
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
  };

subscriptionPaymentInitValidation = (req, res, next) => {
    let {user_id,total_price,currency} = req.body;
    let error = [];
  
    if (_.isEmpty(user_id)){
        error.push({ "field name": "userId", "message": "user id" + ' is required' });
    }
 
    if (_.isEmpty(total_price)){
      error.push({ "field name": "total_price", "message": "total_price" + ' is required' });
    }
    if (_.isEmpty(currency)){
        error.push({ "field name": "currency ", "message": "currency " + ' is required' });
      }
  
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
  };
subscriptionPaymentFinalValidation = (req, res, next) => {
    let {user_id,payment_id,status,stripePaymentId} = req.body;
    let error = [];
  
    if (_.isEmpty(user_id)){
        error.push({ "field name": "userId", "message": "user id" + ' is required' });
    }
    if (_.isEmpty(payment_id)){
      error.push({ "field name": "payment_id", "message": "payment_id" + ' is required' });
    }
    // if (_.isEmpty(total_price)){
    //   error.push({ "field name": "total_price", "message": "total_price" + ' is required' });
    // }
    if (_.isEmpty(status)){
        error.push({ "field name": "status ", "message": "status(failed/paid)" + ' is required' });
      }
    if (_.isEmpty(stripePaymentId)){
        error.push({ "field name": "stripePaymentId", "message": "stripePaymentId" + ' is required' });
      }  
  
    if (error.length > 0) {
        //send response validation error
        console.log('---------1--------->Entered values validated',error);
        sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
    }
    else {
        console.log('----------2-------->Entered values validated');
        next();
    }
  };  

}
module.exports = new paymentValidation()