const sendResponse = require('../../util/CustomResponse')
const _ = require('lodash')

class lastSeenValidation{
    constructor(){}
  

lastSeenValidation = (req, res, next) => {
  let {userId,videoId,lastSeen} = req.body;
  let error = [];

  if (_.isEmpty(userId)){
      error.push({ "field name": "userId", "message": "user id" + ' is required' });
  }
  if (_.isEmpty(videoId)){
    error.push({ "field name": "videoId", "message": "videoId" + ' is required' });
  }
  if (_.isEmpty(lastSeen)){
    error.push({ "field name": "lastSeen", "message": "lastSeen" + ' is required' });
}

  if (error.length > 0) {
      //send response validation error
      console.log('---------1--------->Entered values validated',error);
      sendResponse.sendCustomError({ validation: error }, res, 4, "Validation Error");
  }
  else {
      console.log('----------2-------->Entered values validated');
      next();
  }
};






 

}
module.exports = new lastSeenValidation()