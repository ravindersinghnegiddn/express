var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var cors = require('cors')
var app = express();
var schedule     = require('node-schedule');
const upload = require("express-fileupload");
const userCronController = require('./controller/adminController/userCronController')
app.use(upload({
  useTempFiles:true
}));
require('dotenv').config()

var bodyParser = require('body-parser')
var connection = require('./database/connection')
// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'ejs');
app.use("/userImages", express.static("uploads/userImages"));
app.use("/categoryImages", express.static("uploads/categoryImages"));
app.use(cors())
app.use(logger('dev'));
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.bodyParser({limit: '50mb'}));

app.use('/user', require("./routes/userRoutes"));
app.use('/admin',require("./routes/adminRoutes"))
app.use('/instructor',require("./routes/instructorRoutes"))
//app.use('/', express.static(__dirname + '/public/dist'));
// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
app.use(function(err, req, res, next) {
  console.log("error ========== ",err)
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// var k = schedule.scheduleJob('*/5 * * * * *', function(){
//   userCronController.userCronInActive();
//   console.log('In Cron run 2!');
// });

module.exports = app;
