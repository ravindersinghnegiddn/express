class required{
    constructor(){}
    
    bannerModel                 =   require('../database/schema/banner_schema');
    homeContentModel            =   require('../database/schema/homeContent_schema');
    aboutUsWebModel             =   require('../database/schema/webAbout_schema');
    testimonialModel            =   require('../database/schema/testimonials_scheme');
    supportCatroriesModel       =   require('../database/schema/bliss_support_categories');
    supportFaqsModel            =   require('../database/schema/faq_schema');
    retreatModel                =   require('../database/schema/bliss_retreats');
    careerModel                 =   require('../database/schema/bliss_careers');
    jobOpeningModel             =   require('../database/schema/bliss_job_openings');
    podcastsModel               =   require('../database/schema/bliss_podcasts');
    applicationModel            =   require('../database/schema/bliss_applied_jobs');
    termsModel                  =   require('../database/schema/bliss_terms_policies');
    coursesModel                =   require('../database/schema/course_schema');
    coursesDetails              =   require('../database/schema/bliss_course_details');
    sectionDetails              =   require('../database/schema/section_schema');
    membershipDetails           =   require('../database/schema/bliss_membership_details');
    categories                  =   require('../database/schema/categories_shema');
    userModel                   =   require('../database/schema/user_schema');
    orderModel                  =   require('../database/schema/payment_schema');
    quotesModel                 =   require('../database/schema/bliss_quotes');

    adminModel                  =   require('../database/schema/admin_schema');
    mainModuleModel             =   require('../database/schema/bliss_admin_main_module');
    moduleModel                 =   require('../database/schema/bliss_admin_modules');
    questionModel               =   require('../database/schema/question_schema');
    newsnModel                  =   require('../database/schema/news_schema');
    appliedJobsModel            =   require('../database/schema/bliss_applied_jobs');
    contactsModel               =   require('../database/schema/contact_us_scheme');
    countryModel                =   require('../database/schema/country_scheme');
    agoraModel                  =   require('../database/schema/bliss_agora_details');
    eventModel                  =   require('../database/schema/event_schema');
    certiModel                  =   require('../database/schema/bliss_certificates');
    paymentModel                =   require('../database/schema/payment_schema');
    instructorModel             =   require('../database/schema/instructor_schema');
    liveChatModel               =   require('../database/schema/bliss_live_chat');
    generalModel                =   require('../database/schema/bliss_general_data');
    corporateModel              =   require('../database/schema/corporate_cms_scheme');
    emailTemplateModel          =   require('../database/schema/bliss_email_template');
    notifTemplateModel          =   require('../database/schema/bliss_notification_template');
    seoModel                    =   require('../database/schema/bliss_seo');
    newsLetterModel             =   require('../database/schema/bliss_newsletter_subscription');
    
}
module.exports = new required;