let mongoose = require('mongoose');

const server = '127.0.0.1:27017';
const database = 'testing_db';      

class Database {
  constructor() {
    this._connect()
  }
  serverUrl = `mongodb://blissGenx:blissGenx@localhost:27017/bliss_Database`
  localUrl = `mongodb://localhost:27017/bliss_Database`
  //mongodb://blissGenx:blissGenx@3.16.96.108:27017/bliss_Database
_connect() {
     mongoose.connect(this.localUrl,{useNewUrlParser: true, useUnifiedTopology: true})
       .then(() => {
         console.log('Database connected')
       })
       .catch(err => {
         console.error('Database connection error',err)
       })
  }
}

module.exports = new Database()