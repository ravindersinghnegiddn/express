const Mongoose = require("mongoose");

const testimonialModel = new Mongoose.Schema({

    testimonial_id:{type:Number,required: true,trim: true},
    user_name:{type: String,required: true,trim: true},
    user_img:{type: String,required: true,trim: true},
    review_for:{type: String,required: true,trim: true},
    review:{type: String,required: true,trim: true},
    featured:{type: String,required: true,trim: true},
    status:{type: String,required: true,trim: true},
    created_date:{type:String,efault:Date.now()}
});
module.exports = Mongoose.model("bliss_testimonial", testimonialModel);