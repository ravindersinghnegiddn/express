const Mongoose = require("mongoose");

const homeContent = new Mongoose.Schema({
    //categories:[Mongoose.Schema.Types.ObjectId],
    //categories:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' }],
    home_about_title: {
        type: String,
        required: true,
        trim: true
    },
    home_about_description: {
        type: String,
        required: true,
        trim: true
    },
    home_about_image: {
        type: String,
        required: true,
        trim: true
    },
    home_program_title: {
        type: String,
        required: true,
        trim: true
    },
 
    home_program_description: {
        type: String,
        required: true,
        trim: true
    },

    home_category_title: {
        type: String,
         default:'',
        trim: true
    },
    home_category_sub_title: {
        type: String,
         default:'',
        trim: true
    },
    home_member_content: {
        type: String,
         default:'',
        trim: true
    },
    home_member_title: {
        type: String,
         default:'',
        trim: true
    },


    home_member_image: {
        type: String,
         default:'',
        trim: true
    },

    home_courses_title: {
        type: String,
         default:'',
        trim: true
    },

    home_courses_description: {
        type: String,
         default:'',
        trim: true
    },

    home_testimonial_title: {
        type: String,
         default:'',
        trim: true
    },

    home_user_join_title: {
        type: String,
         default:'',
        trim: true
    },

    home_user_join_sub_title: {
        type: String,
         default:'',
        trim: true
    },
    home_user_join_content: {
        type: String,
         default:'',
        trim: true
    },
    home_user_join_sub_content: {
        type: String,
         default:'',
        trim: true
    },
    home_user_join_image: {
        type: String,
         default:'',
        trim: true
    },
    

    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    },
    status          :{type:String,default:"A"},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
});
module.exports = Mongoose.model("bliss_home_content",homeContent);