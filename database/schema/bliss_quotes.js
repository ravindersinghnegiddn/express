const Mongoose = require("mongoose");

const bliss_quotes = new Mongoose.Schema({

    quote:{
        type: String,
        required: true,
        trim: true
    },
    creation_date:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_quotes", bliss_quotes);