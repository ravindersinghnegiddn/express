const Mongoose = require("mongoose");
const { ObjectId } = require('mongodb');        
const bliss_course_details = new Mongoose.Schema({

    details_id:{type: Number,trim: true},
    course_id:{type: ObjectId,required: true,trim: true},
    features:{},
    goals:{},
    what_you_will_learn:{},
    status:{type: String,required: true,default:"A"},
    creation_date:{type:String,efault:Date.now()},
    update_date:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_course_detail", bliss_course_details);