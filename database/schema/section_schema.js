
const Mongoose = require("mongoose");

const section = new Mongoose.Schema({
    course:{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_course' },
    sectionHeading: {
        type: String,
        required: true,
        trim: true
    },
    sectionNumber: {
        type: Number,
        required: true,
        trim: true
    },
    instructorId: {
        type: String,
        required: false,
        trim: true,
        default:null
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_section",section);
