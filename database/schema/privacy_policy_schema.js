const Mongoose = require("mongoose");

const privacyPolicy = new Mongoose.Schema({

    privacyAndPolicy:{
        type: String,
        required: true,
        trim: true
    },
    key :{
        type: String,
        default :'privacyPolicy'
    },
   
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_privacy_policy", privacyPolicy);