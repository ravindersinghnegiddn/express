const Mongoose = require("mongoose");

const categories = new Mongoose.Schema({
   
    name: {
        type: String,
        required: true,
        trim: true
    },
    alias: {
        type: String,
        required: true,
        trim: true
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    icon: {
        type: String,
        required: true,
        trim: true
    },
    category_image: {type: String},
    web_icon_dark: {type: String},
    web_icon_light: {type: String},
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
    
});
module.exports = Mongoose.model("categories", categories);
