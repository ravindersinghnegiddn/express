const Mongoose = require("mongoose");

const watchHistory = new Mongoose.Schema({
   
    userId: {
        type: Mongoose.Schema.Types.ObjectId,ref: 'bliss_user',
        required: true,
        trim: true
    },
    videoId: { type: Mongoose.Schema.Types.ObjectId,ref: 'bliss_course_video',
        required: true,
        trim: true}
        ,
    lastSeen:{
        type: String,
        default:'',
        trim: true
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_watch_history", watchHistory);
