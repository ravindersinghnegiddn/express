const Mongoose = require("mongoose");

const faq = new Mongoose.Schema({
    category_id:{
        type: Number,
        required: true,
        trim: true
    },
    faq_id:{
        type: Number,
        required: true,
        trim: true
    },
    question:{
        type: String,
        required: true,
        trim: true
    },
    answer :{
        type: String,
        required: true,
        trim: true
    },
    status :{
        type: String,
        required: true,
        default:"A"
    },
    is_popular :{
        type: String,
        required: true,
        default:""
    },
   
    creation_date:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_faq",faq);