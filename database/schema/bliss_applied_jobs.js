const Mongoose = require("mongoose");
        
const bliss_applied_jobs = new Mongoose.Schema({
    application_id:{type: Number,trim: true},
    job_id:{type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_job_openings' },
    first_name:{type: String,required: true,trim: true},
    last_name:{type: String,required: true,trim: true},
    email:{type: String,required: true,trim: true},
    phone:{type: String,required: true,trim: true},
    highest_education:{type: String,required: true,trim: true},
    from_year:{type: String,required: true,trim: true},
    to_year:{type: String,required: true,trim: true},
    linkedin_profile:{type: String,trim: true},
    cover_letter_link:{type: String,trim: true},
    how_do_you_hear_about_this:{type: String,trim: true},
    why_bliss:{type: String,required: true,trim: true},
    portfolio:{type: String },
    resume:{type: String },
    current_ctc:{type:String,trim:true,required: true},
    expected_ctc:{type:String,trim:true,required: true},
    notice_period:{type:String,trim:true,required: true},
    current_location:{type:String,trim:true,required: true},
    ready_to_relocate:{type:String,trim:true,required: true},
    creation_date:{type:String,efault:Date.now()},
    update_date:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_applied_job", bliss_applied_jobs);