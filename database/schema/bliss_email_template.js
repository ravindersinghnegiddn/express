const Mongoose = require("mongoose");

const bliss_email_template = new Mongoose.Schema({

    mail_type:{type: String,trim: true},
    from_email:{type: String,trim: true},
    to_email:{type: String,trim: true},
    bcc_email: {type: String},
    subject: {type: String},
    mail_header: {type: String},
    mail_body: {type: String},
    mail_footer: {type: String},
    html: {type: String},
    mail_type_data: {type: String},
    status: {type: String},

    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_email_template", bliss_email_template);