const Mongoose = require("mongoose");

const contactus = new Mongoose.Schema({

    name:{type: String,required: true,trim: true},
    mobile:{type: String,trim: true},
    email:{type: String,required: true,trim: true},
    country:{type: String,trim: true},
    message:{type: String,required: true,trim: true},
    creation_date:{type:String,default:Date.now()},
    updated_by:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_contact_u", contactus);