const Mongoose = require("mongoose");

const bliss_job_openings = new Mongoose.Schema({

    job_id:{type: Number,trim: true},
    job_title:{type: String,trim: true},
    job_slug:{type: String,trim: true},
    short_description:{type: String,trim: true},
    description:{type: String,trim: true},
    location:{type: String,trim: true},
    Pacakage:{type: String,trim: true},
    experience_required:{type: String,trim: true},
    empskills_requiredloyee_image:{type: String,trim: true},
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_job_opening", bliss_job_openings);