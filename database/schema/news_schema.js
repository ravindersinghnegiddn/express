const Mongoose = require("mongoose");

const news = new Mongoose.Schema({
   
    newsHeadline: {
        type: String,
        required: true,
        trim: true
    },
    newsDetail: {
        type: String,
        required: true,
        trim: true
    },
    newsThumbnail: {
        type: String,
        required: true,
        trim: true
    },
    newsUrl: {
        type: String,
        trim: true
    },
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: Number},
    status          :{type:String,default:"A"},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
});
module.exports = Mongoose.model("bliss_news", news);
