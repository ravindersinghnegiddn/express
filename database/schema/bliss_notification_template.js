const Mongoose = require("mongoose");

const bliss_notification_template = new Mongoose.Schema({

    process_type:{type: String,trim: true},
    content:{type: String,trim: true},

    creation_date: {type: Number},
    created_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_notification_template", bliss_notification_template);