const Mongoose = require("mongoose");

const subscriptionPayment = new Mongoose.Schema({
   
  
    userId: {
        type: Mongoose.Schema.Types.ObjectId,ref: 'bliss_user',
        required: true,
        trim: true
    },
    status: {
        type: String,
        required: true,
        trim: true,
        default:'initiated'
    },
    totalPrice: {
        type: Number,
        required: true,
        trim: true
    },
    stripePaymentId: {
        type:String,
        default :'',
        trim: true
    },
    orderNo :{
        type:String,
        default :'',
        trim: true
    },
    purchasedDate:{
        type:String,
        default:''
    },
    expiryDate:{
        type:String,
        default:''
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_subscription_payment", subscriptionPayment);
