const Mongoose = require("mongoose");

const termCondtion = new Mongoose.Schema({

    termsAndCondition:{
        type: String,
        required: true,
        trim: true
    },
    key :{
        type: String,
        default :'termCondition'
    },
   
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_term_condition", termCondtion);