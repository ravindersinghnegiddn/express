const { boolean } = require('joi')
let Mongoose = require('mongoose')

let userSchema = new Mongoose.Schema({

    email :{ 
        type:String,required:true
    },
    firstName :{ 
        type:String,required:true
    },
    lastName : { 
        type:String,
        default:''
    },
    password:{type:String,required:true},
    selectedCategory:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' }],
    selectedQuestion:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_question' }],
    token : { 
        type:String,
        default:null
    },
    profileImg:{
        type:String,
        default:''
    },
    title : { 
        type:String,
        default:''
    },
    city : { 
        type:String,
        default:''
    },
    phone_number: {
        type: String,
        default:''
    },
    address: {
        type: String,
        default:''
    },
    country: {
        type: String,
        default:''
    },
    state: {
        type: String,
        default:''
    },
    pincode: {
        type: String,
        default:''
    },
    industry : { 
        type:String,
        default:''
    },
    shortDescription : { 
        type:String,
        default:''
    },
    aboutMe : { 
        type:String,
        default:''
    },
    website :{ 
        type:String,
        default:''
    },
    is_membership_active :{ 
        type:String,
        default:'N'
    },
    facebook : { 
        type:String,
        default:''
    },
    twitter :{ 
        type:String,
        default:''
    },
    linkdIn : { 
        type:String,
        default:''
    },
    dob :{ 
        type:String,
        default:''
    },
    isBlocked:{
        type:Boolean,
        default:0
    },
    isActive:{
        type:Boolean,
        default:1
    },
    deviceIp:{
        type:String,
        default:''
    },
    deviceToken:{
        type:String,
        default:''
    },
    deviceType:{
        type:String,
        default:''
    },
    lat :{
        type:Number,
        default:''
    },
    long:{
        type:Number,
        default:''
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }

})

module.exports = Mongoose.model('bliss_user',userSchema)