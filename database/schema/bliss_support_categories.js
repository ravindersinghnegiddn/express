const Mongoose = require("mongoose");

const bliss_support_categories = new Mongoose.Schema({
    category_id: {
        type: Number,
        required: true,
        trim: true
    },
    category_name: {
        type: String,
        required: true,
        trim: true
    },
    image: {
        type: String,
        required: true,
        trim: true
    },
    creation_date: {
        type: String,
        required: true,
        trim: true
    },
 
    update_date: {
        type:String,
        default:Date.now()
    },

    status: {
        type: String,
         default:'A',
        trim: true
    },
   
});
module.exports = Mongoose.model("bliss_support_categorie",bliss_support_categories);