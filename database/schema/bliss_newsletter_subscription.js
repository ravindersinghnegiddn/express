const Mongoose = require("mongoose");

const bliss_newsletter_subscription = new Mongoose.Schema({

    email:{type: String,required: true,trim: true},
    creation_date:{type:String,default:Date.now()},
});
module.exports = Mongoose.model("bliss_newsletter_subscription", bliss_newsletter_subscription);