const Mongoose = require("mongoose");

const event = new Mongoose.Schema({
   
    name: {
        type: String,
        trim: true
    },
    alias: {
        type: String,
        trim: true
    },
    eventDescription: {
        type: String,
        trim: true
    },
    icon: {
        type: String,
        trim: true
    },
    eventStatus: {
        type: String,
        trim: true,
        default: 'pending'
    },
    isAdmin: {
        type: String,
        default:"N",
    },
    instructorId: {
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_instructor' ,
        default:"",
    },
    eventDateTime:{
        type:Number,
        default:""
    },
    eventOrder:{
        type:Number,
        default:1
    },
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: String},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: String},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_event",event);
