const Mongoose = require("mongoose");

const cms = new Mongoose.Schema({

    data:{
        type: String,
        required: true,
        trim: true
    },
    key :{
        type: String,
        default :'key'
    },
   
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_cms", cms);