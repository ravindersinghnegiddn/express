const Mongoose = require("mongoose");

const courseVideo = new Mongoose.Schema({
   
    videoUrl: {
        type: String,
        trim: true
    },
    videoName: {
        type: String,
        default:'',
        trim: true
    },
    videoDescription: {
        type: String,
        default:'',
        trim: true
    },
    instructorId:{
        type: String,
        default:null,
        trim: true
    },
    sectionId:{
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_section' ,
        default:'',
        trim: true
    },
    episodeNumber:{
        type: Number,
        default:'',
        trim: true
    },
    videoId: {
        type: String,
        trim: true
    },
    videoDuration: {
        type: String,
        trim: true
    },
    courseId :{
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_course' ,
        trim: true
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_course_video", courseVideo);
