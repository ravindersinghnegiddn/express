const Mongoose = require("mongoose");

const userQuestion= new Mongoose.Schema({
    //categories:[Mongoose.Schema.Types.ObjectId],
    user:{
        type:Mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'bliss_users'
      },

    questions:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_questions',required:true }],

  
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_user_question", userQuestion);