const Mongoose = require("mongoose");

const bliss_retreats = new Mongoose.Schema({
    retreat_id: {
        type: Number,
        required: true,
        trim: true
    },
    retreat_type: {
        type: String,
        required: true,
        trim: true
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    sub_title: {
        type: String,
        required: true,
        trim: true
    },
    title_slug: {
        type: String,
        required: true,
        trim: true
    },
    description_title: {type: String,trim: true},
    description_sub_title: {type: String,trim: true},
    description: {type: String,trim: true},
    why_to_join_title: {type: String,trim: true},
    why_to_join_description: {type: String,trim: true},
    level_1: {type: String,trim: true},
    level_2: {type: String,trim: true},
    level_3: {type: String,trim: true},
    image: {type: String,required: true,trim: true},
    creation_date: {type: String,default:Date.now(),},
    benifits:{ type: Array , "default" : [] },
 
    update_date: {
        type:String,
        default:Date.now()
    },

    status: {
        type: String,
         default:'A',
        trim: true
    },
   
});
module.exports = Mongoose.model("bliss_retreat",bliss_retreats);