const { boolean } = require('joi')
let Mongoose = require('mongoose')

let instructor = new Mongoose.Schema({

    email :{ 
        type:String,required:true
    },

    name :{ 
        type:String,required:true
    },

    experience :{ 
        type:String,
        default:''
    },
    last_name : { 
        type:String,
        default:''
    },
    password:{type:String,required:true},
    token : { 
        type:String,
        default:null
    },
    selectedCategory:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' }],
    profileImg:{
        type:String,
        default:''
    },
    tagline : { 
        type:String,
        default:''
    },
    language : { 
        type:String,
        default:'Hindi,English'
    },
    workedAt : { 
        type:String,
        default:'BlissGenX'
    },
    studiedAt : { 
        type:String,
        default:'BlissGenX'
    },
    premium_member :{
        type:String,
        default:"N"
    },
    city : { 
        type:String,
        default:''
    },
    country :{ 
        type:String,
        default:''
    },
    industry : { 
        type:String,
        default:''
    },
    phone_number: {
        type: String,
        default:''
    },
    shortDescription : { 
        type:String,
        default:''
    },
    aboutMe : { 
        type:String,
        default:''
    },
    website :{ 
        type:String,
        default:''
    },
    facebook : { 
        type:String,
        default:''
    },
    twitter :{ 
        type:String,
        default:''
    },
    linkedin : { 
        type:String,
        default:''
    },
    awards : { 
        type:String,
        default:''
    },
    dob :{ 
        type:String,
        default:''
    },
    isBlocked:{
        type:Boolean,
        default:0
    },
    isActive:{
        type:Boolean,
        default:1
    },
    deviceIp:{
        type:String,
        default:''
    },
    deviceToken:{
        type:String,
        default:''
    },
    deviceType:{
        type:String,
        default:''
    },
    status:{
        type:String,
        default:'I'
    },
    lat :{
        type:Number,
        default:''
    },
    long:{
        type:Number,
        default:''
    },
    joinedAt:{
        type:String,
        default:Date.now()
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
})

module.exports = Mongoose.model('bliss_instructor',instructor)