const Mongoose = require("mongoose");
        
const bliss_terms_policies = new Mongoose.Schema({
    title_en:{type: String,required: true,trim: true},
    description_en:{type: String,required: true,trim: true},
    policy_type:{type: String,trim: true},
    seo_title:{type: String,required: true,trim: true},
    seo_keyword:{type: String,required: true,trim: true},
    seo_description:{type: String,required: true,trim: true},
    creation_date:{type:String,default:Date.now()},
    update_date:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_terms_policie", bliss_terms_policies);