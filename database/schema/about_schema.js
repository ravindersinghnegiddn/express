const Mongoose = require("mongoose");

const about = new Mongoose.Schema({

    aboutUs:{
        type: String,
        required: true,
        trim: true
    },
   
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_about_Us", about);