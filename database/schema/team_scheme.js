const Mongoose = require("mongoose");

const teamScheme = new Mongoose.Schema({

    name:{type: String,required: true,trim: true},
    profile:{type: String,required: true,trim: true},
    image:{type: String,required: true,trim: true},
    designation:{type: String,trim: true},
    about:{type: String,required: true,trim: true},
    profile_with_name:{type: String,required: true,trim: true},
    email_1:{type: String,trim: true},
    about_2:{type: String,trim: true},
    email_2:{type: String,trim: true},
    createdAt:{type:String,efault:Date.now()},
    updatedAt:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_team", teamScheme);