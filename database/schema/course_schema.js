const Mongoose = require("mongoose");

const course = new Mongoose.Schema({
    //categories:[Mongoose.Schema.Types.ObjectId],
    categories:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' }],
    courseName: {
        type: String,
        trim: true
    },
    alias: {
        type: String,
        trim: true
    },
    courseDescription: {
        type: String,
        trim: true
    },
    courseTagLine: {
        type: String,
        trim: true
    },
    courseGoal: {
        type: String,
        trim: true
    },
    courseThumbnail: {
        type: String,
        trim: true
    },
    coursePrice: {
        type: String,
        trim: true
    },
    courseAuthor: {
        type: String,
        trim: true
    },
    aboutAuthor:{
        type: String,
        trim: true
    },
    authorImage:{
        type: String,
        trim: true
    },
    isAdmin: {
        type: Boolean,
        default:true,
    },
    instructorId: {
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_instructor' ,
        default:"6018ded998ec8305827afd97",
        required:false
    },
    startDate: {
        type: String,
        default:"",
        trim: true
    },
    duration: {
        type: String,
        trim: true
    },
    course_details_updated:{
        type:String,
        default:"N"
    },
    eventDateTime:{
        type:Number,
        default:""
    },
    eventStatus: {
        type: String,
        trim: true,
        default: 'pending'
    },
    eventOrder:{
        type:Number,
        default:1
    },
    durationType: {
        type: String,
        default:'',
        trim: true
    },
    folderId: {
        type: String,
         default:'',
        trim: true
    },
    course_type:{type: String},
    added_by:{type: String},
    enrolledCount: { type: Number, default: 0 },
    videoId: {
        type: String,
         default:'',
        trim: true
    },
    videoLink:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_video' }],
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
    
});
module.exports = Mongoose.model("bliss_course", course);