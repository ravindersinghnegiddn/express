const Mongoose = require("mongoose");

const courseCategory = new Mongoose.Schema({
    categoryId:{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' },
    courseId:{type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_course'},
    
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_course_category", courseCategory);
