const Mongoose = require("mongoose");

const bliss_seo = new Mongoose.Schema({

    page_name:{type: String,trim: true},
    page_slug:{type: String,trim: true},
    seo_title:{type: String,trim: true},
    seo_keyword: {type: String},
    seo_description: {type: String},

    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_seo", bliss_seo);