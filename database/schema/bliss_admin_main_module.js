const Mongoose = require("mongoose");
        
const bliss_admin_main_module = new Mongoose.Schema({
    main_module_id:{type: Number,trim: true},
    main_module_name:{type: String,trim: true},
    main_module_display_name:{type: String,trim: true},
    module_orders:{type: Number,trim: true},
    module_icone:{type: String,trim: true},
});
module.exports = Mongoose.model("bliss_admin_main_module", bliss_admin_main_module);