const Mongoose = require("mongoose");

const bliss_certificates = new Mongoose.Schema({

    certificate_name:{type: String,trim: true},
    provider_name:{type: String,trim: true},
    completion_year:{type: String,trim: true},
    instructor_id:{type: String,trim: true},
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: String},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: String},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_certificates", bliss_certificates);