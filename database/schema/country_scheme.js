const Mongoose = require("mongoose");

const bliss_country = new Mongoose.Schema({

    country_name:{type: String,required: true,trim: true},
    country_slug:{type: String,trim: true},
    country_id:{type: String,trim: true},
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_countrie", bliss_country);