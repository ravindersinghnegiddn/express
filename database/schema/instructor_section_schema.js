const Mongoose = require("mongoose");

const instructorSection = new Mongoose.Schema({
    course:{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_course' },
    instructorId:{ type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_instructor' },
    sectionHeading: {
        type: String,
        required: true,
        trim: true
    },
    sectionNumber: {
        type: Number,
        required: true,
        trim: true
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_instructor_section",instructorSection);
