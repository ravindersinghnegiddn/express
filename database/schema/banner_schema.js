const Mongoose = require("mongoose");

const banner = new Mongoose.Schema({
title_1         :{type:String},
sub_title_1     :{type:String},
title_2         :{type:String},
sub_title_2     :{type:String},
banner_type     :{type:String},
page_type:{type:String},
page_url        :{type:String},
video_url       :{type:String},
banner_image    :{type:String},
creation_ip     :{type:String},
creation_date   :{type:String},
created_by      :{type:String},
status          :{type:String,default:"A"},
update_ip: {type: String},
update_date: {type: Number},
updated_by: {type: Number},
});
module.exports = Mongoose.model("bliss_banner",banner);