const Mongoose = require("mongoose");

const bliss_usp = new Mongoose.Schema({

    title:{type: String,required: true,trim: true},
    image:{type: String,required: true,trim: true},
    created_by:{type:String,efault:Date.now()},
    updated_by:{type:String,default:Date.now()},
    status:{type:String,required: true,trim: true}
});
module.exports = Mongoose.model("bliss_usp", bliss_usp);