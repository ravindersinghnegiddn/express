let mongoose = require('mongoose')

let adminSchema = new mongoose.Schema({
    admin_id : { type:Number},                                                                                                                                                                         
    admin_title : { type:String},                                                                                                                                                           
    admin_first_name : { type:String},                                                                                                                                                           
    admin_middle_name : { type:String},                                                                                                                                                
    admin_last_name : { type:String},                                                                                                                                                     
    admin_email : { type:String},                                                                                                                                                                   
    admin_phone : { type:Number},                                                                                                                                                                    
    admin_password : { type:String},                                                                                                                                                                                                                
    admin_password_otp : { type:String},                                                                                                                                                
    admin_image : { type:String},                                                                                                                                                
    admin_address : { type:String},                                                                                                                                                              
    admin_city : { type:String},                                                                                                                                                     
    admin_state : { type:String},                                                                                                                                                  
    admin_country : { type:String},                                                                                                                                                     
    admin_pincode : { type:String},                                                                                                                                                    
    department_id : { type:String},                                                                                                                                                 
    department_name : { type:String},                                                                                                                                                
    designation_id : { type:String},                                                                                                                                                 
    designation_name : { type:String},                                                                                                                                                
    admin_type : { type:String},                                                                                                                                                           
    last_login_ip : { type:String},                                                                                                                                                             
    last_login_date : { type:String},                                                                                                                                                        
    creation_ip : { type:String},                                                                                                                                                         
    creation_date : { type:String},                                                                                                                                                          
    created_by : { type:String},                                                                                                                                                                         
    update_ip : { type:String},                                                                                                                                                             
    update_date : { type:String},                                                                                                                                                        
    updated_by : { type:String},                                                                                                                                                                         
    status : { type:String},
    
}, 
 {  timestamps: true
})

module.exports = mongoose.model('admin',adminSchema)