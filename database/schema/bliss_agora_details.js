const Mongoose = require("mongoose");

const agora = new Mongoose.Schema({
channel_name         :{type:String},
token     :{type:String},
course_id     :{type:String},
instructor_id     :{type:String},
creation_date   :{type:Number},
});
module.exports = Mongoose.model("bliss_agora_details",agora);