const Mongoose = require("mongoose");

const video = new Mongoose.Schema({
   
    videoUrl: {
        type: String,
        required: true,
        trim: true
    },
    videoId: {
        type: String,
        default:'',
        trim: true
    },
   
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_video", video);
