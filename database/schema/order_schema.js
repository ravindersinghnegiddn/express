const Mongoose = require("mongoose");

const order = new Mongoose.Schema({
   
    orderNo: {
        type: String,
        required: true,
        trim: true
    },
    userId: {
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_user' ,
        required: true,
        trim: true
    },
    courseId: {
        type: Mongoose.Schema.Types.ObjectId, ref: 'bliss_course' ,
        required: true,
        trim: true
    },
    totalPrice: {
        type: Number,
        required: true,
        trim: true
    },
    status: {
        type: String,
        required: true,
        trim: true,
        default:'initiated'
    },

    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_order", order);
