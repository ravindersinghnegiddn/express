const Mongoose = require("mongoose");

const corporateCms = new Mongoose.Schema({

    title:{type: String,required: true,trim: true},
    sub_title:{type: String,required: true,trim: true},
    description:{type: String,required: true,trim: true},
    image:{type: String,required: true,trim: true},
    our_mission_title:{type: String,required: true,trim: true},
    our_mission_sub_title:{type: String,required: true,trim: true},
    our_mission_description:{type: String,required: true,trim: true},
    our_mission_image:{type: String,required: true,trim: true},
    our_usp_title:{type: String,required: true,trim: true},
    our_usp_sub_title:{type: String,required: true,trim: true},
    mental_issue_title:{type: String,required: true,trim: true},
    mental_issue_sub_title:{type: String,required: true,trim: true},
    solutions_title:{type: String,required: true,trim: true},
    news_title:{type: String,required: true,trim: true},
    news_sub_title:{type: String,required: true,trim: true},
    clients_title:{type: String,required: true,trim: true},
    corporate_cate_title:{type: String,trim: true},
    corporate_cate_sub_title:{type: String,required: true,trim: true},
    wellness_title:{type: String,required: true,trim: true},
    wellness_sub_title:{type: String,required: true,trim: true},
    created_by:{type:String,efault:Date.now()},
    updated_by:{type:String,default:Date.now()},
    status:{type:String,required: true,trim: true},
    usp:{},
});
module.exports = Mongoose.model("bliss_corporate_cm", corporateCms);