const Mongoose = require("mongoose");
        
const bliss_membership_details = new Mongoose.Schema({
    membership_id:{type: Number,trim: true},
    membership_type:{type: String,required: true,trim: true},
    what_we_get:{type: Array , "default" : []},
    how_it_works:{type: Array , "default" : []},
    status:{type: String,required: true,default:"A"},
    creation_date:{type:String,efault:Date.now()},
    update_date:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_membership_detail", bliss_membership_details);