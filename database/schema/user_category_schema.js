const Mongoose = require("mongoose");

const userCategory = new Mongoose.Schema({
    //categories:[Mongoose.Schema.Types.ObjectId],
    user:{
        type:Mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'bliss_users'
      },

    categories:[{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories',required:true }],

  
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_user_category", userCategory);