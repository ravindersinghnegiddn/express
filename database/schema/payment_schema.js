const Mongoose = require("mongoose");

const payment = new Mongoose.Schema({
   
    order_id: {
        type: String ,
        trim: true
    },
    user_id: {
        type: String,
        trim: true
    },
    instructor_id: {
        type: String,
        trim: true
    },
    first_name: {
        type: String,
        trim: true
    },
    last_name: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true
    },
    phone_number: {
        type: String,
        trim: true
    },
    address: {
        type: String,
        trim: true
    },
    country: {
        type: String,
        trim: true
    },
    state: {
        type: String,
        trim: true
    },
    pincode: {
        type: String,
        trim: true
    },
    purchase_type: {
        type: String,
        trim: true
    },
    course_id: {
        type: String,
        ref: 'bliss_course',
        
        trim: true
    },
    course_price: {
        type: Number,
        trim: true
    },
    membership_price:{
        type:Number,
        trim:true
    },
    tax: {
        type: String,
        trim: true
    },
    status: {
        type: String,
        trim: true,
        default:'initiated'
    },
    total_price: {
        type: Number,
        trim: true
    },
    amount_in_choosed_currency: {
        type: Number,
        trim: true
    },
    currency: {
        type: String,
        trim: true
    },
    stripeToken: {
        type:String,
        default :'',
    },
    customerId: {
        type:String,
        default :'',
    },
    captureAmount: {
        type:String,
        default :'',
    },
    stripeChargeId: {
        type:String,
        default :'',
    },

    creation_date:{
        type:String,
        default:""
    },
    update_date:{
        type:String,
        default:""
    }
});
module.exports = Mongoose.model("bliss_payment", payment);
