const Mongoose = require("mongoose");

const agora = new Mongoose.Schema({
channel_name         :{type:String},
course_id     :{type:String},
user_id     :{type:String},
user_name         :{type:String},
message         :{type:String},
});
module.exports = Mongoose.model("bliss_live_chat",agora);