const Mongoose = require("mongoose");
        
const bliss_admin_modules = new Mongoose.Schema({
    module_id:{type: Number,trim: true},
    module_name:{type: String,trim: true},
    module_display_name:{type: String,trim: true},
    module_orders:{type: Number,trim: true},
    module_icone:{type: String,trim: true},
    main_module_id:{type: Number,trim: true},
    child_data:{type:Array}
});
module.exports = Mongoose.model("bliss_admin_module", bliss_admin_modules);