const Mongoose = require("mongoose");

const instructorCategories = new Mongoose.Schema({

    instructorId: {
        type: String,
        required: true,
        trim: true
    },
   
    name: {
        type: String,
        required: true,
        trim: true
    },
    alias: {
        type: String,
        required: true,
        trim: true
    },
    title: {
        type: String,
        required: true,
        trim: true
    },
    icon: {
        type: String,
        required: true,
        trim: true
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_instructor_categories", instructorCategories);
