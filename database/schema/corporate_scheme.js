const Mongoose = require("mongoose");

const corporateBanner = new Mongoose.Schema({

    title:{type: String,required: true,trim: true},
    description:{type: String,trim: true},
    image:{type: String,required: true,trim: true},
    url:{type: String,trim: true},
    created_by:{type:String,efault:Date.now()},
    updated_by:{type:String,default:Date.now()},
    status:{type:String,required: true,trim: true}
});
module.exports = Mongoose.model("bliss_corporate_banner", corporateBanner);