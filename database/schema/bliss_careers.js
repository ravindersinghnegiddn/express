const Mongoose = require("mongoose");

const bliss_careers = new Mongoose.Schema({

    career_id:{type: Number,required: true,trim: true},
    mission_title:{type: String,required: true,trim: true},
    mission_sub_title:{type: String,required: true,trim: true},
    mission_description:{type: String,required: true,trim: true},
    mission_image:{type: String,required: true,trim: true},
    employee_title:{type: String,required: true,trim: true},
    employee_description:{type: String,required: true,trim: true},
    employee_image:{type: String,required: true,trim: true},
    vision_title:{type: String,required: true,trim: true},
    vision_sub_title:{type: String,required: true,trim: true},
    vision_description:{type: String,required: true,trim: true},
    vision_image:{type: String,required: true,trim: true},
    why_work_at_bliss:{},
    status:{type:String,trim:true,default:"A"},
    creation_date:{type:String,efault:Date.now()},
    update_date:{type:String,default:Date.now()}
});
module.exports = Mongoose.model("bliss_career", bliss_careers);