const Mongoose = require("mongoose");

const bliss_podcasts = new Mongoose.Schema({

    podcast_id:{type: Number,trim: true},
    podcast_title:{type: String,required: true,trim: true},
    podcast_sub_title:{type: String,required: true,trim: true},
    description:{type: String,required: true,trim: true},
    speaker_name:{type: String,required: true,trim: true},
    podcast_audio:{type: String,required: true,trim: true},
    podcast_thumbnail:{type: String,required: true,trim: true},
    podcast_banner:{type: String,required: true,trim: true},
    status:{type:String,trim:true,default:"A"},
    show_banner:{type:String,trim:true,default:"N"},
    is_popular:{type:String,trim:true,default:"Y"},
    creation_date:{type:Number,efault:Date.now()},
    update_date:{type:Number,default:Date.now()}
});
module.exports = Mongoose.model("bliss_podcast", bliss_podcasts);