const Mongoose = require("mongoose");

const subscription = new Mongoose.Schema({
   
    subscriptionType: {
        type: String,
        required: true,
        trim: true
    },
    subscriptionDetail: {
        type: String,
        required: true,
        trim: true,
    },
    subscriptionPrice: {
        type: Number,
        required: true,
        trim: true
    },
    createdAt:{
        type:String,
        default:Date.now()
    },
    updatedAt:{
        type:String,
        default:Date.now()
    }
});
module.exports = Mongoose.model("bliss_subscription", subscription);
