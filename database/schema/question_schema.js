const Mongoose = require("mongoose");

const question = new Mongoose.Schema({
    category:{ type: Mongoose.Schema.Types.ObjectId, ref: 'categories' },
    question: {
        type: String,
        required: true,
        trim: true
    },
    creation_ip: {type: String},
    creation_date: {type: Number},
    created_by: {type: Number},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
    status: {type: String,default:"A"},
});
module.exports = Mongoose.model("bliss_question", question);
