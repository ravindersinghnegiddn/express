const Mongoose = require("mongoose");

const aboutWeb = new Mongoose.Schema({

    title:{type: String,trim: true},
    description:{type: String,trim: true},
    image:{type: String,trim: true},
    about_success_title:{type: String,trim: true},
    student_world_wide_title:{type: String,trim: true},
    student_world_wide_count:{type: String,trim: true},
    instructor_title:{type: String,trim: true},
    instructor_count:{type: String,trim: true},
    events_title:{type: String,trim: true},
    events_count:{type: String,trim: true},
    session_title:{type: String,trim: true},
    session_count:{type: String,trim: true},
    about_success_video_url:{type: String,trim: true},
    join_bliss_title:{type: String,trim: true},
    join_bliss_description:{type: String,trim: true},
    join_bliss_sub_title:{type: String,trim: true},
    join_bliss_sub_description:{type: String,trim: true},
    join_bliss_image:{type: String,trim: true},
    provide_title:{type: String,trim: true},
    provide_description:{type: String,trim: true},
    provide_image:{type: String,trim: true},
    home_about_founder_title:{type: String,trim: true},
    home_about_founder_description:{type: String,trim: true},
    home_about_founder_image:{type: String,trim: true},
    status          :{type:String,default:"A"},
    update_ip: {type: String},
    update_date: {type: Number},
    updated_by: {type: Number},
});
module.exports = Mongoose.model("bliss_about_us_web", aboutWeb);