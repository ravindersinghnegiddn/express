const Mongoose = require("mongoose");

const generalData = new Mongoose.Schema({

    logo_dark:{type: String,trim: true},
    logo_white:{type: String,trim: true},
    facebook:{type: String,trim: true},
    twitter:{type: String,trim: true},
    linkedin:{type: String,trim: true},
    instagram:{type: String,trim: true},
    youtube:{type: String,trim: true},
    membership:{type: Number,trim: true},
    premium:{type: Number,trim: true},
});
module.exports = Mongoose.model("bliss_general_data", generalData);