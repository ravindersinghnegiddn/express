const express = require('express');
const app = express()
const router = express.Router()
const userController = require('../controller/userController/userController')
const userValidation = require('../validations/userValidation/userValidation')
const userMiddleware = require('../middlewares/userMiddleware/UserMiddleware')

const paymentController = require('../controller/userController/paymentController')
const paymentValidation = require('../validations/userValidation/paymentValidation')

const videoController = require('../controller/userController/videoController')

const lastSeenValidation = require('../validations/userValidation/lastSeenValidation')


const testController = require('../controller/userController/testController')

const webCmsController = require('../controller/userController/webCmsController');
const coroporateController = require('../controller/userController/coroporateController');
const countryController = require('../controller/userController/countryController');
const instructorController = require('../controller/instructorController/instructorController');
const careersController = require('../controller/userController/careersController');
const courseController = require('../controller/instructorController/courseController');
const eventPaymentController = require('../controller/userController/eventController');
const agoraController = require('../controller/userController/agora_controller');

//***************************************************************************************************************************************************************************/
router.get('/categoryList',userController.getCategoryList)
router.get('/questionList',userController.getQuestionList)
router.post('/questionList',userController.getQuestionList2)

router.post('/signup',userValidation.userSignupValidation,userValidation.userValidation,userController.userSignup1)
router.post('/login',userValidation.isEmailExist,userController.login)
router.get('/getProfile',userMiddleware.check_login,userValidation.userGetProfileValidation,userController.getProfile)
router.post('/updateProfile',userMiddleware.check_login,userValidation.userUpdateProfileValidation,userController.updateProfile)
router.put('/putChangePassword',userMiddleware.check_login,userValidation.passwordValidation,userController.putChangePassword)

//About api
router.get('/about',userMiddleware.check_login,userController.getAboutUs) 

//Web About api
router.get('/webAbout',testController.webAbout) 

router.post('/homePage',userMiddleware.check_login,userController.homePage)
router.post('/allCourse',userController.courseList)
router.post('/paidMasterClass',userController.paidMasterClass)
router.post('/paidMasterClassDetails',userController.paidMasterClassDetails)
router.post('/freeMasterClass',userController.freeMasterClass)
router.post('/freeMasterClassDetails',userController.freeMasterClassDetails)
router.post('/purchasedCourse',userMiddleware.check_login,userController.purchasedCourseList)
router.post('/viewCourse',userMiddleware.check_login,userController.viewCourse)

//Purchased course video list
router.post('/purchasedCourseVideo',userMiddleware.check_login,videoController.viewPurchasedCourseVideo1)
router.post('/purchasedCourseVideoDetail',userMiddleware.check_login,videoController.viewPurchasedCourseVideoDetails)


//Update watch history of video
router.post('/updateVideoLastSeen',userMiddleware.check_login,lastSeenValidation.lastSeenValidation,videoController.updateVideoLastSeen)

//Order apis
router.post('/createOrder',userMiddleware.check_login,paymentValidation.createOrderValidation,paymentController.createOrder);

//Program payment apis
router.post('/create-payment-intent',userMiddleware.check_login,paymentValidation.OrderPaymentInitValidation,paymentController.paymentInit);
router.post('/final-payment',userMiddleware.check_login,paymentValidation.OrderPaymentFinalValidation,paymentController.paymentFinal);


//Subscription payment apis
router.post('/subscription-payment-intent',userMiddleware.check_login,paymentValidation.subscriptionPaymentInitValidation,paymentController.subscriptionPaymentInit);
router.post('/subscription-payment-final',userMiddleware.check_login,paymentValidation.subscriptionPaymentFinalValidation,paymentController.subscriptionPaymentFinal);

//Check subscription
router.get('/subscriptionStatus',userMiddleware.check_login,paymentController.subscriptionStatus);
router.get('/subscriptionDetail',userMiddleware.check_login,paymentController.subscriptionDetail);

//News APIS
router.get('/allNews',userController.newsList)
router.post('/viewNews',userController.viewNews)

//Instructor related API
router.post('/listInstructor',userController.listInstructor)
router.post('/listInstructorCourses',userMiddleware.check_login,userController.InstructorCourseList)

//Admin Event related API
router.post('/listEvent',userController.listEvent)
router.post('/viewEvent',userMiddleware.check_login,userController.viewEvent)

//Instructor Event related API
router.post('/listEventInstructor',userController.listEventInstructor)
router.post('/viewEventInstructor',userMiddleware.check_login,userController.viewEventInstructor)



//About api web
router.get('/getAboutUsWeb',webCmsController.getAboutUsWeb) 

//Team api web
router.get('/getTeamData',webCmsController.getTeamData) 

//corporate page api
router.get('/getCorporatePageData',coroporateController.getCorporatePageData)

//corporate page api
router.post('/putContactUsData',webCmsController.putContactUsData)

//country api
router.get('/getCountryData',countryController.getCountryData)

//instuctorsessions api
router.get('/getInstuctorSessions',instructorController.getInstuctorSessions)

//homepage api
router.get('/getHomePageData',webCmsController.getHomePageData)
router.get('/getSupportCategories',webCmsController.getSupportCategories)
router.post('/getSupportFaq',webCmsController.getSupportFaq)
router.post('/getRetreats',webCmsController.getRetreats)
router.post('/getCareersAbout',careersController.getCareersAbout);
router.post('/getPodcasts',webCmsController.getPodcasts);
router.post('/getJobOpeningDetails',careersController.getJobOpeningDetails);
router.post('/putJobForm',careersController.putJobForm);
router.post('/getTermsPolicy',webCmsController.getTermsPolicy);
router.post('/getPurchaseHistory',userController.getPurchaseHistory);
router.post('/getDataBySearch',courseController.getDataBySearch);
router.post('/getDailyQuotes',webCmsController.getDailyQuotes);
router.post('/getPageBannerData',webCmsController.getPageBannerData);
router.post('/getPageSEOData',webCmsController.getPageSEOData);
router.post('/setNewsLetterSubscription',webCmsController.setNewsLetterSubscription);


router.post('/setAgoraDetails',agoraController.setAgoraDetails);
router.post('/getAgoraDetails',agoraController.getAgoraDetails);
router.post('/updateEventStatus',agoraController.updateEventStatus);
router.post('/sendMessage',agoraController.sendMessage);
router.post('/getChatData',agoraController.getChatData);

//premium payment apis
router.post('/initiate-event-payment',userMiddleware.check_login,eventPaymentController.paymentInit);
router.post('/complete-event-payment',userMiddleware.check_login,eventPaymentController.paymentFinal);
//***************************************************************************************************************************************************************************/
module.exports = router