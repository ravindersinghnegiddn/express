const express = require('express');
const app = express()
const router = express.Router()
//***********************************************************************************************/

const instructorController = require('../controller/instructorController/instructorController')            
const instructorValidation = require('../validations/instructorValidation/instructorValidation')
const instructorsMiddleware = require('../middlewares/instructorMiddleware/instructorMiddleware')

const categoryController = require('../controller/instructorController/categoryController')

const courseController = require('../controller/instructorController/courseController')            
const courseValidation = require('../validations/instructorValidation/courseValidation')
const courseMiddlewares = require('../middlewares/instructorMiddleware/courseMiddleware')

const sectionController = require('../controller/instructorController/sectionController')
const sectionValidation = require('../validations/instructorValidation/sectionValidation')
const sectionMiddleware = require('../middlewares/instructorMiddleware/sectionMiddleware')

const videoController = require('../controller/instructorController/videoController')
const videoValidation = require('../validations/instructorValidation/videoValidation')
const videoMiddleware = require('../middlewares/instructorMiddleware/instructorVideoMiddleware')

const premiumController = require('../controller/instructorController/premiumController')

const eventController = require('../controller/instructorController/eventController')
const eventValidation = require('../validations/instructorValidation/eventValidation')
const eventMiddleware = require('../middlewares/instructorMiddleware/eventMiddleware')
//***********************************************************************************************/




//***********************************************************************************************/
//************************DropDown API************************//
router.get('/listCategories',instructorsMiddleware.check_login,categoryController.listCategory)
router.get('/listCourse',instructorsMiddleware.check_login,courseController.listCourse)
router.get('/listSection',instructorsMiddleware.check_login,sectionController.listSection)


//************************Instructor basic API************************//
router.get('/listCategory',categoryController.listCategoryDropdown)
router.post('/signup',instructorValidation.instructorSignupValidation,instructorValidation.instructorValidation,instructorController.instructorSignup)
router.post('/login',instructorValidation.instructorLoginValidation,instructorValidation.isEmailExist,instructorController.instructorlogin)
router.get('/getProfile',instructorsMiddleware.check_login,instructorValidation.instructorGetProfileValidation,instructorController.getProfile)
router.post('/updateProfile',instructorsMiddleware.check_login,instructorValidation.instructorUpdateProfileValidation,instructorController.updateProfile)
router.post('/addInstructorRegistrationForm',instructorValidation.instructorJoinFormValidation,instructorValidation.instructorValidation,instructorController.addInstructorRegistrationForm)

//************************admin Dashboard api************************************//
router.get('/dashboard',instructorsMiddleware.check_login,instructorController.dashboard)

//************************Instructor Category  API************************//
router.post('/addCategories',instructorsMiddleware.check_login,instructorsMiddleware.check_category_name,categoryController.addCategory)
router.post('/updateCategories',instructorsMiddleware.check_login,categoryController.updateCategory)
router.get('/viewCategories',instructorsMiddleware.check_login,categoryController.viewCategory)
router.post('/listCategories',instructorsMiddleware.check_login,categoryController.listCategory1)
router.post('/deleteCategories',instructorsMiddleware.check_login,categoryController.deleteCategory)

//************************course API************************//
router.post('/addCourse',instructorsMiddleware.check_login,courseValidation.addCourseValidation,courseMiddlewares.check_course_name,courseController.addCourse1)
router.get('/viewCourse',instructorsMiddleware.check_login,courseValidation.viewCourseValidation,courseController.viewCourse)
router.post('/updateCourse',instructorsMiddleware.check_login,courseValidation.updateCourseValidation,courseMiddlewares.check_course_name_update,courseController.updateCourse1)
router.post('/listCourse',instructorsMiddleware.check_login,courseController.listCourse1)
router.post('/deleteCourse',instructorsMiddleware.check_login,courseController.deleteCourse)
router.post('/addCourseDetails',instructorsMiddleware.check_login,courseController.addCourseDetails)
router.post('/updateCourseDetails',instructorsMiddleware.check_login,courseController.updateCourseDetails)

//************************section API**********************//
router.post('/addSection',instructorsMiddleware.check_login,sectionValidation.addSectionValidation,sectionMiddleware.checkSectionNumber,sectionController.addSection)
router.post('/listSection',instructorsMiddleware.check_login,sectionController.listSection1)
router.post('/updateSection',instructorsMiddleware.check_login,sectionValidation.updateSectionValidation,sectionMiddleware.checkSectionNumberUpdate,sectionController.updateSection)
router.get('/viewSection',instructorsMiddleware.check_login,sectionValidation.viewSectionValidation,sectionController.viewSection)


//************************video API************************//
router.post('/addVideo',instructorsMiddleware.check_login,videoValidation.addVideoValidation,videoMiddleware.checkEpisodeNumber,videoController.addVideo)
router.post('/listVideo',instructorsMiddleware.check_login,videoController.listVideo1)
router.get('/viewVideo',instructorsMiddleware.check_login,videoValidation.viewVideoValidation,videoController.viewVideo)
router.post('/updateVideo',instructorsMiddleware.check_login,videoValidation.updateVideoValidation,videoMiddleware.checkEpisodeNumberUpdate,videoController.updateVideo)
router.post('/deleteVideo',instructorsMiddleware.check_login,videoController.deleteVideo)

//************************course Video List************************//
router.post('/listCourseVideo',instructorsMiddleware.check_login,videoController.listCourseVideo)

//************************Live Event API************************//
router.post('/addEvent',instructorsMiddleware.check_login,eventValidation.addEventValidation,eventMiddleware.check_event_name,eventController.addEvent)
router.post('/updateEvent',instructorsMiddleware.check_login,eventValidation.updateEventValidation,eventMiddleware.check_event_name_update,eventController.updateEvent)
router.get('/viewEvent',instructorsMiddleware.check_login,eventController.viewEvent)
router.post('/listEvent',instructorsMiddleware.check_login,eventController.listEvent1)
router.post('/deleteEvent',instructorsMiddleware.check_login,eventController.deleteEvent)
router.post('/generateToken',instructorsMiddleware.check_login,eventController.generateToken)
router.post('/updateEvent',instructorsMiddleware.check_login,eventController.updateEvent)
router.post('/getCourseDetails',courseController.getCourseDetails);
//****************************************** Certificates *********************************************************/

router.post('/addCertificate',instructorsMiddleware.check_login,instructorController.addCertificate)
router.post('/updateCertificate',instructorsMiddleware.check_login,instructorController.updateCertificate)
router.get('/viewCertificate',instructorsMiddleware.check_login,instructorController.viewCertificate)
router.post('/listCertificates',instructorsMiddleware.check_login,instructorController.listCertificates)
router.post('/deleteCertificate',instructorsMiddleware.check_login,instructorController.deleteCertificate)

//premium payment apis
router.post('/initiate-premium-payment',instructorsMiddleware.check_login,premiumController.paymentInit);
router.post('/complete-premium-payment',instructorsMiddleware.check_login,premiumController.paymentFinal);

module.exports = router