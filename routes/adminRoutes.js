const express = require('express');
const app = express()
const router = express.Router()

//***********************************************************************************************/
//***********************************************************************************************/
//***********************************************************************************************/
const adminController = require('../controller/adminController/adminController')            
const adminValidation = require('../validations/adminValidation/adminValidation')
const AdminMiddleware = require('../middlewares/adminMiddleware/AdminMiddleware')

const categoryController = require('../controller/adminController/categoryController')

const questionValidation = require('../validations/adminValidation/questionValidation')
const questionController = require('../controller/adminController/questionController')

const courseController = require('../controller/adminController/courseController')
const courseValidation = require('../validations/adminValidation/courseValidation')
const courseMiddlewares = require('../middlewares/adminMiddleware/courseMiddlware')

const videoController = require('../controller/adminController/videoController')
const videoValidation = require('../validations/adminValidation/videoValidation')
const videoMiddleware = require('../middlewares/adminMiddleware/courseVideoMiddleware')

const faqValidation = require('../validations/adminValidation/faqValidation')

const orderController = require('../controller/adminController/orderController')

const subscriptionValidation = require('../validations/adminValidation/subscriptionValidation')
const subscriptionMiddleware = require('../middlewares/adminMiddleware/subscriptionMiddleware')
const subscriptionController = require('../controller/adminController/subscriptionController')

const newsValidation = require('../validations/adminValidation/newsValidation')
const newsMiddleware = require('../middlewares/adminMiddleware/newsMiddleware')
const newsController = require('../controller/adminController/newsController')

const sectionController = require('../controller/adminController/sectionController')
const sectionValidation = require('../validations/adminValidation/sectionValidation')
const sectionMiddleware = require('../middlewares/adminMiddleware/sectionMiddleware')

const instructorController = require('../controller/adminController/instructorController')
const instructorValidation = require('../validations/adminValidation/instructorValidation')
const instructorMiddleware = require('../middlewares/adminMiddleware/instructorMiddleware')

const eventController = require('../controller/adminController/eventController')
const eventValidation = require('../validations/adminValidation/eventValidation')
const eventMiddleware = require('../middlewares/adminMiddleware/eventMiddleware')

const bannerController = require('../controller/adminController/bannerController')
const bannerValidation = require('../validations/adminValidation/bannerValidation')
const bannerMiddleware = require('../middlewares/adminMiddleware/bannerMiddleware')

const homeCmsController = require('../controller/adminController/homeCmsController')
const careersController = require('../controller/adminController/careersController')
const countryController = require('../controller/adminController/countryController')
const podcastController = require('../controller/adminController/podcasts')
const corporateController = require('../controller/adminController/corporate')
const retreatController = require('../controller/adminController/retreats')
const emailTemplateController = require('../controller/adminController/emailtemplate')
const notificationTemplateController = require('../controller/adminController/notificationtemplate')
const seoController = require('../controller/adminController/seo')
//***********************************************************************************************/
//***********************************************************************************************/
//***********************************************************************************************/



//***********************************************************************************************/
//************************DropDown API************************//
router.get('/listCategories',AdminMiddleware.check_login,categoryController.listCategory)
router.get('/listCourse',AdminMiddleware.check_login,courseController.listCourse)
router.get('/listSection',AdminMiddleware.check_login,sectionController.listSection)


router.post('/signup',adminValidation.adminValidation,adminController.adminSignup)
router.post('/login',adminValidation.isEmailExist,adminController.adminlogin)
router.post('/updateAdmin',adminController.updateAdmin)
router.post('/getMainDashBoard',adminController.getMainDashBoard)
router.post('/getModuleMenu',adminController.getModuleMenu)

//************************admin Dashboard api************************************//
router.get('/dashboard',AdminMiddleware.check_login,adminController.dashboard)


//router.post('/addAboutUs',AdminMiddleware.check_login,adminController.addAboutUs)
router.post('/addAboutUs',AdminMiddleware.check_login,adminController.addAboutUs1)
router.post('/aboutUs',AdminMiddleware.check_login,adminController.aboutUs)
router.post('/updateAboutUs',AdminMiddleware.check_login,adminController.updateAboutUs)


//************************terms and condition************************//
router.post('/terms_condition',AdminMiddleware.check_login,adminController.termCondition)
router.post('/update_terms_condition',AdminMiddleware.check_login,adminController.updateTermCondition)
router.post('/listPolicies',AdminMiddleware.check_login,adminController.listPolicies)
router.post('/listContactRequests',AdminMiddleware.check_login,adminController.listContactRequests)

//************************privacy and policy************************//
router.post('/privacy_policy',AdminMiddleware.check_login,adminController.privacyPolicy)
router.post('/update_privacy_policy',AdminMiddleware.check_login,adminController.updatePrivacyPolicy)

//************************FAQ API************************//
router.post('/addFaq',AdminMiddleware.check_login,faqValidation.addFaqValidation,adminController.addFaq)
router.post('/listFaq',AdminMiddleware.check_login,adminController.listFaq)
router.post('/viewFaq',AdminMiddleware.check_login,adminController.viewFaq)
router.post('/updateFaq',AdminMiddleware.check_login,faqValidation.updateFaqValidation,adminController.updateFaq)


//************************User related API************************//
//router.get('/listUser',AdminMiddleware.check_login,adminController.listUser)
router.get('/viewUser',AdminMiddleware.check_login,adminValidation.userIdValidation,adminController.viewUser)
router.post('/listUser',AdminMiddleware.check_login,adminController.listUser1)
router.post('/blockUnblock',AdminMiddleware.check_login,adminController.blockUnblockUser)
router.post('/getCustomerPayments',AdminMiddleware.check_login,adminController.getCustomerPayments)
router.get('/customerPaymentDetails',AdminMiddleware.check_login,adminController.viewPaymentDetails)

//************************categories API************************//
router.post('/addCategories',AdminMiddleware.check_login,AdminMiddleware.check_category_name,categoryController.addCategory)
router.post('/updateCategories',AdminMiddleware.check_login,categoryController.updateCategory)
router.get('/viewCategories',AdminMiddleware.check_login,categoryController.viewCategory)
router.post('/listCategories',AdminMiddleware.check_login,categoryController.listCategory1)
router.post('/deleteCategories',AdminMiddleware.check_login,categoryController.deleteCategory)
router.post('/updateCategoryStatus',AdminMiddleware.check_login,categoryController.updateCategoryStatus)

//************************Country API************************//
router.post('/addCountry',AdminMiddleware.check_login,countryController.addCountry)
router.post('/updateCountry',AdminMiddleware.check_login,countryController.updateCountry)
router.get('/viewCountry',AdminMiddleware.check_login,countryController.viewCountry)
router.post('/listCountry',AdminMiddleware.check_login,countryController.listCountry)
router.post('/deleteCountry',AdminMiddleware.check_login,countryController.deleteCountry)
router.post('/updateCountryStatus',AdminMiddleware.check_login,countryController.updateCountryStatus)

//************************question API************************//
router.post('/addQuestion',AdminMiddleware.check_login,questionValidation.questionValidation,questionController.addQuestion)
router.post('/updateQuestion',AdminMiddleware.check_login,questionValidation.updateQuestionValidation,questionController.updateQuestion)
//router.get('/listQuestion',AdminMiddleware.check_login,questionController.listQuestion)
router.get('/viewQuestion',AdminMiddleware.check_login,questionValidation.viewQuestionValidation,questionController.viewQuestion)
router.post('/listQuestion',AdminMiddleware.check_login,questionController.listQuestion1)
router.post('/deleteQuestion',AdminMiddleware.check_login,questionController.deleteQuestion)
router.post('/updatQuestionStatus',AdminMiddleware.check_login,questionController.updatQuestionStatus)


//************************List category Question API************************//
router.post('/listCategoryQuestion',AdminMiddleware.check_login,questionController.listCategoryQuestion)

//************************course API************************//
router.post('/addCourse',AdminMiddleware.check_login,courseValidation.addCourseValidation,courseMiddlewares.check_course_name,courseController.addCourse1)
router.get('/viewCourse',AdminMiddleware.check_login,courseValidation.viewCourseValidation,courseController.viewCourse)
router.post('/updateCourse',AdminMiddleware.check_login,courseValidation.updateCourseValidation,courseController.updateCourse1)
router.post('/listCourse',AdminMiddleware.check_login,courseController.listCourse1)
router.post('/deleteCourse',AdminMiddleware.check_login,courseController.deleteCourse)
router.post('/addCourseDetails',AdminMiddleware.check_login,courseController.addCourseDetails)
router.post('/updateCourseDetails',AdminMiddleware.check_login,courseController.updateCourseDetails)
router.post('/getCourseDetails',courseController.getCourseDetails);


//************************section API**********************//
router.post('/addSection',AdminMiddleware.check_login,sectionValidation.addSectionValidation,sectionMiddleware.checkSectionNumber,sectionController.addSection)
router.post('/listSection',AdminMiddleware.check_login,sectionController.listSection1)
router.post('/updateSection',AdminMiddleware.check_login,sectionValidation.updateSectionValidation,sectionMiddleware.checkSectionNumberUpdate,sectionController.updateSection)
router.get('/viewSection',AdminMiddleware.check_login,sectionValidation.viewSectionValidation,sectionController.viewSection)
router.post('/getallCourses',sectionController.getallCourses);
router.get('/getsectionCourses',sectionController.getsectionCourses);
router.post('/deleteSection',AdminMiddleware.check_login,sectionController.deleteSection)

//************************video API************************//
router.post('/addVideo',AdminMiddleware.check_login,videoValidation.addVideoValidation,videoMiddleware.checkEpisodeNumber,videoController.addVideo)
router.post('/listVideo',AdminMiddleware.check_login,videoController.listVideo1)
router.get('/viewVideo',AdminMiddleware.check_login,videoValidation.viewVideoValidation,videoController.viewVideo)
router.post('/updateVideo',AdminMiddleware.check_login,videoMiddleware.checkEpisodeNumberUpdate,videoController.updateVideo)
router.post('/deleteVideo',AdminMiddleware.check_login,videoController.deleteVideo)

//************************course Video List************************//
router.post('/listCourseVideo',AdminMiddleware.check_login,videoController.listCourseVideo)


//************************subscription API************************//
router.post('/addSubscription',AdminMiddleware.check_login,subscriptionValidation.subscriptionValidation,subscriptionMiddleware.check_subscription_name,subscriptionController.addSubscription)
router.post('/listSubscription',AdminMiddleware.check_login,subscriptionController.listSubscription)
router.get('/viewSubscription',AdminMiddleware.check_login,subscriptionValidation.viewSubscriptionValidation,subscriptionController.viewSubscription)
router.post('/deleteSubscription',AdminMiddleware.check_login,subscriptionController.deleteSubscription)
router.post('/updateSubscription',AdminMiddleware.check_login,subscriptionValidation.updateSubscriptionValidation,subscriptionMiddleware.check_subscription_name_update,subscriptionController.updateSubscription)


//************************news API************************//
router.post('/addNews',AdminMiddleware.check_login,newsValidation.newsValidation,newsMiddleware.check_news_name,newsController.addNews)
router.post('/listNews',AdminMiddleware.check_login,newsController.listNews)
router.get('/viewNews',AdminMiddleware.check_login,newsValidation.viewNewsValidation,newsController.viewNews)
router.post('/deleteNews',AdminMiddleware.check_login,newsController.deleteNews)
router.post('/updateNews',AdminMiddleware.check_login,newsValidation.updateNewsValidation,newsMiddleware.check_news_name_update,newsController.updateNews)
router.post('/updatNewsStatus',AdminMiddleware.check_login,newsController.updatNewsStatus)

//************************order related Api************************//
router.post('/listOrder',AdminMiddleware.check_login,orderController.listOrder)
router.get('/viewOrder',AdminMiddleware.check_login,orderController.viewOrder)

//************************csv related API************************//
router.get('/exportUserCsv',adminController.exportUserCsv)



//************************instructor API************************//
router.post('/addInstructor',AdminMiddleware.check_login,instructorValidation.addInstructorValidation,instructorMiddleware.checkInstructorEmail,instructorController.addInstructor)
router.post('/listInstructor',AdminMiddleware.check_login,instructorController.listInstructor)
router.get('/viewInstructor',AdminMiddleware.check_login,instructorValidation.viewInstructorValidation,instructorController.viewInstructor)
router.post('/blockUnblockInstructor',AdminMiddleware.check_login,instructorController.blockUnblockInstructor)
router.post('/getInstructorPayments',AdminMiddleware.check_login,instructorController.getInstructorPayments)
router.get('/paymentDetails',AdminMiddleware.check_login,instructorController.viewPaymentDetails)


//************************Live Event API************************//
router.post('/addEvent',AdminMiddleware.check_login,eventValidation.addEventValidation,eventMiddleware.check_event_name,eventController.addEvent)
router.post('/updateEvent',AdminMiddleware.check_login,eventValidation.updateEventValidation,eventMiddleware.check_event_name_update,eventController.updateEvent)
router.get('/viewEvent',AdminMiddleware.check_login,eventController.viewEvent)
router.post('/listEvent',AdminMiddleware.check_login,eventController.listEvent1)
router.post('/deleteEvent',AdminMiddleware.check_login,eventController.deleteEvent)
router.post('/generateToken',AdminMiddleware.check_login,eventController.generateToken)
router.post('/updateEventStatus',AdminMiddleware.check_login,eventController.updateEventStatus)
router.post('/startRecording',AdminMiddleware.check_login,eventController.startRecording)


//************************Banner API************************//
router.post('/addBanner',AdminMiddleware.check_login,bannerController.addBanner)
router.get('/viewBanner',AdminMiddleware.check_login,bannerController.viewBanner)
router.post('/updateCourse',AdminMiddleware.check_login,courseValidation.updateCourseValidation,courseMiddlewares.check_course_name_update,courseController.updateCourse1)
router.post('/listBanner',AdminMiddleware.check_login,bannerController.listBanner)
router.post('/deleteBanner',AdminMiddleware.check_login,bannerController.deleteBanner)
router.post('/updateBanner',AdminMiddleware.check_login,bannerController.updateBanner)
router.post('/updatBannerStatus',AdminMiddleware.check_login,bannerController.updatBannerStatus)

router.post('/updateHomePage',AdminMiddleware.check_login,homeCmsController.updateHomePage)
router.get('/lisHomePageCms',AdminMiddleware.check_login,homeCmsController.lisHomePageCms)
router.post('/updateAboutUsWeb',AdminMiddleware.check_login,homeCmsController.updateAboutUsWeb)
router.get('/listAboutUsWeb',AdminMiddleware.check_login,homeCmsController.listAboutUsWeb)
router.get('/getTestimonials',AdminMiddleware.check_login,homeCmsController.getTestimonials)
router.get('/getGeneralData',AdminMiddleware.check_login,homeCmsController.getGeneralData)
router.post('/updateGeneralData',AdminMiddleware.check_login,homeCmsController.updateGeneralData)

router.post('/updateCareersContent',AdminMiddleware.check_login,careersController.updateCareersContent)
router.get('/getCareerContent',AdminMiddleware.check_login,careersController.getCareerContent)
router.post('/addJobOpening',AdminMiddleware.check_login,careersController.addJobOpening)
router.post('/updateJobOpening',AdminMiddleware.check_login,careersController.updateJobOpening)
router.get('/viewJobOpening',AdminMiddleware.check_login,careersController.viewJobOpening)
router.post('/listJobs',AdminMiddleware.check_login,careersController.listJobs)
router.post('/deleteJob',AdminMiddleware.check_login,careersController.deleteJob)
router.post('/updateJobStatus',AdminMiddleware.check_login,careersController.updateJobStatus)
router.post('/listAppliedJobs',AdminMiddleware.check_login,careersController.listAppliedJobs)
router.get('/viewAppliedJobs',AdminMiddleware.check_login,careersController.viewAppliedJobs)

router.post('/addPodcast',AdminMiddleware.check_login,podcastController.addPodcast)
router.post('/updatePodcast',AdminMiddleware.check_login,podcastController.updatePodcasts)
router.get('/viewPodcast',AdminMiddleware.check_login,podcastController.viewPodcast)
router.post('/listPodcast',AdminMiddleware.check_login,podcastController.getPodcastsData)
router.post('/deletePodcast',AdminMiddleware.check_login,podcastController.deletePodcast)
router.post('/updatePodcastStatus',AdminMiddleware.check_login,podcastController.updatePodcastStatus)
router.post('/makeAudioFree',AdminMiddleware.check_login,podcastController.makeAudioFree)

router.post('/updateCorporate',AdminMiddleware.check_login,corporateController.updateCorporate)
router.get('/viewCorporate',AdminMiddleware.check_login,corporateController.viewCorporate)
router.get('/getCorporateData',AdminMiddleware.check_login,corporateController.getCorporateData)

router.post('/updateRetreatOne',AdminMiddleware.check_login,retreatController.updateRetreatOne)
router.post('/updateRetreatTwo',AdminMiddleware.check_login,retreatController.updateRetreatTwo)
router.get('/viewRetrat',AdminMiddleware.check_login,retreatController.viewRetrat)
router.get('/getRetreatData',AdminMiddleware.check_login,retreatController.getRetreatData)

//************************template API************************//
router.post('/addEmailTemplate',AdminMiddleware.check_login,emailTemplateController.addEmailTemplate)
router.post('/updateEmailTemplate',AdminMiddleware.check_login,emailTemplateController.updateEmailTemplate)
router.get('/viewEmailTemplate',AdminMiddleware.check_login,emailTemplateController.viewEmailTemplate)
router.post('/listEmailTemplate',AdminMiddleware.check_login,emailTemplateController.listEmailTemplate)
router.post('/deleteEmailTemplate',AdminMiddleware.check_login,emailTemplateController.deleteEmailTemplate)
router.post('/updateEmailTemplateStatus',AdminMiddleware.check_login,emailTemplateController.updateEmailTemplateStatus)

router.post('/addNotificationTemplate',AdminMiddleware.check_login,notificationTemplateController.addNotificationTemplate)
router.post('/listNotificationTemplate',AdminMiddleware.check_login,notificationTemplateController.listNotificationTemplate)
router.post('/deleteNotificationTemplate',AdminMiddleware.check_login,notificationTemplateController.deleteNotificationTemplate)

//************************seo API************************//
router.post('/addSeo',AdminMiddleware.check_login,seoController.addSeo)
router.post('/updateSeo',AdminMiddleware.check_login,seoController.updateSeo)
router.get('/viewSeo',AdminMiddleware.check_login,seoController.viewSeo)
router.post('/listSeo',AdminMiddleware.check_login,seoController.listSeo)
router.post('/deleteSeo',AdminMiddleware.check_login,seoController.deleteSeo)
router.post('/updateSeoStatus',AdminMiddleware.check_login,seoController.updateSeoStatus)
// router.post('/image',(req,res)=>{
//      console.log(req.files)
// })
//***************************************************************************************************************************/
module.exports = router