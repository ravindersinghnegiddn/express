var FCM = require('fcm-node');
const userService = require('../services/adminServices/adminServices')
var serverKey = 'AAAAQvbOeM0:APA91bFHHYniSeq9DFJOdyVwLhWEBbIUwrPjp97q6Ar2GB_Z_GQxL6RKnMRGX0z8wQKYGzODbpSBPl1u-ciLqHMafSCgKvGuXK1KERAcNuSJS9S-cur-DemCBtrpx0XWM0IY___4KEK8'; //put your server key here
var fcm = new FCM(serverKey);


const sendPush = async(message1,courseThumbnail) =>{

    const androidDeviceToken = await userService.listAndroidDeviceToken()
    if(androidDeviceToken.length >0 ){
         androidDeviceToken.forEach(element => {
           // console.log(element)
            var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                to: element.deviceToken, 
            
                
                notification: {
                    title: 'Bliss Genx', 
                    body: message1, 
                    image: courseThumbnail
                },
                
            
            };

            fcm.send(message, function(err, response){
                if (err) {
                    console.log(JSON.stringify(err));

                    console.log("Something has gone wrong!");
                    
                } else {
                    console.log("Successfully sent with response: ", response);
                    
                    
                }
            });
         });
        return   
     }
}

module.exports = {sendPush}